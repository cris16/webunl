/* global Ext, opcionesCheck, INFOMESSAGEBLANKTEXT, INFOMESSAGEREQUERID, formatEstadoRegistro, HEIGT_VIEWS */

/**
 * This view is an example list of people.
 */
STORE_INTENCION = Ext.create('botWeb.store.intencion.s_Intencion');
COMBO_INTENCION_ELEMENTOS = Ext.create('botWeb.store.stores.s_TipoElemento');
Ext.define('botWeb.view.intencion.v_Intencion', {
    extend: 'Ext.panel.Panel',
    xtype: 'intenciones',
    controller: 'c_Intencion',
//    title: 'intencion',
    requires: [
        'Ext.layout.container.Border',
        'Ext.selection.CellModel',
        'botWeb.view.intencion.c_Intencion'
    ],
    layout: 'border',
    bodyBorder: false,
    defaults: {
        collapsible: true,
        collapsed: false,
        split: true,
        bodyPadding: 0
    },
    listeners: {
        afterrender: 'onView'
    },
    items: [
        {
            xtype: 'panel',
            region: 'center',
            width: '70%',
            collapseMode: 'mini',
            margin: 1, 
            header: false,
//            collapsible: true,
            collapsed: false,
            collapsible: false,
//            layout: 'fit',
            items: [
                {
                    xtype: 'grid',
                    name: 'grid',
                    plugins: [{ptype: 'gridfilters'}],
                    bufferedRenderer: false,
                    store: STORE_INTENCION,
                    viewConfig: {
                        deferEmptyText: false,
                        enableTextSelection: true,
                        preserveScrollOnRefresh: true,
                        listeners: {
                            loadingText: 'Cargando...'
                        }, loadMask: true,
                        emptyText: '<center><h1 style="margin:20px">No existen resultados</h1></center>'
                    },
                    tbar: [{
                            xtype: 'textfield',
                            name: 'txtParam',
                            flex: 2,
                            emptyText: 'Nombre,descripción corta,descripción larga...',
                            tip: 'Buscar por el nombre de la intención o descripción',
                            listeners: {
                                specialkey: 'onBuscar',
                                render: 'cargarToolTip'
                            }
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-search',
                            tooltip: 'Buscar',
                            handler: 'onBuscar'
                        }, {
                            xtype: 'button',
                            iconCls: 'x-fa fa-eraser',
                            tooltip: 'Limpiar buscador',
                            handler: 'onLimpiar'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fas fa-sync',
                            tooltip: 'Recargar',
                            handler: 'onRecargar'
                        }
                    ],
                    features: [
                        {
                            ftype: 'grouping',
                            groupHeaderTpl: '{name}',
                            hideGroupedHeader: true,
                            enableGroupingMenu: true
                        }
                    ],
                    columns: [
                        Ext.create('Ext.grid.RowNumberer', {header: '#', width: 40, align: 'center'}),
                        {text: 'Intención', dataIndex: 'nombre', tooltip: "Intención", filter: true, sortable: true, flex: 1},
                        {text: 'Parametro', dataIndex: 'parametro', filter: true, sortable: true, flex: 1},
                        {text: 'Mensaje', dataIndex: 'text', filter: true, sortable: true, flex: 1},
                        {text: 'Respuesta', dataIndex: 'respuesta', filter: true, sortable: true, flex: 1},
                        {text: 'Fecha Registro', dataIndex: 'fechaRegistro', filter: true, sortable: true, xtype: 'datecolumn', format: 'Y-m-d H:i:s', flex: 1},
                        {filter: true, tooltip: "Habilitado", text: "Habilitado", width: 100, dataIndex: 'habilitado', sortable: true, renderer: formatEstadoRegistro}
                    ],
                    listeners: {
                        select: 'onSelect'
                    },
                    bbar: Ext.create('Ext.PagingToolbar', {
                        store: STORE_INTENCION,
                        displayInfo: true,
                        name: 'paginacionGrid',
                        emptyMsg: "Sin datos que mostrar.",
                        displayMsg: 'Visualizando {0} - {1} de {2} registros',
                        beforePageText: 'Página',
                        afterPageText: 'de {0}',
                        firstText: 'Primera página',
                        prevText: 'Página anterior',
                        nextText: 'Siguiente página',
                        lastText: 'Última página',
                        refreshText: 'Actualizar',
                        inputItemWidth: 35,
                        items: [
                            {
//                                    xtype: 'button',
//                                    text: 'Exportar',
//                                    iconCls: 'x-fa fa-download',
//                                    handler: function (btn) {
//                                        onExportar(btn, "Sanciones", this.up('grid'));
//                                    }
                            }
                        ],
                        listeners: {
                            afterrender: function () {
                                this.child('#refresh').hide();
                            }
                        }
                    })
                }
            ]
        },
        {
            xtype: 'panel',
            region: 'east',
            padding: 5,
            width: '30%',
            collapseMode: 'mini',
            anchor: '100% 100%',
            layout: 'anchor',
            cls: 'panelCrearEditar',
            autoScroll: true,
            collapsible: true,
            collapsed: false,
            header: false,
            headerAsText: false,
            items: [
                {
                    xtype: 'form',
                    title: 'Tipo de Intenciones',
                    name: 'form',
                    cls: 'quick-graph-panel shadow panelComplete',
                    ui: 'light',
                    padding: 5,
                    flex: 1,
//                    height: window.innerHeight - 60,
                    defaults: {
                        width: '100%',
                        margin: '0 0 5 0',
                        defaultType: 'textfield',
//                        minLengthText: MINIMUMMESSAGUEREQUERID,
//                        maxLengthText: MAXIMUMMESSAGUEREQURID,
//                        afterLabelTextTpl: INFOMESSAGEREQUERID,
                        blankText: INFOMESSAGEBLANKTEXT,
                        labelWidth: 90,
                        allowOnlyWhitespace: false,
                        allowBlank: false
                    },
                    items: [{
                            xtype: "combobox",
                            fieldLabel: "Tipo de Elemento",
                            value: 0,
                            width: "100%",
                            tooltip: "Seleccionar el tipo de mensaje",
                            name: "idIntencionTipo",
                            emptyText: "Seleccione",
                            reference: "text",
                            displayField: "text",
                            valueField: "id",
                            filterPickList: true,
                            queryParam: "param",
                            queryMode: "remote",
                            growMax: 20,
//                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                            allowOnlyWhitespace: false,
//                                blankText: INFOMESSAGEBLANKTEXT,
                            store: Ext.create('botWeb.store.stores.s_TipoIntencion')

                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Intención',
                            name: 'nombre',
                            minLength: 3,
                            maxLength: 45
                        },
                        {
                            xtype: 'numberfield',
                            fieldLabel: 'Parametro',
                            name: 'parametro',
                            minLength: 1,
                            maxLength: 5
                        },
                        {
                            xtype: "combobox",
                            fieldLabel: "Tipo de Mensaje",
                            value: 'text',
                            forceSelection: true,
                            width: "100%",
                            tooltip: "Seleccionar un tipo",
                            name: "idIntencionTipoMensaje",
                            emptyText: "Seleccione",
                            reference: "text",
                            displayField: "text",
                            valueField: "id",
                            filterPickList: true,
                            anyMatch: true,
                            queryMode: "local",
                            growMax: 20,
//                            afterLabelTextTpl: INFOMESSAGEREQUERID,
                            allowOnlyWhitespace: false,
//                            blankText: INFOMESSAGEBLANKTEXT,
                            store: Ext.create('botWeb.store.stores.s_TipoIntencionMensaje')
                        }, {
                            text: "Agregar Elemento",
                            name: 'btnReasignar',
                            xtype: 'button',
                            height: 30,
                            tooltip: 'Agregar a la Lista',
                            handler: 'onCargarElemento'
                        },
                        {
                            xtype: 'textfield',
                            name: 'respuesta',
                            value: "{}",
                            hidden: true,
                            allowOnlyWhitespace: true

                        },
                        {
                            xtype: 'textarea',
                            fieldLabel: 'Mensaje',
                            name: 'text',
                            minLength: 3,
                            maxLength: 145,
                            emptyText: 'Descripción de la intención',
                            hideLabel: true
                        },
                        {
                            xtype: 'checkboxgroup',
                            allowBlank: true,
                            allowOnlyWhitespace: true,
                            items: [
                                {xtype: 'checkboxfield', uncheckedValue: 0, name: 'habilitado', boxLabel: 'Habilitado', inputValue: 1}
                            ]
                        },
                        {
                            name: 'dataEntrenamiento',
                            hidden: true,
                            value: false,
                            allowBlank: true,
                            allowOnlyWhitespace: true
                        },
                        {
                            text: "Agregar Elemento a Entrenar",
                            name: 'btnReasignar',
                            xtype: 'button',
                            height: 30,
                            tooltip: 'Agregar a la Lista',
                            handler: 'onAgregarElementos'
                        },
                        {
                            xtype: "grid",
                            name: "gridEntrenamiento",
                            height: '50%',
                            region: 'center',
                            autoScroll: true,
                            bufferedRenderer: false,
                            anchor: '100% 100%',
                            minHeight: 120,
                            maxHeight: 200,
                            viewConfig: {
                                enableTextSelection: true,
                                emptyText: "<center>Sin Elementos..</center>"
                            },
                            resizable: true,
                            selModel: {
                                type: 'cellmodel'
                            },
                            plugins: {
                                cellediting: {
                                    clicksToEdit: 2
                                }
                            },
                            store: Ext.create('botWeb.store.stores.s_Intencion_Entrenamiento'),
                            columns: [
                                {name: 'Text', text: "Título", flex: 3, dataIndex: "text", editor: {
                                        allowBlank: false,
                                        selectOnFocus: false
                                    }},
                                {flex: 1,
                                    boxLabel: "Habilitado",
                                    menuDisabled: true,
                                    sortable: false,
                                    xtype: 'actioncolumn',
                                    minWidth: 20,
                                    items: [
                                        {
                                            getClass: opcionesCheck,
                                            handler: 'onDeleteEntrenamiento'
                                        }
                                    ]
                                }
                            ],
                        }
                    ],
                    dockedItems: [{
                            ui: 'footer',
                            xtype: 'toolbar',
                            dock: 'bottom',
                            defaults: {
                                width: '25%',
                                height: 30
                            },
                            items: [
                                {
                                    text: 'Limpiar',
                                    tooltip: 'Limpiar',
                                    disabled: false,
                                    limpiar: true,
                                    handler: 'onLimpiarFormulario'
                                },
                                '->',
                                {
                                    text: 'Crear',
                                    tooltip: 'Crear',
                                    name: 'btnCrear',
                                    handler: 'onCrear',
                                    disabled: false
                                },
                                {
                                    text: 'Editar',
                                    tooltip: 'Editar',
                                    name: 'btnEditar',
                                    handler: 'onEditar',
                                    disabled: false
                                }
                            ]
                        }
                    ],
                },
            ],

        }
    ]
});
