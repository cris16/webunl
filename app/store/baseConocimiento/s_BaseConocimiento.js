/* global Ext, MODULO_INTENCION, MODULO_BASE_CONOCIMIENTO */

Ext.define('botWeb.store.baseConocimiento.s_BaseConocimiento', {
    extend: 'Ext.data.Store',
    fields: [
        {name: 'id'},
        {name: "entidadUno"},
        {name: "idEntidadUno"},
        {name: "idEntidadDos"},
        {name: "entidadDos"},
        {name: "idEntidadTres"},
        {name: "entidadTres"},
        {name: "orden"},
        {name: "ayuda"},
        {name: "text"},
        {name: "articulo"},
        {name: "version"},
        {name: "habilitado"},
        {name: "fechaRegistro"}
    ],
    pageSize: 250,
    remoteSort: false,
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/baseConocimiento/read.php',
            create: 'php/baseConocimiento/create.php',
            update: 'php/baseConocimiento/update.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'data',
            totalProperty: 'total',
            successProperty: 'success'
        },
        simpleSortMode: true
    },
    listeners: {
        beforeload: function (store, operation, eOpts) {
            if (MODULO_BASE_CONOCIMIENTO) {
                var params = {
                    param: MODULO_BASE_CONOCIMIENTO.down('[name=txtParam]').getValue()
                };
                store.proxy.extraParams = params;
            }
        },
        load: function (thisObj, records, successful, operation, eOpts) {
            if (!successful) {
                var obj = {};
                if (isJsonString(operation._response.responseText)) {
                    obj = JSON.parse(operation._response.responseText);
                    if (!obj.success)
                        if (obj.error)
                            Ext.toast(obj.error, 'ERROR');
                        else if (obj.message)
                            Ext.toast(obj.message, 'Mensaje');
                        else
                            Ext.toast('Problema en la comunicación con el servidor local', 'ERROR', );
                } else {
                    obj.resAjax = -2;
                    obj['error'] = "Exite un error al crear el JSON";
                    obj['message'] = "Existe un error con la solicitud realizada, inténtelo mas tarde por favor.";
                    Ext.toast(obj.message, 'Mensaje');
                }
            }
        }
    }


});
