/* global Ext */

/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting automatically applies the "viewport"
 * plugin causing this view to become the body element (i.e., the viewport).
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
HEIGT_VIEWS=0;
Ext.define('botWeb.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',

    requires: [
        'Ext.plugin.Viewport',
        'Ext.window.MessageBox',
        'botWeb.view.main.MainController',
        'botWeb.view.main.MainModel',
        'botWeb.view.intencion.v_Intencion',
        'botWeb.view.usuarios.v_Usuarios',
        'botWeb.view.entidades.v_Entidades',
        'botWeb.view.baseConocimiento.v_BaseConocimiento',
        'botWeb.view.plataforma.v_Plataforma',
        'botWeb.view.logs.v_Logs'
    ],

    controller: 'main',
    viewModel: 'main',

    ui: 'navigation',

    tabBarHeaderPosition: 1,
    titleRotation: 0,
    tabRotation: 0,
    listeners: {
        afterrender: 'onView',
        activeItemchange: 'onSelectionchange'
    },

    header: {
        layout: {
            align: 'stretchmax'
        },
        title: {
            bind: {
                text: '{name}'
            },
            flex: 0
        },
        iconCls: 'fa-th-list'
    },

    tabBar: {
        flex: 1,
        layout: {
            align: 'stretch',
            overflowHandler: 'none'
        }
    },

    responsiveConfig: {
        tall: {
            headerPosition: 'top'
        },
        wide: {
            headerPosition: 'left'
        }
    },

    defaults: {
        bodyPadding: 20,
        tabConfig: {
            responsiveConfig: {
                wide: {
                    iconAlign: 'left',
                    textAlign: 'left'
                },
                tall: {
                    iconAlign: 'top',
                    textAlign: 'center',
                    width: 120
                }
            }
        }
    },

    items: [],
    tbar: ['->',
        {
            xtype: 'button',
            text: 'Entrenar Modelo',
            listeners: {
                click: 'onEntrenarModelo'
            }
        },
        {
            xtype: 'button',
            text: 'Actualizar Plataformas',
            listeners: {
                click: 'onActualizarPlataforma'
            }
        },
        {
            xtype: 'button',
            text: 'Salir',
            listeners: {
                click: 'onSalir'
            }
        }]
});
