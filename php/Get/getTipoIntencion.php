<?php

include '../../dll/config.php';
//include '../../dll/funciones.php';
extract($_GET);
extract($_POST);
if (!$mysqli = getConectionDb())
    return $mysqli;
$sql = "SELECT idIntencionTipo, tipo "
        . "FROM $DB_NAME.intencionTipo ";
if (isset($param) && ($param !== '')) {
        $sql .= " WHERE LOWER(tipo) LIKE LOWER('%$param%')";
}

if (isset($limite)) {
    $sql .= " LIMIT $limite";
} else {
    $sql .= " LIMIT $LIMITE_REGISTROS";
}
$result = $mysqli->query($sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS", 'sql' => $sql));
    return $mysqli->close();
}
$arreglo = [];
while ($myrow_read_users = $result->fetch_assoc()) {
    $arreglo[] = array(
        'id' => intval($myrow_read_users["idIntencionTipo"]),
        'text' => $myrow_read_users["tipo"]
    );
}
$mysqli->close();
echo json_encode(array('success' => true, 'data' => $arreglo));

