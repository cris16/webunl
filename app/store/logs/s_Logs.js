/* global Ext, MODULO_INTENCION, MODULO_PLATAFORMA, MODULO_LOGS */

Ext.define('botWeb.store.logs.s_Logs', {
    extend: 'Ext.data.Store',
    fields: [
        {name: 'id'},
        {name: 'idPlataforma'},
        {name: "idCliente"},
        {name: "intencion"},
        {name: "porcentajeProximacion"},
        {name: "mensajeEntrada"},
        {name: "respuesta"},
        {name: "lugar"},
        {name: "fechaRegistro"}
    ],
    pageSize: 250,
    remoteSort: false,
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/logsUsuario/read.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'data',
            totalProperty: 'total',
            successProperty: 'success'
        },
        simpleSortMode: true
    },
    listeners: {
        beforeload: function (store, operation, eOpts) {
            if (MODULO_LOGS) {
                var params = {};
                var fecha = MODULO_LOGS.down('[name=fecha]').getRawValue();
                params['fecha'] = fecha;
                if (MODULO_LOGS.down('[name=idPlataforma]').getSelection())
                    params['idPlataforma'] = MODULO_LOGS.down('[name=idPlataforma]').getSelection()['id'];
                if (MODULO_LOGS.down('[name=idIntencion]').getSelection())
                    params['idIntencion'] = MODULO_LOGS.down('[name=idIntencion]').getSelection()['id'];
                if (MODULO_LOGS.down('[name=txtParam]').getValue())
                    params['param'] = MODULO_LOGS.down('[name=txtParam]').getValue();
                store.proxy.extraParams = params;
            }
        },
        load: function (thisObj, records, successful, operation, eOpts) {
            if (!successful) {
                var obj = {};
                if (isJsonString(operation._response.responseText)) {
                    obj = JSON.parse(operation._response.responseText);
                    if (!obj.success)
                        if (obj.error)
                            Ext.toast(obj.error, 'ERROR');
                        else if (obj.message)
                            Ext.toast(obj.message, 'Mensaje');
                        else
                            Ext.toast('Problema en la comunicación con el servidor local', 'ERROR', );
                } else {
                    obj.resAjax = -2;
                    obj['error'] = "Exite un error al crear el JSON";
                    obj['message'] = "Existe un error con la solicitud realizada, inténtelo mas tarde por favor.";
                    Ext.toast(obj.message, 'Mensaje');
                }
            }
        }
    }


});
