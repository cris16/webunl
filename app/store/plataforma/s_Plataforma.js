/* global Ext, MODULO_INTENCION, MODULO_PLATAFORMA */

Ext.define('botWeb.store.plataforma.s_Plataforma', {
    extend: 'Ext.data.Store',
    fields: [
        {name: 'id'},
        {name: 'idPlataforma'},
        {name: "detalle"},
        {name: "plataforma"},
        {name: "tipoPlataforma"},
        {name: "credenciales"},
        {name: "version"},
        {name: "versionPlataforma"},
        {name: "idTipoPlataforma"},
        {name: "habilitado"},
        {name: "fechaRegistro"}
    ],
    pageSize: 250,
    remoteSort: false,
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/plataforma/read.php',
            create: 'php/plataforma/create.php',
            update: 'php/plataforma/update.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'data',
            totalProperty: 'total',
            successProperty: 'success'
        },
        simpleSortMode: true
    },
    listeners: {
        beforeload: function (store, operation, eOpts) {
            if (MODULO_PLATAFORMA) {
                var params = {
                    param: MODULO_PLATAFORMA.down('[name=txtParam]').getValue()
                };
                store.proxy.extraParams = params;
            }
        },
        load: function (thisObj, records, successful, operation, eOpts) {
            if (!successful) {
                var obj = {};
                if (isJsonString(operation._response.responseText)) {
                    obj = JSON.parse(operation._response.responseText);
                    if (!obj.success)
                        if (obj.error)
                            Ext.toast(obj.error, 'ERROR');
                        else if (obj.message)
                            Ext.toast(obj.message, 'Mensaje');
                        else
                            Ext.toast('Problema en la comunicación con el servidor local', 'ERROR', );
                } else {
                    obj.resAjax = -2;
                    obj['error'] = "Exite un error al crear el JSON";
                    obj['message'] = "Existe un error con la solicitud realizada, inténtelo mas tarde por favor.";
                    Ext.toast(obj.message, 'Mensaje');
                }
            }
        }
    }


});
