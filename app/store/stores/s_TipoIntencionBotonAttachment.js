/* global Ext */

Ext.define('botWeb.store.stores.s_TipoIntencionBotonAttachment', {
    extend: 'Ext.data.ArrayStore',
    data: [
        ["postback", 'Postback'],
        ["web_url", 'Dirección web'],
        ["phone_number", 'Número Telefonico']
    ],
    fields: [
        {name: 'id'},
        {name: 'text'}
    ]
});