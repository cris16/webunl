
/* global MODULO_MAIN, Ext */
var INFOMESSAGEBLANKTEXT = "Este campo no puede ser vacío";
//var URL_SERVICIOS = "http://4448-45-161-34-73.ngrok.io/botUnl/recurso/";
var URL_SERVICIOS = "https://cristophernagua.com/botUnl/recurso/";
var INFOMESSAGEREQUERID = "<i style='color:red;font-weight:bold' data-qtip='Obligatorio'>*</i>";
var INFOMESSAGEBLANKTEXT = "Este campo no puede ser vacío";
function opcionesCheck(v, meta, rec) {
//    console.log("rec.data", rec.data);
    if (rec.data.habilitado == "1" || rec.data.habilitado == true) {
        return 'gridAuxCheck x-fa fa-check';
    } else {
        if (rec.data.nuevo) {
            return 'gridAuxDelete x-fa fa-times';
        } else {
            return 'gridAuxDelete x-fa fa-times';
        }
    }
}

function formatEstadoRegistro(estado, metaData) {
    var htmlEstado = '';
    if (estado == "1" || estado == true)
        htmlEstado = '<span style="color:green;" title="Habilitado">Habilitado</span>';
    else
        htmlEstado = '<span style="color:red;" title="Deshabilitado">Deshabilitado</span>';
    return htmlEstado;
}

function onProcesarPeticion(response, funcionLimpiar) {
    if (response.operations.length > 0) {
        var obj = {};
        console.log("response.operations[0]", response);
        if (isJsonString(response.operations[0]._response.responseText)) {
            obj = JSON.parse(response.operations[0]._response.responseText);
            if (obj.success)
                notificaciones("Petición realizada con éxito", 1);
            else {
                if (obj.error)
                    notificaciones(obj.error, null, 2);
                else if (obj.message)
                    notificaciones(obj.message, 'Mensaje', 1);
                else
                    notificaciones("Lamentamos los inconvenientes, la petición no se ha completado.", 'Mensaje', 2);
            }
        } else {
            obj.resAjax = -2;
            obj['error'] = "Exite un error al crear el JSON";
            obj['message'] = "Exite un error al crear el JSON";
            notificaciones(obj.message, 'Mensaje', 2);
        }
    }
    if (typeof funcionLimpiar === 'function')
        funcionLimpiar();
}

function notificaciones(s, title, tipo, opciones) {
    var data = {
        html: s,
        closable: false,
        align: 't',
        slideDUration: 400,
        maxWidth: 400
    };
    if (title.length > 2)
        data['title'] = title;
    if (tipo == 2) {
        data['title'] = "Error";
        data['iconCls'] = 'fa fa-info-circle';
    } else if (tipo == 1) {
        data['iconCls'] = 'x-fa fa-check';
    }
    Ext.toast(data);
}

function isJsonString(texto) {
    if (texto) {
        texto = texto.toString();
        if (texto.substring(0, 1) === "{" && texto.substring(texto.length - 1, texto.length) === "}")
            return true;
    }
    return false;
}

function mensajesValidacionForms(fields) {
    fields.each(function (field) {
        if (!field.isValid()) {
            if (field.getErrors().length > 0)
                if (field.getErrors()[0] === "Este campo no puede ser vacío") {
                    return  notificaciones("Debe llenar el campo: " + field.fieldLabel, 2);
                }
        }
    });
}

function setMensajeGridEmptyText(grid, mensaje) {
    grid.getStore().removeAll();
    grid.getView().setEmptyText('<center>' + mensaje + '</center>');
}

function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function setCookie(cname, cvalue, exdays) {
    const d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    let expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getAjax(url, params, opciones, callback) {
    var {timeout, headers, body, method} = opciones;
    if (!timeout)
        timeout = 60000;//1 minuto
    if (!headers)
        headers = {};
    if (!body)
        body = {};
    if (!method)
        method = 'POST';
    Ext.Ajax.request({
        url,
        params,
        timeout,
        headers,
        body,
        method,
        success: function (response, opts) {
            console.log("success", response);
            return callback(response);
        },
        failure: function (response, opts) {
            console.log("failure", response);
            return callback(response);
        }
    });
}

function isInStore(store, item, label, type) {
    switch (type) {
        case 'exact':
            var exist = store.findExact(label, item);
            if (exist !== -1) {
                exist = store.data.items[exist];
            } else {
                exist = null;
            }
            break;
        default:
            var exist = store.findRecord(label, item);
            break;
    }
    if (exist !== null) {
        return exist;
    } else {
        return false;
    }
}