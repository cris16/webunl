<?php
session_start();
// Comprobar si esta logueado
if (!isset($_SESSION["IS_SESSION"])) {
   header("Location: login.html");
   return;
}
