<?php

$GLOBALS['DB_NAME'] = $DB_NAME = "botUnl";
$GLOBALS['DB_HOST'] = $DB_HOST = "128.199.10.156";
$GLOBALS['DB_USER'] = $DB_USER = "rootCris";
$GLOBALS['DB_PASSWORD'] = $DB_PASSWORD = "}C6;zbdPEt2CoK%Vc^";
$PUERTO = "9090";
$LIMITE_REGISTROS=10;
session_start();

function conectBase($db_host, $db_user, $db_password, $db_name, $puerto) {
    $mysqli = new mysqli($db_host, $db_user, $db_password, $db_name, $puerto);
    if ($mysqli->connect_errno) {
        echo json_encode(array('success' => false, 'message' => "ERROR EN LA CONEXIÓN A BASE DE DATOS"));
        return false;
    }
    $mysqli->set_charset("utf8mb4");
    return $mysqli;
}

function getConectionDb() {
    $db_host = $GLOBALS['DB_HOST'];
    $db_name = $GLOBALS['DB_NAME'];
    $db_user = $GLOBALS['DB_USER'];
    $db_password = $GLOBALS['DB_PASSWORD'];
    return conectBase($db_host, $db_user, $db_password, $db_name, 3306);
}

function EJECUTAR_SQL($mysqli, $sql) {
    $stmt = $mysqli->prepare($sql);
    if (!$stmt) {
        return array('success' => false, 'message' => "ERROR EN EL SQL: $sql", "filas" => 0, "error" => "PROBLEMA EN LA COMUNICACIÓN CON EL SERVIDOR LOCAL, POR FAVOR INTENTELO MAS TARDE.", "stmt" => $stmt);
    }
    $stmt->execute();
    if ($stmt->affected_rows > 0) {
        return array('success' => true, 'message' => "SOLICITUD REALIZADA CON ÉXITO.", "id" => $stmt->insert_id, "filas" => $stmt->affected_rows, "error" => "", "stmt" => $stmt);
    } elseif ($stmt->affected_rows === 0) {
        return array('success' => false, 'message' => "SQL: $sql", "error" => "NO SE HAN DETECTADO CAMBIOS.", "filas" => -1, "stmt" => $stmt);
    } else {
        return array('success' => false, 'message' => "SQL: $sql", "error" => "ERROR: " . $stmt->error, "filas" => -1, "stmt" => $stmt);
    }
}
