<?php

include '../../dll/config.php';
if (!$mysqli = getConectionDb()) {
    return $mysqli;
}
extract($_GET);

$sql = "SELECT count(*) as total FROM botUnl.logs l LEFT JOIN botUnl.plataforma p ON p.idPlataforma = l.idPlataforma inner join botUnl.intenciones i on i.idIntenciones=l.idIntencion  WHERE TRUE";
if (isset($param)) {
    $sql .= " AND l.idCliente like '%$param%'";
}

if (isset($fecha)) {
    $sql .= " AND l.fecha = '$fecha' ";
}
if (isset($idIntencion)) {
    $sql .= " AND l.idIntencion = $idIntencion ";
}
if (isset($idPlataforma)) {
    $sql .= " AND l.idPlataforma = $idPlataforma ";
}

$result = $mysqli->query($sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
    return $mysqli->close();
}
$myrow = $result->fetch_assoc();
$total = $myrow['total'];
if ($total > 0) {
    $sql = "SELECT l.idLogs,l.idPlataforma,ifnull(c.alias, l.idCliente) as idCliente,i.nombre as intencion,l.porcentajeProximacion,l.mensajeEntrada,l.respuesta,DATE_FORMAT(CONVERT_TZ(l.fechaRegistro, @@session.time_zone, '+00:00'), '%Y-%m-%dT%H:%i:%s.000Z') AS fechaRegistro, ifnull(p.nombre,if(l.idPlataforma='web','Consulta realizada desde postman','no Identificado')) as lugar"
            . " FROM botUnl.logs l"
            . " LEFT JOIN botUnl.plataforma p ON p.idPlataforma = l.idPlataforma"
            . " INNER JOIN botUnl.intenciones i on i.idIntenciones=l.idIntencion"
            . " LEFT JOIN botUnl.clientes c on c.idClientes=l.idCliente and c.idPlataforma=l.idPlataforma"
            . " WHERE TRUE ";
    if (isset($param)) {
        $sql .= " AND l.idCliente like '%$param%'";
    }

    if (isset($fecha)) {
        $sql .= " AND l.fecha = '$fecha' ";
    }
    if (isset($idIntencion)) {
        $sql .= " AND l.idIntencion = $idIntencion ";
    }
    if (isset($idPlataforma)) {
        $sql .= " AND l.idPlataforma = $idPlataforma ";
    }
    $sql .= " order by l.fechaRegistro";
    if (isset($limit)) {
        $inicio = intval($limit) * (intval($page) - 1);
        $sql .= " LIMIT $inicio, $limit ";
    } else {
        $sql .= " LIMIT $LIMITE_REGISTROS";
    }
    $result = $mysqli->query($sql);
    if (!isset($result->num_rows)) {
        echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
        return $mysqli->close();
    }
    $arreglo = [];
    while ($myrow_read = $result->fetch_assoc()) {
        $arreglo[] = array(
            'id' => $myrow_read["idLogs"],
            'idPlataforma' => $myrow_read["idPlataforma"],
            "idCliente" => $myrow_read["idCliente"],
            "intencion" => $myrow_read["intencion"],
            "porcentajeProximacion" => $myrow_read["porcentajeProximacion"],
            "mensajeEntrada" => $myrow_read["mensajeEntrada"],
            "respuesta" => $myrow_read["respuesta"],
            "lugar" => $myrow_read["lugar"],
            "fechaRegistro" => $myrow_read["fechaRegistro"]
        );
    }
    echo json_encode(array('success' => TRUE, 'data' => $arreglo, 'total' => $total));
} else
    echo json_encode(array('success' => true, 'data' => [], 'total' => $total));
$mysqli->close();
