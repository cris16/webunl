/* global Ext */

Ext.define('botWeb.model.Personnel', {
    extend: 'botWeb.model.Base',

    fields: [
        'name', 'email', 'phone'
    ]
});
