/* global Ext */
var MODULO_BASE_CONOCIMIENTO;
Ext.define('botWeb.view.baseConocimiento.c_BaseConocimiento', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.c_BaseConocimiento',
    onView: function (panelLoad) {
        MODULO_BASE_CONOCIMIENTO = panelLoad;
        MODULO_BASE_CONOCIMIENTO.down('[name=grid]').getStore().load();
        MODULO_BASE_CONOCIMIENTO.down('[name=btnEditar]').disable();
        MODULO_BASE_CONOCIMIENTO.down('[name=form]').down('[name=idEntidadUno]').getStore().proxy.extraParams = {idEntidadDefecto: "1"};
        MODULO_BASE_CONOCIMIENTO.down('[name=form]').down('[name=idEntidadDos]').getStore().proxy.extraParams = {idEntidadDefecto: "1,2"};
        MODULO_BASE_CONOCIMIENTO.down('[name=form]').down('[name=idEntidadTres]').getStore().proxy.extraParams = {idEntidadDefecto: "2,3"};
        if (document.body) {
            HEIGT_VIEWS = (document.body.clientHeight);
        } else {
            HEIGT_VIEWS = (window.innerHeight);

        }
        HEIGT_VIEWS = HEIGT_VIEWS - 65;
        MODULO_BASE_CONOCIMIENTO.setHeight(HEIGT_VIEWS);
        MODULO_BASE_CONOCIMIENTO.down('[name=grid]').setHeight(HEIGT_VIEWS);
        MODULO_BASE_CONOCIMIENTO.down('[name=form]').setHeight(HEIGT_VIEWS);
    },
    onRecargar: function () {
        MODULO_BASE_CONOCIMIENTO.down('[name=grid]').getStore().reload();
    },
    onLimpiarFormulario: function (btn, e) {
        if (btn.limpiar) {
            MODULO_BASE_CONOCIMIENTO.down('[name=form]').getForm().reset();
            var grid = MODULO_BASE_CONOCIMIENTO.down('[name=grid]');
            grid.getView().deselect(grid.getSelection());
        }
        MODULO_BASE_CONOCIMIENTO.down('[name=grid]').getStore().load();
        MODULO_BASE_CONOCIMIENTO.down('[name=btnEditar]').disable();
        MODULO_BASE_CONOCIMIENTO.down('[name=btnCrear]').enable();
    },
    onLimpiar: function (btn, e) {
        MODULO_BASE_CONOCIMIENTO.down('[name=txtParam]').reset();
        if (btn.limpiar) {
            MODULO_BASE_CONOCIMIENTO.down('[name=form]').getForm().reset();
            var grid = MODULO_BASE_CONOCIMIENTO.down('[name=grid]');
            grid.getView().deselect(grid.getSelection());
        }
        MODULO_BASE_CONOCIMIENTO.down('[name=grid]').getStore().load();
        MODULO_BASE_CONOCIMIENTO.down('[name=btnEditar]').disable();
        MODULO_BASE_CONOCIMIENTO.down('[name=btnCrear]').enable();
    },
    onBuscar: function (btn, e) {
        var txtParam = MODULO_BASE_CONOCIMIENTO.down('[name=txtParam]').getValue(), params = {}, me = this;
        params['param'] = txtParam;
        if (btn.xtype === 'button' || e.event.keyCode === 13) {
            me.onLimpiarFormulario({limpiar: true});
            MODULO_BASE_CONOCIMIENTO.down('[name=paginacionGrid]').moveFirst();
            MODULO_BASE_CONOCIMIENTO.down('[name=grid]').getStore().load({
                params: params,
                callback: function (records, operation, success) {
                    if (!success)
                        setMensajeGridEmptyText(MODULO_BASE_CONOCIMIENTO.down('[name=grid]'), '<h3>Problema en la comunicación con el servidor local, por favor inténtelo mas tarde.</h3>');
                    else if (records.length === 0)
                        setMensajeGridEmptyText(MODULO_BASE_CONOCIMIENTO.down('[name=grid]'), '<h3>No existen resultados.</h3>');
                }
            });
        }
    },
    onSelect: function (grid, selected, eOpts) {
        if (!MODULO_BASE_CONOCIMIENTO.down('[name=form]').down('[name=idEntidadUno]').getStore().getById(selected['data']['idEntidadUno'])) {
//            MODULO_BASE_CONOCIMIENTO.down('[name=form]').down('[name=idEntidadUno]').getStore().proxy.extraParams = {idEntidadDefecto: "1", idEntidad: ""};
            MODULO_BASE_CONOCIMIENTO.down('[name=form]').down('[name=idEntidadUno]').getStore().reload({
                params: {idEntidad: selected['data']['idEntidadUno']},
                callback: function (records) {
                    if (records.length > 0) {
                        MODULO_BASE_CONOCIMIENTO.down('[name=form]').down('[name=idEntidadUno]').setValue(selected['data']['idEntidadUno']);
                    }
                }
            });
        }
        if (!MODULO_BASE_CONOCIMIENTO.down('[name=form]').down('[name=idEntidadDos]').getStore().getById(selected['data']['idEntidadDos'])) {
//            MODULO_BASE_CONOCIMIENTO.down('[name=form]').down('[name=idEntidadUno]').getStore().proxy.extraParams = {idEntidadDefecto: "1", idEntidad: ""};
            MODULO_BASE_CONOCIMIENTO.down('[name=form]').down('[name=idEntidadDos]').getStore().reload({
                params: {idEntidad: selected['data']['idEntidadDos']},
                callback: function (records) {
                    if (records.length > 0) {
                        MODULO_BASE_CONOCIMIENTO.down('[name=form]').down('[name=idEntidadDos]').setValue(selected['data']['idEntidadDos']);
                    }
                }
            });
        }
        if (!MODULO_BASE_CONOCIMIENTO.down('[name=form]').down('[name=idEntidadTres]').getStore().getById(selected['data']['idEntidadTres'])) {
//            MODULO_BASE_CONOCIMIENTO.down('[name=form]').down('[name=idEntidadUno]').getStore().proxy.extraParams = {idEntidadDefecto: "1", idEntidad: ""};
            MODULO_BASE_CONOCIMIENTO.down('[name=form]').down('[name=idEntidadTres]').getStore().reload({
                params: {idEntidad: selected['data']['idEntidadTres']},
                callback: function (records) {
                    if (records.length > 0) {
                        MODULO_BASE_CONOCIMIENTO.down('[name=form]').down('[name=idEntidadTres]').setValue(selected['data']['idEntidadTres']);
                    }
                }
            });
        }

        MODULO_BASE_CONOCIMIENTO.down('[name=form]').getForm().loadRecord(selected);
        MODULO_BASE_CONOCIMIENTO.down('[name=btnEditar]').enable();
        MODULO_BASE_CONOCIMIENTO.down('[name=btnCrear]').disable();
    },
    onCrear: function () {
        var me = this, form = MODULO_BASE_CONOCIMIENTO.down('[name=form]').getForm();
        if (form.isValid()) {
            MODULO_BASE_CONOCIMIENTO.down('[name=grid]').getStore().insert(0, form.getValues());
            MODULO_BASE_CONOCIMIENTO.down('[name=grid]').getStore().sync({
//                callback: function (response, operation, success) {
//                    onProcesarPeticion(response, me.onLimpiarFormulario({limpiar: true}));
//                },
                success: function (response, options) {
                    notificaciones("Registro creado", 'Mensaje', 1);
                    me.onLimpiarFormulario({limpiar: true});
                },
                failure: function (response, options) {
                    notificaciones("Algo ocurrio, intene más tarde", 'Mensaje', 2);
                    me.onLimpiarFormulario({limpiar: true});
                }
            });
        } else
            mensajesValidacionForms(form.getFields());
    },
    onEditar: function () {
        var me = this, form = MODULO_BASE_CONOCIMIENTO.down('[name=form]').getForm();
        if (form.isValid()) {
            form.updateRecord(form.activeRecord);
            MODULO_BASE_CONOCIMIENTO.down('[name=grid]').getStore().sync({
//                callback: function (response, operation, success) {
//                    onProcesarPeticion(response, me.onLimpiarFormulario({limpiar: true}));
//                },
                success: function (response, options) {
                    notificaciones("Cambios realizados", 'Mensaje', 1);
                    me.onLimpiarFormulario({limpiar: true});
                },
                failure: function (response, options) {
                    notificaciones("Algo ocurrio, intene más tarde", 'Mensaje', 2);
                    me.onLimpiarFormulario({limpiar: true});
                }
            });
        } else
            mensajesValidacionForms(form.getFields());
    },
    cargarToolTip: function (c) {
        Ext.create('Ext.tip.ToolTip', {
            target: c.getEl(),
            html: c.tip
        });
    },
    onAgregarElementos: function () {
        MODULO_BASE_CONOCIMIENTO.down('[name=form]').down('[name=gridEntrenamiento]').getStore().add(new Ext.data.Record({text: "", habilitado: 1, nuevo: true}));
    },
    onDeleteEntrenamiento: function (grid, rowIndex, colIndex) {
        var ventana = MODULO_BASE_CONOCIMIENTO.down('[name=form]');
        var gridPerfil = ventana.down('[name=gridEntrenamiento]');
        var rec = grid.getStore().getAt(rowIndex);
        if (rec.data.nuevo)
            gridPerfil.getStore().remove(rec);
        else {
            rec.set('actualizar', true);
            if (rec.data.habilitado)
                rec.set('habilitado', 0);
            else
                rec.set('habilitado', 1);
        }
    }
});