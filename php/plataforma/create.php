<?php

include '../../dll/config.php';
$data = json_decode(file_get_contents('php://input'));
if (isset($data)) {
    if (!$mysqli = getConectionDb()) {
        return;
    }

    $habilitado = 0;
    $idParametro = 0;
    if (isset($data->idParametro) && is_int($data->idParametro)) {
        $idParametro = $data->idParametro;
    }
    if (isset($data->habilitado)) {
        if ((bool) $data->habilitado) {
            $habilitado = 1;
        }
    }

    $sql_create_Intencion = "INSERT INTO botUnl.plataforma (idPlataforma, nombre, idTipoPlataforma, clave, version, versionPlataforma, detalle, idUsuarioRegistro) "
            . " VALUES "
            . "('" . $data->idPlataforma . "', '" . $data->plataforma . "', '" . $data->idTipoPlataforma . "', '123456789', '0','" . number_format($data->versionPlataforma, 1, '.', '') . "' ,'" . $data->credenciales . "' ,'" . $_SESSION["idUsuario"] . "')";
    echo json_encode(EJECUTAR_SQL($mysqli, $sql_create_Intencion));
} else {
    echo json_encode(array('success' => false, 'message' => "FALTAN PARÁMETROS"));
}
$mysqli->close();
