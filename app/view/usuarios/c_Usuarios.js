/* global Ext */
var MODULO_USUARIOS, RESPUESTA_USUARIO;
Ext.define('botWeb.view.usuarios.c_Usuarios', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.c_Usuarios',
    onView: function (panelLoad) {
        MODULO_USUARIOS = panelLoad;
        MODULO_USUARIOS.down('[name=grid]').getStore().load();
        MODULO_USUARIOS.down('[name=btnEditar]').disable();
        if (document.body) {
            HEIGT_VIEWS = (document.body.clientHeight);
        } else {
            HEIGT_VIEWS = (window.innerHeight);

        }
        HEIGT_VIEWS = HEIGT_VIEWS - 65;
        MODULO_USUARIOS.setHeight(HEIGT_VIEWS);
        MODULO_USUARIOS.down('[name=grid]').setHeight(HEIGT_VIEWS);
        MODULO_USUARIOS.down('[name=form]').setHeight(HEIGT_VIEWS);
    },
    onShow: function (choice) {
        setCookie("ID_MODULO", MODULO_USUARIOS.xtype, 1);
    },
    onRecargar: function () {
        MODULO_USUARIOS.down('[name=grid]').getStore().reload();
    },
    onLimpiarFormulario: function (btn, e) {
        if (btn.limpiar) {
            MODULO_USUARIOS.down('[name=form]').getForm().reset();
            var grid = MODULO_USUARIOS.down('[name=grid]');
            grid.getView().deselect(grid.getSelection());
        }
        MODULO_USUARIOS.down('[name=grid]').getStore().load();
        MODULO_USUARIOS.down('[name=btnEditar]').disable();
        MODULO_USUARIOS.down('[name=btnCrear]').enable();
    },
    onLimpiar: function (btn, e) {
        MODULO_USUARIOS.down('[name=txtParam]').reset();
        if (btn.limpiar) {
            MODULO_USUARIOS.down('[name=form]').getForm().reset();
            var grid = MODULO_USUARIOS.down('[name=grid]');
            grid.getView().deselect(grid.getSelection());
        }
        MODULO_USUARIOS.down('[name=grid]').getStore().load();
        MODULO_USUARIOS.down('[name=btnEditar]').disable();
        MODULO_USUARIOS.down('[name=btnCrear]').enable();
    },
    onBuscar: function (btn, e) {
        var txtParam = MODULO_USUARIOS.down('[name=txtParam]').getValue(), params = {}, me = this;
        params['param'] = txtParam;
        if (btn.xtype === 'button' || e.event.keyCode === 13) {
            me.onLimpiarFormulario({limpiar: true});
            MODULO_USUARIOS.down('[name=paginacionGrid]').moveFirst();
            MODULO_USUARIOS.down('[name=grid]').getStore().load({
                params: params,
                callback: function (records, operation, success) {
                    if (!success)
                        setMensajeGridEmptyText(MODULO_USUARIOS.down('[name=grid]'), '<h3>Problema en la comunicación con el servidor local, por favor inténtelo mas tarde.</h3>');
                    else if (records.length === 0)
                        setMensajeGridEmptyText(MODULO_USUARIOS.down('[name=grid]'), '<h3>No existen resultados.</h3>');
                }
            });
        }
    },
    onSelect: function (grid, selected, eOpts) {
        if (!MODULO_USUARIOS.down('[name=form]').down('[name=rol]').getStore().getById(selected['data']['rol'])) {
            MODULO_USUARIOS.down('[name=form]').down('[name=rol]').getStore().proxy.extraParams = {param: ""};
            MODULO_USUARIOS.down('[name=form]').down('[name=rol]').getStore().reload({
                params: {idIntencionTipo: selected['data']['rol']},
                callback: function (records) {
                    if (records.length > 0) {
                        MODULO_USUARIOS.down('[name=form]').down('[name=rol]').setValue(selected['data']['rol']);
                    }
                }
            });
        }

        MODULO_USUARIOS.down('[name=form]').getForm().loadRecord(selected);
        MODULO_USUARIOS.down('[name=btnEditar]').enable();
        MODULO_USUARIOS.down('[name=btnCrear]').disable();
    },
    onCrear: function () {
        var me = this, form = MODULO_USUARIOS.down('[name=form]').getForm();
        if (form.isValid()) {
            MODULO_USUARIOS.down('[name=grid]').getStore().insert(0, form.getValues());
            MODULO_USUARIOS.down('[name=grid]').getStore().sync({
//                callback: function (response, operation, success) {
//                    onProcesarPeticion(response, me.onLimpiarFormulario({limpiar: true}));
//                },
                success: function (response, options) {
                    notificaciones("Registro creado", 'Mensaje', 1);
                    me.onLimpiarFormulario({limpiar: true});
                },
                failure: function (response, options) {
                    notificaciones("Algo ocurrio, intene más tarde", 'Mensaje', 2);
                    me.onLimpiarFormulario({limpiar: true});
                }
            });
        } else
            mensajesValidacionForms(form.getFields());
    },
    onEditar: function () {
        var me = this, form = MODULO_USUARIOS.down('[name=form]').getForm();
        if (form.isValid()) {
            form.updateRecord(form.activeRecord);
            MODULO_USUARIOS.down('[name=grid]').getStore().sync({
                success: function (response, options) {
                    notificaciones("Cambios realizados", 'Mensaje', 1);
                    me.onLimpiarFormulario({limpiar: true});
                },
                failure: function (response, options) {
                    notificaciones("Algo ocurrio, intene más tarde", 'Mensaje', 2);
                    me.onLimpiarFormulario({limpiar: true});
                }
//                callback: function (response, operation, success) {
//                    console.log("response->callback", response, operation, success);
//                    onProcesarPeticion(response, me.onLimpiarFormulario({limpiar: true}));
//                },
//                scope: function (res) {
//                     console.log("response->scope", res);
//                }
            });
        } else
            mensajesValidacionForms(form.getFields());
    },
    cargarToolTip: function (c) {
        Ext.create('Ext.tip.ToolTip', {
            target: c.getEl(),
            html: c.tip
        });
    },
    onAgregarElementos: function () {
        MODULO_USUARIOS.down('[name=form]').down('[name=gridEntrenamiento]').getStore().add(new Ext.data.Record({text: "", habilitado: 1, nuevo: true}));
    },
    onDeleteEntrenamiento: function (grid, rowIndex, colIndex) {
        var ventana = MODULO_USUARIOS.down('[name=form]');
        var gridPerfil = ventana.down('[name=gridEntrenamiento]');
        var rec = grid.getStore().getAt(rowIndex);
        if (rec.data.nuevo)
            gridPerfil.getStore().remove(rec);
        else {
            rec.set('actualizar', true);
            if (rec.data.habilitado)
                rec.set('habilitado', 0);
            else
                rec.set('habilitado', 1);
        }
    }
});