/* global Ext */

Ext.define('botWeb.store.stores.s_ElementoAtachmen', {
    extend: 'Ext.data.ArrayStore',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'titulo', type: 'string'},
        {name: 'subTitulo', type: 'string'},
        {name: 'botones', type: 'string'},
        {name: 'urlImg', type: 'string'}
    ],
    data: [
       
    ]
});