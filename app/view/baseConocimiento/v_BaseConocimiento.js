/* global Ext, opcionesCheck, INFOMESSAGEBLANKTEXT, formatEstadoRegistro */

/**
 * This view is an example list of people.
 */
STORE_USUARIOS = Ext.create('botWeb.store.baseConocimiento.s_BaseConocimiento');
Ext.define('botWeb.view.baseConocimiento.v_BaseConocimiento', {
    extend: 'Ext.panel.Panel',
    xtype: 'baseConocimiento',
    controller: 'c_BaseConocimiento',
//    title: 'intencion',
    requires: [
        'Ext.layout.container.Border',
        'Ext.selection.CellModel',
        'botWeb.view.baseConocimiento.c_BaseConocimiento'
    ],
    layout: 'border',
    bodyBorder: false,
    defaults: {
        collapsible: true,
        collapsed: false,
        split: true,
        bodyPadding: 0
    },
    listeners: {
        afterrender: 'onView'
    },
    items: [
        {
            xtype: 'panel',
            region: 'center',
            width: '70%',
            collapseMode: 'mini',
            margin: 1,
            header: false,
//            collapsible: true,
            collapsed: false,
            collapsible: false,
//            layout: 'fit',
            items: [
                {
                    xtype: 'grid',
                    name: 'grid',
                    plugins: [{ptype: 'gridfilters'}],
                    bufferedRenderer: false,
                    store: STORE_USUARIOS,
                    viewConfig: {
                        deferEmptyText: false,
                        enableTextSelection: true,
                        preserveScrollOnRefresh: true,
                        listeners: {
                            loadingText: 'Cargando...'
                        }, loadMask: true,
                        emptyText: '<center><h1 style="margin:20px">No existen resultados</h1></center>'
                    },
                    tbar: [
                        {
                            xtype: 'textfield',
                            name: 'txtParam',
                            flex: 2,
                            emptyText: 'Nombre, Apellido, usuario..',
                            tip: 'Buscar por el nombre de la intención o descripción',
                            listeners: {
                                specialkey: 'onBuscar',
                                render: 'cargarToolTip'
                            }
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-search',
                            tooltip: 'Buscar',
                            handler: 'onBuscar'
                        }, {
                            xtype: 'button',
                            iconCls: 'x-fa fa-eraser',
                            tooltip: 'Limpiar buscador',
                            handler: 'onLimpiar'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fas fa-sync',
                            tooltip: 'Recargar',
                            handler: 'onRecargar'
                        }
                    ],
                    features: [
                        {
                            ftype: 'grouping',
                            groupHeaderTpl: '{name}',
                            hideGroupedHeader: true,
                            enableGroupingMenu: true
                        }
                    ],
                    columns: [
                        Ext.create('Ext.grid.RowNumberer', {header: '#', width: 40, align: 'center'}),
                        {text: 'Nivel 1', dataIndex: 'entidadUno', tooltip: "Nivel 1", filter: true, sortable: true, flex: 1},
                        {text: 'Nivel 2', dataIndex: 'entidadDos', tooltip: "Nivel 2", filter: true, sortable: true, flex: 1},
                        {text: 'Nivel 3', dataIndex: 'entidadTres', tooltip: "Nivel 3", filter: true, sortable: true, flex: 1},
                        {text: 'Articulo', dataIndex: 'articulo', tooltip: "Articulo", filter: true, sortable: true, flex: 0.5},
                        {text: 'Texto', dataIndex: 'text', filter: true, sortable: true, flex: 2},
                        {filter: true, tooltip: "Habilitado", text: "Habilitado", width: 100, dataIndex: 'habilitado', sortable: true, renderer: formatEstadoRegistro}
                    ],
                    listeners: {
                        select: 'onSelect'
                    },
                    bbar: Ext.create('Ext.PagingToolbar', {
                        store: STORE_USUARIOS,
                        displayInfo: true,
                        name: 'paginacionGrid',
                        emptyMsg: "Sin datos que mostrar.",
                        displayMsg: 'Visualizando {0} - {1} de {2} registros',
                        beforePageText: 'Página',
                        afterPageText: 'de {0}',
                        firstText: 'Primera página',
                        prevText: 'Página anterior',
                        nextText: 'Siguiente página',
                        lastText: 'Última página',
                        refreshText: 'Actualizar',
                        inputItemWidth: 35,
                        items: [
                            {
//                                    xtype: 'button',
//                                    text: 'Exportar',
//                                    iconCls: 'x-fa fa-download',
//                                    handler: function (btn) {
//                                        onExportar(btn, "Sanciones", this.up('grid'));
//                                    }
                            }
                        ],
                        listeners: {
                            afterrender: function () {
                                this.child('#refresh').hide();
                            }
                        }
                    })
                }
            ]
        },
        {
            xtype: 'panel',
            region: 'east',
            padding: 5,
            width: '30%',
            collapseMode: 'mini',
            anchor: '100% 100%',
            layout: 'anchor',
            cls: 'panelCrearEditar',
            autoScroll: true,
            collapsible: true,
            collapsed: false,
            header: false,
            headerAsText: false,
            items: [
                {
                    xtype: 'form',
//                    title: 'Tipo de Usuarioses',
                    name: 'form',
                    cls: 'quick-graph-panel shadow panelComplete',
                    ui: 'light',
                    padding: 5,
                    defaults: {
                        width: '100%',
                        margin: '0 0 5 0',
                        defaultType: 'textfield',
//                        minLengthText: MINIMUMMESSAGUEREQUERID,
//                        maxLengthText: MAXIMUMMESSAGUEREQURID,
//                        afterLabelTextTpl: INFOMESSAGEREQUERID,
                        blankText: INFOMESSAGEBLANKTEXT,
                        labelWidth: 90,
                        allowOnlyWhitespace: false,
                        allowBlank: false
                    },
                    items: [
                        {
                            xtype: "combobox",
                            fieldLabel: "Nivel 1",
                            value: 0,
                            width: "100%",
                            tooltip: "Seleccionar el tipo de rol",
                            name: "idEntidadUno",
                            emptyText: "Seleccione",
                            reference: "text",
                            displayField: "text",
                            valueField: "id",
                            filterPickList: true,
                            queryParam: "param",
                            queryMode: "remote",
                            growMax: 20,
//                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                            allowOnlyWhitespace: false,
//                                blankText: INFOMESSAGEBLANKTEXT,
                            store: Ext.create('botWeb.store.stores.s_Entidad')

                        },
                        {
                            xtype: "combobox",
                            fieldLabel: "Nivel 2",
                            value: 0,
                            width: "100%",
                            tooltip: "Seleccionar el tipo de rol",
                            name: "idEntidadDos",
                            emptyText: "Seleccione",
                            reference: "text",
                            displayField: "text",
                            valueField: "id",
                            filterPickList: true,
                            queryParam: "param",
                            queryMode: "remote",
                            growMax: 20,
//                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                            allowOnlyWhitespace: false,
//                                blankText: INFOMESSAGEBLANKTEXT,
                            store: Ext.create('botWeb.store.stores.s_Entidad')

                        },
                        {
                            xtype: "combobox",
                            fieldLabel: "Nivel 3",
                            value: 0,
                            width: "100%",
                            tooltip: "Seleccionar el tipo de rol",
                            name: "idEntidadTres",
                            emptyText: "Seleccione",
                            reference: "text",
                            displayField: "text",
                            valueField: "id",
                            filterPickList: true,
                            queryParam: "param",
                            queryMode: "remote",
                            growMax: 20,
//                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                            allowOnlyWhitespace: false,
//                                blankText: INFOMESSAGEBLANKTEXT,
                            store: Ext.create('botWeb.store.stores.s_Entidad')

                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Articulo',
                            name: 'articulo',
                            minLength: 3,
                            maxLength: 45
                        },
                        {
                            xtype: 'textarea',
                            fieldLabel: 'Mensaje',
                            name: 'text',
                            minLength: 3,
                            flex: 1,
                            hideLabel: true,
                            height: '60%',
//                            maxLength: 1500,
                            emptyText: 'Mensaje a Enviar al usaurio',
                        },
                        {
                            xtype: 'textarea',
                            fieldLabel: 'Ayuda',
                            name: 'ayuda',
                            minLength: 3,
                            maxLength: 200,
                            emptyText: 'Mensaje de referencia',
                            hideLabel: true
                        },
                        {
                            xtype: 'checkboxgroup',
                            allowBlank: true,
                            allowOnlyWhitespace: true,
                            items: [
                                {xtype: 'checkboxfield', uncheckedValue: 0, name: 'habilitado', boxLabel: 'Habilitado', inputValue: 1}
                            ]
                        }
                    ], dockedItems: [{
                            ui: 'footer',
                            xtype: 'toolbar',
                            dock: 'bottom',
                            defaults: {
                                width: '25%',
                                height: 30
                            },
                            items: [
                                {
                                    text: 'Limpiar',
                                    tooltip: 'Limpiar',
                                    disabled: false,
                                    limpiar: true,
                                    handler: 'onLimpiarFormulario'
                                },
                                '->',
                                {
                                    text: 'Crear',
                                    tooltip: 'Crear',
                                    name: 'btnCrear',
                                    handler: 'onCrear',
                                    disabled: false
                                },
                                {
                                    text: 'Editar',
                                    tooltip: 'Editar',
                                    name: 'btnEditar',
                                    handler: 'onEditar',
                                    disabled: false
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ]
});
