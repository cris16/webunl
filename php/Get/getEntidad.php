<?php

include '../../dll/config.php';
//include '../../dll/funciones.php';
extract($_GET);
extract($_POST);
if (!$mysqli = getConectionDb())
    return $mysqli;
$sql = "SELECT idEntidad, nombre, nivel, concat('(',nivel,')',nombre)  as text, IF(e.habilitado = 1, 1, 0) as habilitado, fechaRegistro "
        . "FROM $DB_NAME.entidad e WHERE e.habilitado=1 ";

if (isset($idEntidadDefecto) && $idEntidadDefecto != '') {
    $sql .= " and e.nivel in ( $idEntidadDefecto)";
}
if (isset($idEntidad) && $idEntidad != '') {
    $sql .= " and e.idEntidad = $idEntidad ";
}

if (isset($param)) {
    $sql .= " AND (e.nombre like '%$param%') ";
}
$sql .= " ORDER BY nivel DESC";
if (isset($limite)) {
    $sql .= " LIMIT $limite";
} else {
    $sql .= " LIMIT $LIMITE_REGISTROS";
}
$result = $mysqli->query($sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS", 'sql' => $sql));
    return $mysqli->close();
}
$arreglo = [];
while ($myrow_read_users = $result->fetch_assoc()) {
    $arreglo[] = array(
        'id' => intval($myrow_read_users["idEntidad"]),
        'nombre' => ($myrow_read_users["nombre"]),
        'nivel' => ($myrow_read_users["nivel"]),
        'text' => ($myrow_read_users["text"]),
        'habilitado' => $myrow_read_users["habilitado"],
        'fechaRegistro' => $myrow_read_users["fechaRegistro"],
    );
}
$mysqli->close();
echo json_encode(array('success' => true, 'data' => $arreglo, 'sql' => $sql));

