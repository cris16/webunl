<?php

include '../../dll/config.php';
$data = json_decode(file_get_contents('php://input'));
if (isset($data)) {
    if (!$mysqli = getConectionDb()) {
        return;
    }

    $resAdicional = [];
    $habilitado = 0;
    $idParametro = 0;
    if (isset($data->idParametro) && is_int($data->idParametro)) {
        $idParametro = $data->idParametro;
    }
    if (isset($data->habilitado)) {
        if ((bool) $data->habilitado) {
            $habilitado = 1;
        }
    }

    $sql_create_Intencion = "INSERT INTO botUnl.intenciones (idIntencionTipo, nombre, parametro, text, idUsuarioRegistro, respuesta) "
            . " VALUES "
            . "('" . $data->idIntencionTipo . "', '" . $data->nombre . "', '" . $idParametro . "', '" . $data->text . "' ,'" . $_SESSION["idUsuario"] . "','" . $data->respuesta . "')";

    $res = EJECUTAR_SQL($mysqli, $sql_create_Intencion);
    if (isset($res['id']) && intval($res['id']) > 0) {
        if (isset($data->dataEntrenamiento)) {
            $dataEntrenamiento = json_decode($data->dataEntrenamiento);
            foreach ($dataEntrenamiento as $p) {
                $resAdicional[] = $p;
                $sql_ProyectoNuevo = "INSERT INTO botUnl.entrenamientoIntencion (idIntenciones, text, entidades, version, habilitado, idUsuarioRegistro) VALUES "
                        . " (" . $res['id'] . ", '" . $p->text . "', '[]',1, b'1', " . $_SESSION["idUsuario"] . ");";
                $resAdicional[] = EJECUTAR_SQL($mysqli, $sql_ProyectoNuevo);
            }
        }
    }
    $res['resAdicional'] = $resAdicional;
    echo json_encode($res);
//    echo json_encode(EJECUTAR_SQL($mysqli, $sql_create_Intencion));
} else {
    echo json_encode(array('success' => false, 'message' => "FALTAN PARÁMETROS"));
}
$mysqli->close();
