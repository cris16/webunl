<?php

include '../../dll/config.php';

$data = json_decode(file_get_contents('php://input'));
if (isset($data->id)) {
    if (!$mysqli = getConectionDb())
        return;
    $sql_update = "UPDATE botUnl.relacion SET ";
    $sql_update .= (isset($data->idEntidadUno)) ? "entidadUno = '$data->idEntidadUno', " : "";
    $sql_update .= (isset($data->idEntidadDos)) ? "entidadDos = '$data->idEntidadDos', " : "";
    $sql_update .= (isset($data->idEntidadTres)) ? "entidadTres = '$data->idEntidadTres', " : "";
    $sql_update .= (isset($data->text)) ? "text = '$data->text', " : "";
    $sql_update .= (isset($data->ayuda)) ? "ayuda = '$data->ayuda', " : "";
    $sql_update .= (isset($data->articulo)) ? "articulo = '$data->articulo', " : "";
    if (isset($data->habilitado)) {
        $sql_update .= (isset($data->habilitado)) ? " habilitado = b'" . (int) ($data->habilitado) . "', " : "";
    }
    $idRegistro = explode(",", $data->id);
    $sql_update .= 'idUsuarioActualizacion =' . $_SESSION["idUsuario"] . ', ';
    $sql_update .= 'fechaActualizacion = NOW() WHERE ';
    $sql_update .= ' entidadUno = ' . $idRegistro[0];
    $sql_update .= ' AND entidadDos = ' . $idRegistro[1];
    $sql_update .= ' AND entidadTres = ' . $idRegistro[2];
    $sql_update .= ' AND orden = ' . $idRegistro[3];

    echo json_encode(EJECUTAR_SQL($mysqli, $sql_update));
} else {
    echo json_encode(array('success' => false, 'message' => "FALTAN PARÁMETROS"));
}
$mysqli->close();
