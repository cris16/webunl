/* global Ext, opcionesCheck, INFOMESSAGEBLANKTEXT, formatEstadoRegistro */

/**
 * This view is an example list of people.
 */
STORE_USUARIOS = Ext.create('botWeb.store.plataforma.s_Plataforma');
Ext.define('botWeb.view.plataforma.v_Plataforma', {
    extend: 'Ext.panel.Panel',
    xtype: 'plataforma',
    controller: 'c_Plataforma',
    requires: [
        'Ext.layout.container.Border',
        'Ext.selection.CellModel',
        'botWeb.view.plataforma.c_Plataforma'
    ],
    layout: 'border',
    bodyBorder: false,
    defaults: {
        collapsible: true,
        collapsed: false,
        split: true,
        bodyPadding: 0
    },
    listeners: {
        afterrender: 'onView'
    },
    items: [
        {
            xtype: 'panel',
            region: 'center',
            width: '70%',
            collapseMode: 'mini',
            header: false,
             margin: 1,
            collapsed: false,
            collapsible: false,
            items: [
                {
                    xtype: 'grid',
                    name: 'grid',
                    plugins: [{ptype: 'gridfilters'}],
                    bufferedRenderer: false,
                    store: STORE_USUARIOS,
                    viewConfig: {
                        deferEmptyText: false,
                        enableTextSelection: true,
                        preserveScrollOnRefresh: true,
                        listeners: {
                            loadingText: 'Cargando...'
                        }, loadMask: true,
                        emptyText: '<center><h1 style="margin:20px">No existen resultados</h1></center>'
                    },
                    tbar: [
                        {
                            xtype: 'textfield',
                            name: 'txtParam',
                            flex: 2,
                            emptyText: 'Nombre',
                            tip: 'Buscar por el nombre de la intención o descripción',
                            listeners: {
                                specialkey: 'onBuscar',
                                render: 'cargarToolTip'
                            }
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-search',
                            tooltip: 'Buscar',
                            handler: 'onBuscar'
                        }, {
                            xtype: 'button',
                            iconCls: 'x-fa fa-eraser',
                            tooltip: 'Limpiar buscador',
                            handler: 'onLimpiar'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fas fa-sync',
                            tooltip: 'Recargar',
                            handler: 'onRecargar'
                        }
                    ],
                    features: [
                        {
                            ftype: 'grouping',
                            groupHeaderTpl: '{name}',
                            hideGroupedHeader: true,
                            enableGroupingMenu: true
                        }
                    ],
                    columns: [
                        Ext.create('Ext.grid.RowNumberer', {header: '#', width: 30, align: 'center'}),
                        {text: 'Plataforma', dataIndex: 'plataforma', tooltip: "Plataforma", filter: true, sortable: true, flex: 1},
                        {text: 'id Plataforma', dataIndex: 'id', tooltip: "id Plataforma", filter: true, sortable: true, flex: 1},
                        {text: 'Tipo Plataforma', dataIndex: 'tipoPlataforma', tooltip: "Tipo Plataforma", filter: true, sortable: true, flex: 1},
                        {text: 'Versión Plataforma', dataIndex: 'versionPlataforma', tooltip: "Versión Plataforma", filter: true, sortable: true, flex: 1},
//                        {text: 'Articulo', dataIndex: 'articulo', tooltip: "Articulo", filter: true, sortable: true, flex: 0.5},
                        {text: 'Detalle', dataIndex: 'credenciales', filter: true, sortable: true, flex: 2},
                        {text: 'Fecha Registro', dataIndex: 'fechaRegistro', filter: true,xtype: 'datecolumn', format: 'Y-m-d H:i:s', sortable: true, flex: 1},
                        {filter: true, tooltip: "Habilitado", text: "Habilitado", width: 100, dataIndex: 'habilitado', sortable: true, renderer: formatEstadoRegistro}
                    ],
                    listeners: {
                        select: 'onSelect'
                    },
                    bbar: Ext.create('Ext.PagingToolbar', {
                        store: STORE_USUARIOS,
                        displayInfo: true,
                        name: 'paginacionGrid',
                        emptyMsg: "Sin datos que mostrar.",
                        displayMsg: 'Visualizando {0} - {1} de {2} registros',
                        beforePageText: 'Página',
                        afterPageText: 'de {0}',
                        firstText: 'Primera página',
                        prevText: 'Página anterior',
                        nextText: 'Siguiente página',
                        lastText: 'Última página',
                        refreshText: 'Actualizar',
                        inputItemWidth: 35,
                        items: [
                            {
//                                    xtype: 'button',
//                                    text: 'Exportar',
//                                    iconCls: 'x-fa fa-download',
//                                    handler: function (btn) {
//                                        onExportar(btn, "Sanciones", this.up('grid'));
//                                    }
                            }
                        ],
                        listeners: {
                            afterrender: function () {
                                this.child('#refresh').hide();
                            }
                        }
                    })
                }
            ]
        },
        {
            xtype: 'panel',
            region: 'east',
            padding: 5,
            width: '30%',
            collapseMode: 'mini',
            anchor: '100% 100%',
            layout: 'anchor',
            cls: 'panelCrearEditar',
            autoScroll: true,
            collapsible: true,
            collapsed: false,
            header: false,
            headerAsText: false,
            items: [
                {
                    xtype: 'form',
//                    title: 'Tipo de Usuarioses',
                    name: 'form',
                    cls: 'quick-graph-panel shadow panelComplete',
                    ui: 'light',
                    padding: 5,
                    defaults: {
                        width: '100%',
                        margin: '0 0 5 0',
                        defaultType: 'textfield',
//                        minLengthText: MINIMUMMESSAGUEREQUERID,
//                        maxLengthText: MAXIMUMMESSAGUEREQURID,
//                        afterLabelTextTpl: INFOMESSAGEREQUERID,
                        blankText: INFOMESSAGEBLANKTEXT,
                        labelWidth: 90,
                        allowOnlyWhitespace: false,
                        allowBlank: false
                    },
                    items: [
                        {
                            xtype: "combobox",
                            fieldLabel: "Tipo Plataforma",
                            value: 0,
                            width: "100%",
                            tooltip: "Seleccionar un tipo",
                            name: "idTipoPlataforma",
                            emptyText: "Seleccione",
                            reference: "text",
                            displayField: "text",
                            valueField: "id",
                            filterPickList: true,
                            queryParam: "param",
                            queryMode: "remote",
                            growMax: 20,
//                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                            allowOnlyWhitespace: false,
//                                blankText: INFOMESSAGEBLANKTEXT,
                            store: Ext.create('botWeb.store.stores.s_TipoPlataforma')

                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Plataforma',
                            name: 'plataforma',
                            minLength: 3,
                            maxLength: 45
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'id Plataforma',
                            name: 'idPlataforma',
                            minLength: 3,
                            maxLength: 45
                        },
                        {
                            xtype: 'numberfield',
                            fieldLabel: 'version',
                            name: 'versionPlataforma',
                            decimalPrecision:1,
                            value: 9.0,
                            step:0.1,
                            minLength: 1.0,
                            maxLength: 10.0
                        },
                        {
                            xtype: 'textarea',
                            fieldLabel: 'Credenciales',
                            name: 'credenciales',
                            minLength: 3,
                            maxLength: 1500,
                            emptyText: 'CREDENCIALES DE LA PLATAFORMA EN JSON',
                            hideLabel: true
                        },
                        {
                            xtype: 'checkboxgroup',
                            allowBlank: true,
                            allowOnlyWhitespace: true,
                            items: [
                                {xtype: 'checkboxfield', uncheckedValue: 0, name: 'habilitado', boxLabel: 'Habilitado', inputValue: 1}
                            ]
                        }
                    ], dockedItems: [{
                            ui: 'footer',
                            xtype: 'toolbar',
                            dock: 'bottom',
                            defaults: {
                                width: '25%',
                                height: 30
                            },
                            items: [
                                {
                                    text: 'Limpiar',
                                    tooltip: 'Limpiar',
                                    disabled: false,
                                    limpiar: true,
                                    handler: 'onLimpiarFormulario'
                                },
                                '->',
                                {
                                    text: 'Crear',
                                    tooltip: 'Crear',
                                    name: 'btnCrear',
                                    handler: 'onCrear',
                                    disabled: false
                                },
                                {
                                    text: 'Editar',
                                    tooltip: 'Editar',
                                    name: 'btnEditar',
                                    handler: 'onEditar',
                                    disabled: false
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ]
});
