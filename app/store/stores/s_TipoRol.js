
/* global Ext */

Ext.define('botWeb.store.stores.s_TipoRol', {
    extend: 'Ext.data.ArrayStore',
    data: [
        ["1", 'Ceo'],
        ["2", 'Secretaria']
    ],
    fields: [
        {name: 'id'},
        {name: 'text'}
    ]
});