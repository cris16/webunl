/* global Ext, URL_SERVICIOS */

/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
var MODULO_MAIN;
Ext.define('botWeb.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',
    onView: function (panelLoad) {
        MODULO_MAIN = panelLoad;
        Ext.Ajax.request({
            url: 'php/login/validarPermiso.php',
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            success: function (response) {
                var estadoSeccion = {};
                if (response.responseText !== "") {
                    var res = Ext.util.JSON.decode(response.responseText);
                    estadoSeccion = res;
                }
                if (estadoSeccion['en'] == -1)
                    window.location = 'php/login/logout.php';
                else {
                    MODULO_MAIN['rolUser'] = estadoSeccion['rol'];
                    if (estadoSeccion['rol'] == 1) {
                        MODULO_MAIN.add([
                            {title: 'Intención', iconCls: 'fa-comment', items: [{xtype: 'intenciones'}]}, //<i class="fal fa-user-md-chat"></i>
                            {title: 'Administrador', iconCls: 'fa-users', items: [{xtype: 'usuarios'}]}, //usuarios
                            {title: 'Entidades', iconCls: 'fa-pen', items: [{xtype: 'entidades'}]}, //entidades
                            {title: 'Base de Conocimiento', iconCls: 'fa-book', items: [{xtype: 'baseConocimiento'}]}, //baseConocimiento
                            {title: 'Plataformas', iconCls: 'fa-cog', items: [{xtype: 'plataforma'}]}, //plataforma
                            {title: 'Logs', iconCls: 'fa-bug', items: [{xtype: 'logsUsuario'}]} //Se presentan los mensajes realizados al bot
                        ]);

                    } else if (estadoSeccion['rol'] == 2) {
                        MODULO_MAIN.add([
                            {title: 'Intención', iconCls: 'fa-comment', items: [{xtype: 'intenciones'}]},
                            {title: 'Entidades', iconCls: 'fa-cog', items: [{xtype: 'entidades'}]}, //entidades
                            {title: 'Base de Conocimiento', iconCls: 'fa-book', items: [{xtype: 'baseConocimiento'}]}, //baseConocimiento
                        ]);

                    }
                }
            }
        });
    },
    onAgregarRolcooki: function (rol) {
        var listaCookie = document.cookie.split(';');
        for (var i = 0; i < listaCookie.length; i++) {

        }
    },
    onSelectionchange: function (newActiveItem, thisS, oldActiveItem, eOpts) {
    },

    onConfirm: function (choice) {

    },
    onSalir: function () {
        Ext.MessageBox.confirm('Salir del Sistema', 'Desea salir del sistema?', this.showResult, this);
//        Ext.MessageBox.maskClickAction = this.getMaskClickAction();
    },
    onEntrenarModelo: function () {
        Ext.Ajax.request({
            url: URL_SERVICIOS + "entrenarModelo",
            method: 'POST',
//            contentType: "application/json; charset=utf-8",
            cors: true,
            useDefaultXhrHeader: false,
            withCredentials: true,
//            dataType: 'json',
            success: function (response) {
                var estadoSeccion = {};
                if (response.responseText !== "") {
                    var res = Ext.util.JSON.decode(response.responseText);
                    estadoSeccion = res;
                }
                if (res['m']) {
                    if (res['en'] == 1)
                        notificaciones(res['m'], 'Mensaje', 1);
                    else
                        notificaciones(res['m'], 'Mensaje', 2);
                }
            }});
    },
    onActualizarPlataforma: function () {
        Ext.Ajax.request({
            url: URL_SERVICIOS + "actualizarPlataforma",
            method: 'POST',
//            contentType: "application/json; charset=utf-8",
            cors: true,
            useDefaultXhrHeader: false,
            withCredentials: true,
//            dataType: 'json',
            success: function (response) {
                var estadoSeccion = {};
                if (response.responseText !== "") {
                    var res = Ext.util.JSON.decode(response.responseText);
                    estadoSeccion = res;
                }
                if (res['m']) {
                    if (res['en'] == 1)
                        notificaciones(res['m'], 'Mensaje', 1);
                    else
                        notificaciones(res['m'], 'Mensaje', 2);
                }
            }});
    },
    showResult: function (btn, text) {
        if (btn === 'yes') {
            setCookie("ID_MODULO", "", 1);
            window.location = 'php/login/logout.php';
        }
    }, getMaskClickAction: function () {
        return this.lookupReference('hideOnMaskClick').getValue() ? 'hide' : 'focus';
    }
});
