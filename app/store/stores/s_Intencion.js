/* global Ext */

Ext.define('botWeb.store.stores.s_Intencion', {
    extend: 'Ext.data.Store',
//    storeId: 's_TipoIntencion',
    proxy: {
        type: 'ajax',
        url: 'php/Get/getIntencion.php',
        method: 'GET',
        reader: {
            type: 'json',
            rootProperty: 'data'
        }
    },
    fields: [
        {name: 'id', type: 'int'},
        {name: 'text'}
    ]
});