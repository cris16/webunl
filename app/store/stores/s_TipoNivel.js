
/* global Ext */

Ext.define('botWeb.store.stores.s_TipoNivel', {
    extend: 'Ext.data.ArrayStore',
    data: [
        ["1", 'Nivel 1'],
        ["2", 'Nivel 2'],
        ["3", 'Nivel 3']
    ],
    fields: [
        {name: 'id'},
        {name: 'text'}
    ]
});