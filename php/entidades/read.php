<?php

include '../../dll/config.php';
if (!$mysqli = getConectionDb()) {
    return $mysqli;
}
extract($_GET);

$sql = "SELECT count(*) as total FROM botUnl.entidad i ";
if (isset($param)) {
    $sql .= " LEFT JOIN botUnl.entidadSinonimo es on es.idEntidad=i.idEntidad ";
    $sql .= " WHERE (i.nombre like '%$param%' ) OR (es.nombre like '%$param%')";
}

$result = $mysqli->query($sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
    return $mysqli->close();
}
$myrow = $result->fetch_assoc();
$total = $myrow['total'];
if ($total > 0) {
    $sql = "SELECT i.idEntidad, i.nombre, i.nivel, IF(i.habilitado = 1, 1, 0) as habilitado,  DATE_FORMAT(CONVERT_TZ(i.fechaRegistro, @@session.time_zone, '+00:00'), '%Y-%m-%dT%H:%i:%s.000Z') AS fechaRegistro "
            . " FROM botUnl.entidad i ";
    if (isset($param)) {
        $sql .= " LEFT JOIN botUnl.entidadSinonimo es on es.idEntidad=i.idEntidad ";
        $sql .= " WHERE (i.nombre like '%$param%' ) OR (es.nombre like '%$param%')  ";
    }
    if (isset($limit)) {
        $inicio = intval($limit) * (intval($page) - 1);
        $sql .= " LIMIT $inicio, $limit ";
    } else {
        $sql .= " LIMIT $LIMITE_REGISTROS";
    }
    $result = $mysqli->query($sql);
    if (!isset($result->num_rows)) {
        echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
        return $mysqli->close();
    }
    $arreglo = [];
    while ($myrow_read = $result->fetch_assoc()) {
        $arreglo[] = array(
            'id' => intval($myrow_read["idEntidad"]),
            'nombre' => ($myrow_read["nombre"]),
            'nivel' => ($myrow_read["nivel"]),
            'habilitado' => ($myrow_read["habilitado"]),
            'fechaRegistro' => ($myrow_read["fechaRegistro"]),
        );
    }
    echo json_encode(array('success' => TRUE, 'data' => $arreglo, 'total' => $total));
} else
    echo json_encode(array('success' => true, 'data' => [], 'total' => $total));
$mysqli->close();
