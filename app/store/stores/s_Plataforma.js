/* global Ext */

Ext.define('botWeb.store.stores.s_Plataforma', {
    extend: 'Ext.data.Store',
//    storeId: 's_TipoIntencion',
    proxy: {
        type: 'ajax',
        url: 'php/Get/getPlataforma.php',
        method: 'GET',
        reader: {
            type: 'json',
            rootProperty: 'data'
        }
    },
    fields: [
        {name: 'id', type: 'int'},
        {name: 'text'}
    ]
});