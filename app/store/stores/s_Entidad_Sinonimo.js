/* global Ext */

Ext.define('botWeb.store.stores.s_Entidad_Sinonimo', {
    extend: 'Ext.data.ArrayStore',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'idIntencion', type: 'int'},
        {name: 'text', type: 'string'},
        {name: 'habilitado', type: 'int'},
        {name: 'fechaRegistro'}
    ],
    proxy: {
        type: 'ajax',
        url: 'php/Get/getEntidadSinonimo.php',
        method: 'GET',
        reader: {
            rootProperty: 'data',
            totalProperty: 'total',
            successProperty: 'success',
            type: 'json'
        }
    },
    data: [

    ]
});