<?php

include '../../dll/config.php';
//include '../../dll/funciones.php';
extract($_GET);
extract($_POST);
if (!$mysqli = getConectionDb())
    return $mysqli;
$sql = "SELECT idEntrenamientoIntencion,idIntenciones,text,habilitado,fechaRegistro "
        . "FROM $DB_NAME.entrenamientoIntencion p WHERE TRUE";

if (isset($idIntenciones) && $idIntenciones != '') {
    $sql .= " and p.idIntenciones = $idIntenciones ";
}

if (isset($limite)) {
    $sql .= " LIMIT $limite";
} else {
    $sql .= " LIMIT $LIMITE_REGISTROS";
}
$result = $mysqli->query($sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS", 'sql' => $sql));
    return $mysqli->close();
}
$arreglo = [];
while ($myrow_read_users = $result->fetch_assoc()) {
    $arreglo[] = array(
        'id' => intval($myrow_read_users["idEntrenamientoIntencion"]),
        'idIntenciones' => intval($myrow_read_users["idIntenciones"]),
        'text' => $myrow_read_users["text"],
        'habilitado' => $myrow_read_users["habilitado"],
        'fechaRegistro' => $myrow_read_users["fechaRegistro"],
    );
}
$mysqli->close();
echo json_encode(array('success' => true, 'data' => $arreglo));

