/* global Ext */

Ext.define('botWeb.store.stores.s_Entidad', {
    extend: 'Ext.data.ArrayStore',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'text', type: 'string'},
        {name: 'nombre', type: 'string'},
        {name: 'habilitado'},
        {name: 'nivel'},
        {name: 'fechaRegistro'}
    ],
    proxy: {
        type: 'ajax',
        url: 'php/Get/getEntidad.php',
        method: 'GET',
        reader: {
            rootProperty: 'data',
            totalProperty: 'total',
            successProperty: 'success',
            type: 'json'
        }
    },
    data: [

    ]
});