/* global Ext */

Ext.define('botWeb.store.stores.s_Intencion_Entrenamiento', {
    extend: 'Ext.data.ArrayStore',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'idIntencion', type: 'int'},
        {name: 'text', type: 'string'},
        {name: 'habilitado', type: 'int'},
        {name: 'fechaRegistro'},
        {name: 'actualizar', defaultValue: false},
        {name: 'nuevo', defaultValue: false}
    ],
    proxy: {
        type: 'ajax',
        url: 'php/Get/getIntencionEntrenamiento.php',
        method: 'GET',
        reader: {
            rootProperty: 'data',
            totalProperty: 'total',
            successProperty: 'success',
            type: 'json'
        }
    },
    data: [

    ]
});