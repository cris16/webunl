/* global Ext, opcionesCheck, INFOMESSAGEBLANKTEXT, formatEstadoRegistro */

/**
 * This view is an example list of people.
 */
STORE_ENTIDADES = Ext.create('botWeb.store.entidades.s_Entidades');
Ext.define('botWeb.view.entidades.v_Entidades', {
    extend: 'Ext.panel.Panel',
    xtype: 'entidades',
    controller: 'c_Entidades',
//    title: 'intencion',
    requires: [
        'Ext.layout.container.Border',
        'Ext.selection.CellModel',
        'botWeb.view.entidades.c_Entidades'
    ],
    layout: 'border',
    bodyBorder: false,
    defaults: {
        collapsible: true,
        collapsed: false,
        split: true,
        bodyPadding: 0
    },
    listeners: {
        afterrender: 'onView'
    },
    items: [
        {
            xtype: 'window',
            layaut: 'anchor',
            title: 'Sinónimos',
            name: 'ventanaSinonimo',
            resizable: true,
            closable: true,
            width: '50%',
            height: '82%',
            autoScroll: true,
            closeAction: 'hide',
            closeToolText: 'Permite cerrar la ventana',
            constrain: true,
            draggable: true,
            collapsible: false,
            modal: true,
            scope: this,
            items: [
                {
                    height: '50%',
                    xtype: 'grid',
                    region: 'center',
                    autoScroll: true,
                    bufferedRenderer: false,
                    anchor: '100% 100%',
                    name: 'gridSinonimo',
                    columnLines: true,
                    selType: 'checkboxmodel',
                    minHeight: 120,
                    maxHeight: Ext.getBody().getViewSize().height - 50,
                    store: Ext.create('botWeb.store.stores.s_listaSinonimo'),
                    tbar: [
                        {
                            xtype: 'textfield',
                            flex: 4,
                            name: 'txtSinonimo',
                            emptyText: 'Escribir Sinónimo',
                            tip: 'Buscar Sinonimos de la palabra'
                        },
                        {
                            xtype: 'button',
                            text: 'Buscar Sinónimo',
                            name: 'agregarSinonimo',
                            flex: 2,
                            iconCls: 'x-fa fa-plus',
                            handler: 'onBuscarSinonimo'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'fas fa-times-circle',
                            iconAlign: 'right',
                            flex: 1,
                            text: 'Guardar',
                            tooltip: 'Guardar',
                            style: {
                                border: '1px solid #36beb3',
                                '-webkit-border-radius': '5px 5px',
                                '-moz-border-radius': '5px 5px'
                            },
                            handler: 'onGuardar'
                        }
                    ],
                    columns: [
                        {header: 'Nombre ', flex: 2, dataIndex: 'text', sortable: true}
                    ]
                }
            ]
        },
        {
            xtype: 'panel',
            region: 'center',
            width: '70%',
            collapseMode: 'mini',
            margin: 1,
            header: false,
//            collapsible: true,
            collapsed: false,
            collapsible: false,
//            layout: 'fit',
            items: [
                {
                    xtype: 'grid',
                    name: 'grid',
                    plugins: [{ptype: 'gridfilters'}],
                    bufferedRenderer: false,
                    store: STORE_ENTIDADES,
                    viewConfig: {
                        deferEmptyText: false,
                        enableTextSelection: true,
                        preserveScrollOnRefresh: true,
                        listeners: {
                            loadingText: 'Cargando...'
                        }, loadMask: true,
                        emptyText: '<center><h1 style="margin:20px">No existen resultados</h1></center>'
                    },
                    tbar: [{
                            xtype: 'textfield',
                            name: 'txtParam',
                            flex: 2,
                            emptyText: 'Nombre, ...',
                            tip: 'Buscar por el nombre de la entidad ',
                            listeners: {
                                specialkey: 'onBuscar',
                                render: 'cargarToolTip'
                            }
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-search',
                            tooltip: 'Buscar',
                            handler: 'onBuscar'
                        }, {
                            xtype: 'button',
                            iconCls: 'x-fa fa-eraser',
                            tooltip: 'Limpiar buscador',
                            handler: 'onLimpiar'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fas fa-sync',
                            tooltip: 'Recargar',
                            handler: 'onRecargar'
                        }
                    ],
                    features: [
                        {
                            ftype: 'grouping',
                            groupHeaderTpl: '{name}',
                            hideGroupedHeader: true,
                            enableGroupingMenu: true
                        }
                    ],
                    columns: [
                        Ext.create('Ext.grid.RowNumberer', {header: '#', width: 30, align: 'center'}),
                        {text: 'Entidad', dataIndex: 'nombre', tooltip: "Intención", filter: true, sortable: true, flex: 1},
                        {text: 'Nivel', dataIndex: 'nivel', filter: true, sortable: true, flex: 1},
                        {text: 'Fecha Registro', dataIndex: 'fechaRegistro', filter: true, xtype: 'datecolumn', format: 'Y-m-d H:i:s', sortable: true, flex: 1},
                        {filter: true, tooltip: "Habilitado", text: "Habilitado", width: 100, dataIndex: 'habilitado', sortable: true, renderer: formatEstadoRegistro}
                    ],
                    listeners: {
                        select: 'onSelect'
                    },
                    bbar: Ext.create('Ext.PagingToolbar', {
                        store: STORE_ENTIDADES,
                        displayInfo: true,
                        name: 'paginacionGrid',
                        emptyMsg: "Sin datos que mostrar.",
                        displayMsg: 'Visualizando {0} - {1} de {2} registros',
                        beforePageText: 'Página',
                        afterPageText: 'de {0}',
                        firstText: 'Primera página',
                        prevText: 'Página anterior',
                        nextText: 'Siguiente página',
                        lastText: 'Última página',
                        refreshText: 'Actualizar',
                        inputItemWidth: 35,
                        items: [
                            {
//                                    xtype: 'button',
//                                    text: 'Exportar',
//                                    iconCls: 'x-fa fa-download',
//                                    handler: function (btn) {
//                                        onExportar(btn, "Sanciones", this.up('grid'));
//                                    }
                            }
                        ],
                        listeners: {
                            afterrender: function () {
                                this.child('#refresh').hide();
                            }
                        }
                    })
                }
            ]
        },
        {
            xtype: 'panel',
            region: 'east',
            padding: 5,
            width: '30%',
            collapseMode: 'mini',
            anchor: '100% 100%',
            layout: 'anchor',
            cls: 'panelCrearEditar',
            autoScroll: true,
            collapsible: true,
            collapsed: false,
            header: false,
            headerAsText: false,
            items: [
                {
                    xtype: 'form',
//                    title: 'Tipo de Intenciones',
                    name: 'form',
                    cls: 'quick-graph-panel shadow panelComplete',
                    ui: 'light',
                    padding: 5,
                    defaults: {
                        width: '100%',
                        margin: '0 0 5 0',
                        defaultType: 'textfield',
//                        minLengthText: MINIMUMMESSAGUEREQUERID,
//                        maxLengthText: MAXIMUMMESSAGUEREQURID,
//                        afterLabelTextTpl: INFOMESSAGEREQUERID,
                        blankText: INFOMESSAGEBLANKTEXT,
                        labelWidth: 90,
                        allowOnlyWhitespace: false,
                        allowBlank: false
                    },
                    items: [
                        {
                            xtype: "combobox",
                            fieldLabel: "Nivel",
                            value: 0,
                            width: "100%",
                            tooltip: "Seleccionar el Nivel de la entidad",
                            name: "nivel",
                            emptyText: "Seleccione",
                            reference: "text",
                            displayField: "text",
                            valueField: "id",
                            filterPickList: true,
                            queryParam: "param",
                            queryMode: "remote",
                            growMax: 20,
//                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                            allowOnlyWhitespace: false,
//                                blankText: INFOMESSAGEBLANKTEXT,
                            store: Ext.create('botWeb.store.stores.s_TipoNivel')

                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Entidad',
                            name: 'nombre',
                            minLength: 3,
                            maxLength: 45
                        },
                        {
                            xtype: 'checkboxgroup',
                            allowBlank: true,
                            allowOnlyWhitespace: true,
                            items: [
                                {xtype: 'checkboxfield', uncheckedValue: 0, name: 'habilitado', boxLabel: 'Habilitado', inputValue: 1}
                            ]
                        },
                        {
                            name: 'dataEntrenamiento',
                            hidden: true,
                            value: false,
                            allowBlank: true,
                            allowOnlyWhitespace: true
                        }, {
                            text: "Buscar Sinónimos",
                            name: 'btnSinonimo',
                            xtype: 'button',
                            height: 30,
                            tooltip: 'Buscar Sinónimos',
                            handler: 'onSinonimo'
                        },
                        {
                            text: "Agregar Sinonimo de entidad",
                            name: 'btnReasignar',
                            xtype: 'button',
                            height: 30,
                            tooltip: 'Agregar a la Lista',
                            handler: 'onAgregarElementos'
                        },
                        {
                            xtype: "grid",
                            name: "gridSinonimos",
                            height: '50%',
                            region: 'center',
                            autoScroll: true,
                            bufferedRenderer: false,
                            anchor: '100% 100%',
                            minHeight: 120,
                            maxHeight: 200,
                            viewConfig: {
                                enableTextSelection: true,
                                emptyText: "<center>Sin Elementos..</center>"
                            },
                            resizable: true,
                            selModel: {
                                type: 'cellmodel'
                            },
                            plugins: {
                                cellediting: {
                                    clicksToEdit: 2
                                }
                            },
                            store: Ext.create('botWeb.store.stores.s_Entidad_Sinonimo'),
                            columns: [
                                {name: 'Text', text: "Título", flex: 1, dataIndex: "text", editor: {
                                        allowBlank: false,
                                        selectOnFocus: false
                                    }},
                                {flex: 1,
                                    text: "Habilitado",
                                    menuDisabled: true,
                                    sortable: false,
                                    xtype: 'actioncolumn',
                                    minWidth: 20,
                                    items: [
                                        {
                                            getClass: opcionesCheck,
                                            handler: 'onDeleteEntrenamiento'
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    dockedItems: [{
                            ui: 'footer',
                            xtype: 'toolbar',
                            dock: 'bottom',
                            defaults: {
                                width: '25%',
                                height: 30
                            },
                            items: [
                                {
                                    text: 'Limpiar',
                                    tooltip: 'Limpiar',
                                    disabled: false,
                                    limpiar: true,
                                    handler: 'onLimpiarFormulario'
                                },
                                '->',
                                {
                                    text: 'Crear',
                                    tooltip: 'Crear',
                                    name: 'btnCrear',
                                    handler: 'onCrear',
                                    disabled: false
                                },
                                {
                                    text: 'Editar',
                                    tooltip: 'Editar',
                                    name: 'btnEditar',
                                    handler: 'onEditar',
                                    disabled: false
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ]
});
