<?php

include '../../dll/config.php';
if (!$mysqli = getConectionDb()) {
    return $mysqli;
}
if ($_SESSION && isset($_SESSION["idUsuario"])) {

    echo json_encode(array("idUsuario" => $_SESSION["idUsuario"], 'rol' => $_SESSION["rol"]));
} else {
    echo json_encode(array("en" => -1));
}