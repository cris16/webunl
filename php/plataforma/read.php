<?php

include '../../dll/config.php';
if (!$mysqli = getConectionDb()) {
    return $mysqli;
}
extract($_GET);

$sql = "SELECT count(*) as total FROM botUnl.plataforma p inner join botUnl.tipoPlataforma tp on tp.idTipoPlataforma=p.idTipoPlataforma WHERE TRUE";
if (isset($param)) {
    $sql .= " AND (p.nombre like '%$param%' or p.idPlataforma like '%$param%') ";
}

$result = $mysqli->query($sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
    return $mysqli->close();
}
$myrow = $result->fetch_assoc();
$total = $myrow['total'];
if ($total > 0) {
    $sql = "SELECT  p.idPlataforma,p.nombre as plataforma,IF(p.habilitado = 1, 1, 0) as habilitado, p.idTipoPlataforma, p.version, p.versionPlataforma,"
            . " p.detalle, tp.nombre as tipoPlataforma, DATE_FORMAT(CONVERT_TZ(p.fechaRegistro, @@session.time_zone, '+00:00'), '%Y-%m-%dT%H:%i:%s.000Z') AS fechaRegistro"
            . " FROM botUnl.plataforma p inner join botUnl.tipoPlataforma tp on tp.idTipoPlataforma=p.idTipoPlataforma WHERE TRUE ";
    if (isset($param)) {
        $sql .= " AND (p.nombre like '%$param%' or p.idPlataforma like '%$param%') ";
    }
    $sql .= " order by p.nombre";
    if (isset($limit)) {
        $inicio = intval($limit) * (intval($page) - 1);
        $sql .= " LIMIT $inicio, $limit ";
    } else {
        $sql .= " LIMIT $LIMITE_REGISTROS";
    }
    $result = $mysqli->query($sql);
    if (!isset($result->num_rows)) {
        echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
        return $mysqli->close();
    }
    $arreglo = [];
    while ($myrow_read = $result->fetch_assoc()) {
        $arreglo[] = array(
            'id' => $myrow_read["idPlataforma"],
            'idPlataforma' => $myrow_read["idPlataforma"],
            "plataforma" => $myrow_read["plataforma"],
            "version" => $myrow_read["version"],
            "versionPlataforma" => $myrow_read["versionPlataforma"],
            "credenciales" => $myrow_read["detalle"],
            "tipoPlataforma" => $myrow_read["tipoPlataforma"],
            "habilitado" => $myrow_read["habilitado"],
            "idTipoPlataforma" => $myrow_read["idTipoPlataforma"],
            "fechaRegistro" => $myrow_read["fechaRegistro"]
        );
    }
    echo json_encode(array('success' => TRUE, 'data' => $arreglo, 'total' => $total));
} else
    echo json_encode(array('success' => true, 'data' => [], 'total' => $total));
$mysqli->close();
