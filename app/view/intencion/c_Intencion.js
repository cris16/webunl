/* global Ext, COMBO_INTENCION_ELEMENTOS */
var MODULO_INTENCION;
Ext.define('botWeb.view.intencion.c_Intencion', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.c_Intencion',
    onView: function (panelLoad) {
        MODULO_INTENCION = panelLoad;
        MODULO_INTENCION.down('[name=grid]').getStore().load();
        MODULO_INTENCION.down('[name=btnEditar]').disable();
        if (document.body) {
            HEIGT_VIEWS = (document.body.clientHeight);
        } else {
            HEIGT_VIEWS = (window.innerHeight);

        }
        HEIGT_VIEWS = HEIGT_VIEWS - 65;
        MODULO_INTENCION.setHeight(HEIGT_VIEWS);
        MODULO_INTENCION.down('[name=grid]').setHeight(HEIGT_VIEWS);
        MODULO_INTENCION.down('[name=form]').setHeight(HEIGT_VIEWS);
    },
    onRecargar: function () {
        MODULO_INTENCION.down('[name=grid]').getStore().reload();
    },
    onLimpiarFormulario: function (btn, e) {
        if (btn.limpiar) {
            MODULO_INTENCION.down('[name=form]').getForm().reset();
            var grid = MODULO_INTENCION.down('[name=grid]');
            grid.getView().deselect(grid.getSelection());
            MODULO_INTENCION.down('[name=form]').down('[name=gridEntrenamiento]').getStore().removeAll();
        }
        MODULO_INTENCION.down('[name=grid]').getStore().load();
        MODULO_INTENCION.down('[name=btnEditar]').disable();
        MODULO_INTENCION.down('[name=btnCrear]').enable();
    },
    onLimpiar: function (btn, e) {
        MODULO_INTENCION.down('[name=txtParam]').reset();
        if (btn.limpiar) {
            MODULO_INTENCION.down('[name=form]').getForm().reset();
            var grid = MODULO_INTENCION.down('[name=grid]');
            grid.getView().deselect(grid.getSelection());
            MODULO_INTENCION.down('[name=form]').down('[name=gridEntrenamiento]').getStore().removeAll();
        }
        MODULO_INTENCION.down('[name=grid]').getStore().load();
        MODULO_INTENCION.down('[name=btnEditar]').disable();
        MODULO_INTENCION.down('[name=btnCrear]').enable();
    },
    onSelect: function (grid, selected, eOpts) {
        var respuesta = JSON.parse(selected.data.respuesta);
        if (!MODULO_INTENCION.down('[name=form]').down('[name=idIntencionTipo]').getStore().getById(selected['data']['idIntencionTipo'])) {
            MODULO_INTENCION.down('[name=form]').down('[name=idIntencionTipo]').getStore().proxy.extraParams = {param: ""};
            MODULO_INTENCION.down('[name=form]').down('[name=idIntencionTipo]').getStore().reload({
                params: {idIntencionTipo: selected['data']['idIntencionTipo']},
                callback: function (records) {
                    if (records.length > 0) {
                        MODULO_INTENCION.down('[name=form]').down('[name=idIntencionTipo]').setValue(selected['data']['idIntencionTipo']);
                    }
                }
            });
        }
        MODULO_INTENCION.down('[name=form]').down('[name=gridEntrenamiento]').getStore().removeAll();
        if (selected && selected['data']) {
            var gridProyecto = MODULO_INTENCION.down('[name=form]').down('[name=gridEntrenamiento]').getStore();
            if (gridProyecto.getData().length <= 0) {
                gridProyecto.proxy.extraParams = {idIntenciones: selected.get('id')};
//                mostrarBarraProgreso("Carganto la Información");
                gridProyecto.reload({callback: function (records, operation, success) {
//                        ocultarBarraProgreso();
                    }
                });
            }
        }

        if (respuesta.quick_replies)
            MODULO_INTENCION.down('[name=idIntencionTipoMensaje]').setValue("quick_replies");
        else if (respuesta.attachment) {
            var template_type = respuesta.attachment.payload;
            if (template_type.template_type === "receipt" || template_type.template_type === "button")
                MODULO_INTENCION.down('[name=idIntencionTipoMensaje]').setValue(template_type.template_type);
            else
                MODULO_INTENCION.down('[name=idIntencionTipoMensaje]').setValue("attachment");
        } else if (respuesta.text)
            MODULO_INTENCION.down('[name=idIntencionTipoMensaje]').setValue("text");

        MODULO_INTENCION.down('[name=form]').getForm().loadRecord(selected);
        MODULO_INTENCION.down('[name=btnEditar]').enable();
        MODULO_INTENCION.down('[name=btnCrear]').disable();
    },
    onCrear: function () {
        var me = this, form = MODULO_INTENCION.down('[name=form]').getForm();
        var dataEntrenamiento = me.onDatosExtras(MODULO_INTENCION.down('[name=form]'));
        var gridLeer = MODULO_INTENCION.down('[name=grid]');

        if (form.isValid()) {
            var tip = MODULO_INTENCION.down('[name=idIntencionTipoMensaje]').getValue();
            var record = form.getValues();
            if (tip !== 'text') {
                if (MODULO_INTENCION.down('[name=respuesta]').getValue().length <= 2)
                    return notificaciones('Necesita agregar elementos', 2);
                else {
                    gridLeer.getStore().proxy.extraParams = dataEntrenamiento;
                    record.dataEntrenamiento = dataEntrenamiento['dataEntrenamiento'];
                    MODULO_INTENCION.down('[name=grid]').getStore().insert(0, record);
                    MODULO_INTENCION.down('[name=grid]').getStore().sync({
                        success: function (response, options) {
                            notificaciones("Registro creado", 'Mensaje', 1);
                            me.onLimpiarFormulario({limpiar: true});
                        },
                        failure: function (response, options) {
                            notificaciones("Algo ocurrio, intene más tarde", 'Mensaje', 2);
                            me.onLimpiarFormulario({limpiar: true});
                        }
                    });
                }
            } else {
                if (MODULO_INTENCION.down('[name=respuesta]').getValue().length <= 2) {
                    var respuestaJson = {text: 'Ingrese el mensaje a presentar el usuario.'};
                    MODULO_INTENCION.down('[name=respuesta]').setValue(JSON.stringify(respuestaJson));
                }
                gridLeer.getStore().proxy.extraParams = dataEntrenamiento;
                record.dataEntrenamiento = dataEntrenamiento['dataEntrenamiento'];
                MODULO_INTENCION.down('[name=grid]').getStore().insert(0, record);
                MODULO_INTENCION.down('[name=grid]').getStore().sync({
                    success: function (response, options) {
                        notificaciones("Registro creado", 'Mensaje', 1);
                        me.onLimpiarFormulario({limpiar: true});
                    },
                    failure: function (response, options) {
                        notificaciones("Algo ocurrio, intene más tarde", 'Mensaje', 2);
                        me.onLimpiarFormulario({limpiar: true});
                    }
                });
            }

        } else
            mensajesValidacionForms(form.getFields());
    },
    onEditar: function () {
        var me = this, form = MODULO_INTENCION.down('[name=form]');
        var gridLeer = MODULO_INTENCION.down('[name=grid]');
        var gridSelect = gridLeer.getSelection();
        var dataEntrenamiento = me.onDatosExtras(form);
        if (dataEntrenamiento['total'] > 0)
            gridSelect[0].set('dataEntrenamiento', true);

        if (form.getForm().isValid()) {
            var tip = MODULO_INTENCION.down('[name=idIntencionTipoMensaje]').getValue();
            if (tip !== 'text') {
                if (MODULO_INTENCION.down('[name=respuesta]').getValue().length <= 2)
                    return notificaciones('Necesita agregar elementos', 2);
                else {
                    gridLeer.getStore().proxy.extraParams = dataEntrenamiento;
                    form.getForm().updateRecord(form.getForm().activeRecord);
                    MODULO_INTENCION.down('[name=grid]').getStore().sync({
                        success: function (response, options) {
                            notificaciones("Cambios realizados", 'Mensaje', 1);
                            me.onLimpiarFormulario({limpiar: true});
                        },
                        failure: function (response, options) {
                            notificaciones("Algo ocurrio, intene más tarde", 'Mensaje', 2);
                            me.onLimpiarFormulario({limpiar: true});
                        }
                    });
                }
            } else {
                if (MODULO_INTENCION.down('[name=respuesta]').getValue().length <= 2) {
                    var respuestaJson = {text: 'Ingrese el mensaje a presentar el usuario.'};
                    MODULO_INTENCION.down('[name=respuesta]').setValue(JSON.stringify(respuestaJson));
                }
                gridLeer.getStore().proxy.extraParams = dataEntrenamiento;
                form.getForm().updateRecord(form.getForm().activeRecord);
                MODULO_INTENCION.down('[name=grid]').getStore().sync({
                    success: function (response, options) {
                        notificaciones("Cambios realizados", 'Mensaje', 1);
                        me.onLimpiarFormulario({limpiar: true});
                    },
                    failure: function (response, options) {
                        notificaciones("Algo ocurrio, intene más tarde", 'Mensaje', 2);
                        me.onLimpiarFormulario({limpiar: true});
                    }
                });
            }

        } else
            mensajesValidacionForms(form.getForm().getFields());
    },
    onBuscar: function (btn, e) {
        var txtParam = MODULO_INTENCION.down('[name=txtParam]').getValue(), params = {}, me = this;
        params['param'] = txtParam;
        if (btn.xtype === 'button' || e.event.keyCode === 13) {
            me.onLimpiarFormulario({limpiar: true});
            MODULO_INTENCION.down('[name=paginacionGrid]').moveFirst();
            MODULO_INTENCION.down('[name=grid]').getStore().load({
                params: params,
                callback: function (store, record, success, opts) {
//                    var response_text = store.proxy.reader.rawData;
                    if (!success)
                        setMensajeGridEmptyText(MODULO_INTENCION.down('[name=grid]'), '<h3>Problema en la comunicación con el servidor local, por favor inténtelo mas tarde.</h3>');
                    else if (record.length === 0)
                        setMensajeGridEmptyText(MODULO_INTENCION.down('[name=grid]'), '<h3>No existen resultados.</h3>');
                }
            });
        }
    },
    cargarToolTip: function (c) {
        Ext.create('Ext.tip.ToolTip', {
            target: c.getEl(),
            html: c.tip
        });
    },
    onAgregarElementos: function () {
        MODULO_INTENCION.down('[name=form]').down('[name=gridEntrenamiento]').getStore().add(new Ext.data.Record({text: "", habilitado: 1, nuevo: true}));
    },
    onDeleteEntrenamiento: function (grid, rowIndex, colIndex) {
        var ventana = MODULO_INTENCION.down('[name=form]');
        var gridPerfil = ventana.down('[name=gridEntrenamiento]');
        var rec = grid.getStore().getAt(rowIndex);
        if (rec.data.nuevo)
            gridPerfil.getStore().remove(rec);
        else {
            rec.set('actualizar', true);
            if (rec.data.habilitado)
                rec.set('habilitado', 0);
            else
                rec.set('habilitado', 1);
        }
    },
    onDatosExtras: function (ventanaPerfil, gridProyecto) {
        var listPerfiles = [];
        var dataAuxPerfil = ventanaPerfil.down('[name=gridEntrenamiento]').getStore().data.items;
        for (var i in dataAuxPerfil) {
            if (dataAuxPerfil[i].data.nuevo)
                listPerfiles.push({id: dataAuxPerfil[i].data.id, text: dataAuxPerfil[i].data.text, habilitado: dataAuxPerfil[i].data.habilitado, nuevo: dataAuxPerfil[i].data.nuevo});

//            else {
//                listPerfiles[i] = {id: dataAuxPerfil[i].data.id, text: dataAuxPerfil[i].data.text, habilitado: dataAuxPerfil[i].data.habilitado, nuevo: false};
//            }
        }
        var dataAuxPermisosAdmin = ventanaPerfil.down('[name=gridEntrenamiento]').getStore().getUpdatedRecords();
        for (var i in dataAuxPermisosAdmin) {
            listPerfiles.push({id: dataAuxPermisosAdmin[i].data.id, text: dataAuxPerfil[i].data.text, habilitado: dataAuxPermisosAdmin[i].data.habilitado, nuevo: false});
        }
        return {dataEntrenamiento: JSON.stringify(listPerfiles), total: listPerfiles.length};
    },
    onCargarElemento: function () {
        var modulo = this;
        COMBO_INTENCION_ELEMENTOS.removeAll();
        var tip = MODULO_INTENCION.down('[name=idIntencionTipoMensaje]').getValue();
        var res = MODULO_INTENCION.down('[name=respuesta]').getValue();
        var respuesta = JSON.parse(res);
        if (tip) {
            switch (tip) {
                case 'text':
                    var window = new Ext.create('Ext.window.Window', {
                        title: 'Elementos de Mesaje Simple',
                        closable: true,
                        layout: 'fit',
                        modal: true,
                        height: 200,
                        width: 450,
                        constrain: true,
                        items: [{
                                region: 'center',
                                xtype: 'form',
                                layout: 'anchor',
                                header: false,
                                padding: 5,
                                headerAsText: false,
                                items: [
                                    {
                                        xtype: 'form',
//                                        title: 'Mensaje',
                                        name: 'formMensaje',
                                        cls: 'quick-graph-panel shadow panelComplete',
                                        ui: 'light',
                                        padding: 5,
                                        defaults: {
                                            width: '100%',
                                            margin: '0 0 5 0',
                                            minLengthText: "Este campo requiere un mínimo de {0} caracteres",
                                            maxLengthText: "Este campo acepta un máximo de {0} caracteres",
                                            blankText: "Este campo no puede ser vacío",
                                            labelWidth: 90,
                                            allowOnlyWhitespace: false,
                                            allowBlank: false

                                        },
                                        items: [
                                            {
                                                xtype: 'textarea',
                                                fieldLabel: 'Mensaje',
                                                name: 'text',
                                                flex: 1,
                                                hideLabel: true,
                                                height: '80%',
                                                value: "Ingrese el mensaje a presentar el usuario.",
                                                tooltip: 'Si desea enviar multiples mensajes, en una intencion, modificar este elemento'
                                            }
                                        ]
                                    }
                                ]
                            }
                        ],
                        buttons: [{
                                xtype: 'button',
                                iconAlign: 'right',
                                text: 'Cerrar',
                                tooltip: 'Cerrar',
                                style: {
//                                    background: "#26A965",
//                                    border: '1px solid #36beb3',
                                    '-webkit-border-radius': '5px 5px',
                                    '-moz-border-radius': '5px 5px'
                                },
                                handler: function () {
                                    window.close();
                                }
                            }, '->', {
                                text: 'Guardar Cambios',
                                tooltip: 'Registrar Cambios',
                                anchor: '100%',
                                style: {
//                                    background: "#26A965",
//                                    border: '1px solid #36beb3',
                                    '-webkit-border-radius': '5px 5px',
                                    '-moz-border-radius': '5px 5px'
                                },
                                handler: function () {
                                    var respuesta = {
                                        text: window.down('[name=text]').getValue()
                                    };
                                    MODULO_INTENCION.down('[name=respuesta]').setValue(JSON.stringify(respuesta));
                                    window.close();
                                }
                            }]
                    }).show();
                    if (res && res.length > 2 && respuesta.text) {
                        if (respuesta.text) {
                            window.down('[name=text]').setValue(respuesta.text);
                        }
                    }
//                    return notificaciones('Este mensaje debe estar configurado en Dialogflow si no es una respuesta multiple', 1);
                    break;
                case 'quick_replies':
                    modulo.onMesajeQuickReplies(res);
                    break;
                case 'attachment':
                    modulo.onMensajeAtachmen(res);
                    break;
                case 'receipt':
                    var window = new Ext.create('Ext.window.Window', {
                        title: 'Orden de Envio',
                        closable: true,
                        layout: 'fit',
                        modal: true,
                        height: 220,
                        width: 820,
                        bodyPadding: 10,
                        constrain: true,
                        items: [
                            {
                                region: 'center',
                                xtype: 'form',
                                name: 'panel_puntos_ClientesCenter',
                                layout: 'anchor',
                                header: false,
                                padding: 5,
                                headerAsText: false,
                                items: [
                                    {
                                        xtype: 'form',
                                        name: 'formInfoGeneral',
                                        cls: 'quick-graph-panel shadow panelComplete',
                                        ui: 'light',
                                        padding: 5,
                                        defaults: {
                                            width: '100%',
                                            margin: '0 0 5 0',
                                            minLengthText: "Este campo requiere un mínimo de {0} caracteres",
                                            maxLengthText: "Este campo acepta un máximo de {0} caracteres",
//                                            afterLabelTextTpl: INFOMESSAGEREQUERID,
                                            blankText: "Este campo no puede ser vacío",
                                            labelWidth: 90,
                                            allowOnlyWhitespace: true,
                                            allowBlank: false
                                        },
                                        items: [
                                            {
                                                flex: 1,
                                                xtype: 'container',
                                                layout: 'hbox',
                                                margin: '10 0 5 0',
                                                items: [
                                                    {
                                                        xtype: 'textfield',
                                                        margin: '0 0 0 5',
                                                        fieldLabel: 'Usuario',
                                                        name: 'recipient_name',
                                                        emptyText: 'Usuario',
                                                        allowBlank: true,
                                                        minLengthText: "Este campo requiere un mínimo de {0} caracteres",
                                                        maxLengthText: "Este campo acepta un máximo de {0} caracteres"
                                                    }, {
                                                        xtype: 'textfield',
                                                        fieldLabel: '# de Orden',
                                                        name: 'order_number',
                                                        emptyText: 'Orden',
                                                        allowBlank: true,
                                                        minLengthText: "Este campo requiere un mínimo de {0} caracteres",
                                                        maxLengthText: "Este campo acepta un máximo de {0} caracteres"
                                                    }
                                                ]
                                            }, {
                                                flex: 1,
                                                xtype: 'container',
                                                layout: 'hbox',
                                                margin: '10 0 5 0',
                                                items: [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Nombre del Comerciante',
                                                        name: 'merchant_name',
                                                        emptyText: 'Compartir',
                                                        allowBlank: true,
                                                        minLengthText: "Este campo requiere un mínimo de {0} caracteres",
                                                        maxLengthText: "Este campo acepta un máximo de {0} caracteres"
                                                    }, {
                                                        xtype: 'checkboxgroup',
                                                        allowBlank: true,
                                                        allowOnlyWhitespace: true,
                                                        items: [
                                                            {xtype: 'checkboxfield', uncheckedValue: 0, name: 'sharable', boxLabel: 'Compartir', inputValue: 1}
                                                        ]
                                                    }
                                                ]
                                            }, {
                                                flex: 1,
                                                xtype: 'container',
                                                layout: 'hbox',
                                                margin: '10 0 5 0',
                                                items: [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Metodo de Pago',
                                                        name: 'payment_method',
                                                        emptyText: 'Metodo de Pago',
                                                        minLengthText: "Este campo requiere un mínimo de {0} caracteres",
                                                        maxLengthText: "Este campo acepta un máximo de {0} caracteres"
                                                    }, {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'URL Orden',
                                                        name: 'order_url',
                                                        emptyText: 'URL Orden',
                                                        allowBlank: true,
                                                        minLengthText: "Este campo requiere un mínimo de {0} caracteres",
                                                        maxLengthText: "Este campo acepta un máximo de {0} caracteres"
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        ],
                        buttons: [{
                                xtype: 'button',
                                iconAlign: 'right',
                                text: 'Cerrar',
                                tooltip: 'Cerrar',
                                style: {
//                                    background: "#26A965",
//                                    border: '1px solid #36beb3',
                                    '-webkit-border-radius': '5px 5px',
                                    '-moz-border-radius': '5px 5px'
                                },
                                handler: function () {
                                    window.close();
                                }
                            }, '->', {
                                text: 'Guardar Cambios',
                                tooltip: 'Registrar Cambios',
                                anchor: '100%',
                                style: {
//                                    background: "#26A965",
//                                    border: '1px solid #36beb3',
                                    '-webkit-border-radius': '5px 5px',
                                    '-moz-border-radius': '5px 5px'
                                },
                                handler: function () {
                                    if (COMBO_INTENCION_ELEMENTOS.getData().length > 0) {

                                    } else {
                                        notificaciones("Falta crear un elemento", 2);
                                    }
                                }
                            }]
                    }).show();
                    break;
                case 'button':
                    var window = new Ext.create('Ext.window.Window', {
                        title: 'Lista de Botones',
                        closable: true,
                        layout: 'fit',
                        modal: true,
                        height: 380,
                        width: 820,
                        bodyPadding: 10,
                        constrain: true,
                        items: [
                            {
                                region: 'center',
                                xtype: 'form',
                                name: 'panel_puntos_ClientesCenter',
                                layout: 'anchor',
                                header: false,
                                padding: 5,
                                headerAsText: false,
                                items: [
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Mensaje',
                                        name: 'title',
                                        width: 280,
                                        margin: '5 0 5 0',
                                        value: "",
                                        emptyText: 'Titulo',
                                        minLengthText: "Este campo requiere un mínimo de {0} caracteres",
                                        maxLengthText: "Este campo acepta un máximo de {0} caracteres",
                                        allowBlank: true
                                    },
                                    {
                                        xtype: 'form',
                                        name: 'formMensaje',
                                        ui: 'light',
                                        padding: 5,
                                        layout: 'hbox',
                                        defaults: {
                                            width: '100%',
                                            margin: '0 0 5 0',
                                            minLengthText: "Este campo requiere un mínimo de {0} caracteres",
                                            maxLengthText: "Este campo acepta un máximo de {0} caracteres",
//                                            afterLabelTextTpl: INFOMESSAGEREQUERID,
                                            blankText: "Este campo no puede ser vacío",
                                            labelWidth: 90,
                                            allowOnlyWhitespace: true,
                                            allowBlank: false
                                        },
                                        items: [
                                            {
                                                flex: 1,
                                                fieldLabel: 'Configuracion de botones',
                                                xtype: 'container',
                                                layout: 'vbox',
                                                items: [
                                                    {
                                                        xtype: 'combobox',
                                                        fieldLabel: 'Tipo de Boton',
                                                        name: 'buttons_type',
                                                        width: "90%",
                                                        store: Ext.create('botWeb.store.stores.s_TipoIntencionBotonAttachment'),
                                                        displayField: 'text',
                                                        valueField: 'id',
                                                        listeners: {
                                                            change: function (thisObj, newValue, oldValue, eOpts) {
                                                                var valor = true;
                                                                if (newValue) {
                                                                    if (newValue === "web_url")
                                                                    {
                                                                        valor = false;
                                                                        window.down('[name=buttons_payload]').setValue("");
                                                                    } else {
                                                                        window.down('[name=buttons_url]').setValue("");
                                                                    }
                                                                    window.down('[name=buttons_url]').allowBlank = valor;
                                                                    window.down('[name=buttons_title]').allowBlank = false;
                                                                    window.down('[name=buttons_payload]').setVisible(valor);
                                                                    window.down('[name=buttons_url]').setVisible(!valor);
                                                                } else {
                                                                    window.down('[name=buttons_payload]').allowBlank = true;
                                                                    window.down('[name=buttons_url]').allowBlank = true;
                                                                    window.down('[name=buttons_title]').allowBlank = true;
                                                                    window.down('[name=buttons_payload]').setValue("");
                                                                    window.down('[name=buttons_url]').setValue("");
                                                                    window.down('[name=buttons_title]').setValue("");
                                                                }

                                                            }
                                                        }
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        width: "90%",
                                                        fieldLabel: 'Titulo',
                                                        name: 'buttons_title',
                                                        emptyText: 'Titulo',
                                                        allowBlank: true,
                                                        minLengthText: "Este campo requiere un mínimo de {0} caracteres",
                                                        maxLengthText: "Este campo acepta un máximo de {0} caracteres"
                                                    }
                                                ]
                                            },
                                            {
                                                flex: 1,
                                                fieldLabel: 'Configuracion de botones',
                                                xtype: 'container',
                                                layout: 'vbox',
                                                items: [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'URL',
                                                        name: 'buttons_url',
                                                        emptyText: 'URL',
                                                        width: "90%",
                                                        allowBlank: true,
                                                        minLengthText: "Este campo requiere un mínimo de {0} caracteres",
                                                        maxLengthText: "Este campo acepta un máximo de {0} caracteres"
                                                    }, {
                                                        xtype: 'textfield',
                                                        width: "90%",
                                                        fieldLabel: 'Payload',
                                                        name: 'buttons_payload',
                                                        emptyText: 'Payload',
                                                        allowBlank: true,
                                                        minLengthText: "Este campo requiere un mínimo de {0} caracteres",
                                                        maxLengthText: "Este campo acepta un máximo de {0} caracteres"
                                                    }
                                                ]
                                            }
                                        ], dockedItems: [{
                                                ui: 'footer',
                                                xtype: 'toolbar',
                                                dock: 'bottom',
                                                defaults: {
                                                    width: '25%',
                                                    height: 30
                                                },
                                                items: [
                                                    {
                                                        text: 'Limpiar',
                                                        tooltip: 'Limpiar',
                                                        disabled: false,
                                                        limpiar: true,
                                                        handler: function () {
                                                            window.down('[name=gridElementos]').getView().deselect(window.down('[name=gridElementos]').getSelection());
                                                            window.down('[name=formMensaje]').getForm().reset();
                                                        }
                                                    },
                                                    '->',
                                                    {
                                                        text: 'Crear',
                                                        tooltip: 'Crear',
                                                        name: 'btnCrear',
                                                        disabled: false,
                                                        handler: function () {
                                                            var form = window.down('[name=formMensaje]').getForm();
                                                            if (COMBO_INTENCION_ELEMENTOS.getData().items.length < 10) {
                                                                if (form.isValid()) {
                                                                    var recordSelected = new Ext.data.Record(form.getValues());
                                                                    window.down('[name=gridElementos]').getStore().add(recordSelected);
                                                                    window.down('[name=formMensaje]').getForm().reset();
                                                                } else
                                                                    notificaciones("Llene los campos obligatorios", 2);
                                                            } else
                                                                notificaciones("Se alcanzo el numero maximo de elementos", 2);
                                                        }
                                                    },
                                                    {
                                                        text: 'Editar',
                                                        tooltip: 'Editar',
                                                        name: 'btnEditar',
                                                        disabled: true,
                                                        handler: function () {
                                                            var form = window.down('[name=formMensaje]').getForm();
                                                            if (form.isValid()) {
                                                                window.down('[name=gridElementos]').getStore().add(new Ext.data.Record(form.getValues()));
                                                                COMBO_INTENCION_ELEMENTOS.remove(window.down('[name=gridElementos]').getSelection());
                                                            } else
                                                                notificaciones("Llene los campos obligatorios", 2);
                                                        }
                                                    }
                                                ]
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "grid",
                                        name: "gridElementos",
                                        autoScroll: true,
                                        frame: true,
                                        bufferedRenderer: false,
                                        plugins: ['gridfilters', {
                                                ptype: 'cellediting',
                                                clicksToEdit: 1
                                            }],
                                        height: 100,
                                        resizable: true,
                                        store: COMBO_INTENCION_ELEMENTOS,
                                        columns: [
                                            {text: "Tipo de boton", flex: 1, dataIndex: "buttons_type"},
                                            {text: "URL del boton", flex: 1, dataIndex: "buttons_url", sortable: true},
                                            {text: "Titulo del boton", flex: 1, dataIndex: "buttons_title", sortable: true},
                                            {text: "Payload del boton", flex: 1, dataIndex: "buttons_payload", sortable: true},
                                            {
                                                flex: 1,
                                                menuDisabled: true,
                                                sortable: false,
                                                xtype: 'actioncolumn',
                                                minWidth: 20,
                                                items: [{
                                                        getClass: function (v, meta, rec) {
                                                            return 'gridAuxDelete x-fa fa-times';
                                                        },
                                                        handler: function (grid, rowIndex, colIndex) {
                                                            var rec = grid.getStore().getAt(rowIndex);
                                                            COMBO_INTENCION_ELEMENTOS.remove(rec);
                                                        }
                                                    }]
                                            }
                                        ],
                                        minHeight: 150,
                                        split: true,
                                        region: "north",
                                        viewConfig: {
                                            enableTextSelection: true,
                                            emptyText: "<center>Sin Elementos..</center>"
                                        },
                                        listeners: {
                                            selectionchange: function (thisObj, selected, eOpts) {
                                                modulo.onSetActiveRecordElemento(selected, window);
                                            }
                                        }
                                    }

                                ]
                            }
                        ],
                        buttons: [{
                                xtype: 'button',
                                iconAlign: 'right',
                                text: 'Cerrar',
                                tooltip: 'Cerrar',
                                style: {
//                                    background: "#26A965",
//                                    border: '1px solid #36beb3',
                                    '-webkit-border-radius': '5px 5px',
                                    '-moz-border-radius': '5px 5px'
                                },
                                handler: function () {
                                    window.close();
                                }
                            }, '->', {
                                text: 'Guardar Cambios',
                                tooltip: 'Registrar Cambios',
                                anchor: '100%',
                                style: {
//                                    background: "#26A965",
//                                    border: '1px solid #36beb3',
                                    '-webkit-border-radius': '5px 5px',
                                    '-moz-border-radius': '5px 5px'
                                },
                                handler: function () {
                                    if (COMBO_INTENCION_ELEMENTOS.getData().length > 0) {
                                        var elements = modulo.onAttachmentButton(COMBO_INTENCION_ELEMENTOS.getData().items);
                                        var val = window.down('[name=title]').getValue();
                                        if (elements) {
                                            var payload = {template_type: "button",
                                                buttons: elements};
                                            val !== "" ? payload["text"] = val : 0;
                                            window.down('[name=buttons_title]').getValue() !== "" ? payload["text"] = window.down('[name=buttons_title]').getValue() : 0;
                                            var respuesta = {attachment: {type: "template", payload: payload}};
                                            MODULO_INTENCION.down('[name=respuesta]').setValue(JSON.stringify(respuesta));
                                            window.close();
                                        } else {
                                            notificaciones("Elementos mal formados, Revise", 2);
                                        }

                                    } else {
                                        notificaciones("Falta crear un elemento", 2);
                                    }
                                }
                            }]
                    }).show();
                    if (res && res.length > 2 && respuesta.attachment) {
                        var payload = respuesta.attachment.payload;
                        if (payload) {
                            if (payload.text && payload.text !== "")
                                window.down('[name=title]').setValue(payload.text);
                            for (var i = 0; i < payload.buttons.length; i++) {
                                var datos = {};
                                payload.buttons[i].type ? datos["buttons_type"] = payload.buttons[i].type : 0;
                                payload.buttons[i].title ? datos["buttons_title"] = payload.buttons[i].title : 0;
                                payload.buttons[i].url ? datos["buttons_url"] = payload.buttons[i].url : 0;
                                payload.buttons[i].payload ? datos["buttons_payload"] = payload.buttons[i].payload : 0;
                                window.down('[name=gridElementos]').getStore().add(datos);
                            }
                        }
                    }
                    break;
                default:
                    notificaciones("Aun no se encuentra activo este elemento", 2);
                    break;
            }
        } else {
            notificaciones("No ha seleccionado que tipo de mensaje desea presenatar", 2);
        }
    },

    //--------------------------------------------------------------------------
    //QUICKREPLIES
    onMesajeQuickReplies: function (datos) {
        var modulo = this;
        var store = Ext.create('botWeb.store.stores.s_TipoElemento');
        var respuesta;
        try {
            respuesta = JSON.parse(datos);
        } catch (e) {
            respuesta = [];
        }
        var window = new Ext.create('Ext.window.Window', {
            title: 'Elementos de Quick Replies',
            closable: true,
            layout: 'fit',
            modal: true,
            height: 480,
            width: 720,
            constrain: true,
            items: [
                {
                    layout: {
                        type: 'hbox',
                        pack: 'start',
                        align: 'stretch'
                    },
                    items: [
                        {
                            flex: 2,
                            items: [
                                {
                                    xtype: 'form',
//                                    ui: 'light',
                                    padding: 5,
                                    defaults: {
                                        width: '100%',
                                        margin: '0 0 5 0',
                                        minLengthText: "Este campo requiere un mínimo de {0} caracteres",
                                        maxLengthText: "Este campo acepta un máximo de {0} caracteres",
//                                        afterLabelTextTpl: INFOMESSAGEREQUERID,
                                        blankText: "Este campo no puede ser vacío",
                                        labelWidth: 90,
                                        allowOnlyWhitespace: false,
                                        allowBlank: false
                                    },
                                    items: [
                                        {
                                            xtype: 'textarea',
                                            fieldLabel: 'Mensaje',
                                            name: 'text',
                                            tooltip: 'Si desea enviar multiples mensajes, en una intencion, modificar este elemento',
                                            value: "Ingrese el mensaje a presentar el usuario.",
                                            anchor: '100%'
                                        },
                                        {
                                            xtype: 'form',
//                                            title: 'Mensaje',
                                            name: 'formMensaje',
                                            ui: 'light',
                                            padding: 5,
                                            defaults: {
                                                width: '100%',
                                                margin: '0 0 5 0',
                                                minLengthText: "Este campo requiere un mínimo de {0} caracteres",
                                                maxLengthText: "Este campo acepta un máximo de {0} caracteres",
//                                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                                blankText: "Este campo no puede ser vacío",
                                                labelWidth: 90,
                                                allowOnlyWhitespace: true,
                                                allowBlank: false
                                            },
                                            items: [
                                                {
                                                    flex: 1,
                                                    xtype: 'container',
                                                    layout: 'hbox',
                                                    margin: '0 0 5 0',
                                                    items: [
                                                        {
                                                            flex: 1,
                                                            xtype: 'container',
                                                            layout: 'vbox',
                                                            margin: '0 0 5 0',
                                                            items: [
                                                                {
                                                                    xtype: "combobox",
                                                                    fieldLabel: "Tipo de Elemento",
                                                                    value: 0,
                                                                    forceSelection: true,
                                                                    tooltip: "Seleccionar un elemento",
                                                                    emptyText: "Seleccione",
                                                                    reference: "text",
                                                                    filterPickList: true,
                                                                    anyMatch: true,
                                                                    growMax: 20,
//                                                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                                                    allowOnlyWhitespace: false,
                                                                    blankText: "Este campo no puede ser vacío",
                                                                    name: 'content_type',
                                                                    displayField: 'text',
                                                                    queryMode: 'local',
                                                                    valueField: 'id',
                                                                    store: Ext.create('botWeb.store.stores.s_TipoIntencionBotonQuick'),
                                                                    listeners: {
                                                                        select: function (grid, selected, eOpts) {
                                                                            document.getElementById("recursoQuickReplies").innerHTML = "<img src=" + selected.data.url + " style='max-width: 105%;'>";
                                                                        },
                                                                        change: function (thisObj, newValue, oldValue, eOpts) {
                                                                            var valor = false;
                                                                            if (newValue) {
                                                                                if (newValue !== 'text') {
                                                                                    window.down('[name=title]').setValue("");
                                                                                    window.down('[name=payload]').setValue("");
                                                                                    window.down('[name=image_url]').setValue("");
                                                                                    valor = true;
                                                                                }
                                                                                window.down('[name=title]').allowBlank = valor;
                                                                                window.down('[name=payload]').allowBlank = valor;
                                                                                window.down('[name=title]').setVisible(!valor);
                                                                                window.down('[name=payload]').setVisible(!valor);
                                                                                window.down('[name=image_url]').setVisible(!valor);
                                                                            } else {
                                                                                window.down('[name=title]').allowBlank = true;
                                                                                window.down('[name=payload]').allowBlank = true;
                                                                            }
                                                                        }
                                                                    }
                                                                },
                                                                {
                                                                    xtype: 'textfield',
                                                                    fieldLabel: 'Titulo',
                                                                    name: 'title',
                                                                    emptyText: 'Titulo',
                                                                    anyMatch: true,
                                                                    allowOnlyWhitespace: false,
                                                                    //maxLength: '20',
                                                                    minLengthText: "Este campo requiere un mínimo de {0} caracteres",
                                                                    maxLengthText: "Este campo acepta un máximo de {0} caracteres"
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            flex: 1,
                                                            xtype: 'container',
                                                            layout: 'vbox',
                                                            margin: '0 0 5 0',
                                                            items: [
                                                                {
                                                                    xtype: 'textfield',
                                                                    fieldLabel: 'Payload',
                                                                    name: 'payload',
                                                                    emptyText: 'Payload',
                                                                    maxLength: '1000',
                                                                    minLengthText: "Este campo requiere un mínimo de {0} caracteres",
                                                                    maxLengthText: "Este campo acepta un máximo de {0} caracteres"
                                                                },
                                                                {
                                                                    xtype: 'textfield',
                                                                    fieldLabel: 'Image url',
                                                                    name: 'image_url',
                                                                    emptyText: 'Image url',
                                                                    minLengthText: "Este campo requiere un mínimo de {0} caracteres",
                                                                    maxLengthText: "Este campo acepta un máximo de {0} caracteres"
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                }
                                            ],
                                            dockedItems: [
                                                {
                                                    ui: 'footer',
                                                    xtype: 'toolbar',
                                                    dock: 'bottom',
                                                    defaults: {
                                                        width: '25%',
                                                        height: 30
                                                    },
                                                    items: [
                                                        {
                                                            text: 'Limpiar',
                                                            tooltip: 'Limpiar',
//                                                            disabled: false,
                                                            limpiar: true,
                                                            handler: function () {
                                                                window.down('[name=gridElementos]').getView().deselect(window.down('[name=gridElementos]').getSelection());
                                                                window.down('[name=formMensaje]').getForm().reset();
                                                            }
                                                        },
                                                        '->',
                                                        {
                                                            text: 'Crear',
                                                            tooltip: 'Crear',
                                                            name: 'btnCrear',
//                                                            disabled: false,
                                                            handler: function () {
                                                                var form = window.down('[name=formMensaje]').getForm();
                                                                if (form.isValid()) {
                                                                    if (store.getData().items.length < 10) {
                                                                        var recordSelected = new Ext.data.Record(form.getValues());
                                                                        store.add(recordSelected);
                                                                        window.down('[name=formMensaje]').getForm().reset();
                                                                    } else
                                                                        notificaciones("El numero de maximo de elementos de 10 elementos", 2);
                                                                } else
                                                                    notificaciones("Llene los campos obligatorios", 2);
                                                            }
                                                        },
                                                        {
                                                            text: 'Editar',
                                                            tooltip: 'Editar',
                                                            name: 'btnEditar',
//                                                            disabled: true,
                                                            handler: function () {
                                                                var form = window.down('[name=formMensaje]').getForm();
                                                                if (form.isValid()) {
                                                                    store.add(new Ext.data.Record(form.getValues()));
                                                                    store.remove(window.down('[name=gridElementos]').getSelection());
                                                                } else {
                                                                    notificaciones("Llene los campos obligatorios", 2);
                                                                }
                                                            }
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }, {
                                    xtype: "grid",
                                    name: "gridElementos",
                                    frame: true,
                                    bufferedRenderer: false,
                                    store: store,
                                    viewConfig: {
                                        plugins: {
                                            ptype: 'gridviewdragdrop',
                                            dragText: 'Arrastra y suelta para reorganizar'
                                        },
                                        enableTextSelection: true,
                                        emptyText: "<center>Sin Elementos..</center>"
                                    },
                                    height: 150,
                                    resizable: true,
                                    columns: [
                                        {text: "Título", flex: 1, dataIndex: "title"},
                                        {text: "Contenido", flex: 1, dataIndex: "content_type"},
                                        {text: "Payload", flex: 1, dataIndex: "payload", sortable: true},
                                        {text: "imagen url", flex: 1, dataIndex: "image_url", sortable: true},
                                        {flex: 1, menuDisabled: true, sortable: false, xtype: 'actioncolumn', minWidth: 20,
                                            items: [
                                                {
                                                    getClass: function (v, meta, rec) {
                                                        return 'gridAuxDelete x-fa fa-times';
                                                    },
                                                    handler: function (grid, rowIndex, colIndex) {
                                                        var rec = grid.getStore().getAt(rowIndex);
                                                        store.remove(rec);
                                                    }
                                                }
                                            ]
                                        }
                                    ],
                                    split: true,
                                    region: "north",
                                    listeners: {
                                        selectionchange: function (thisObj, selected, eOpts) {
                                            modulo.onSetActiveRecordElemento(selected, window);
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                }
            ],
            buttons: [
                {
                    xtype: 'button',
                    iconAlign: 'right',
                    text: 'Cerrar',
                    tooltip: 'Cerrar',
                    style: {
//                        background: "#26A965",
//                        border: '1px solid #36beb3',
                        '-webkit-border-radius': '5px 5px',
                        '-moz-border-radius': '5px 5px'
                    },
                    handler: function () {
                        window.close();
                    }
                },
                '->',
                {
                    text: 'Guardar Cambios',
                    tooltip: 'Registrar Cambios',
                    anchor: '100%',
                    style: {
//                        background: "#26A965",
//                        border: '1px solid #36beb3',
                        '-webkit-border-radius': '5px 5px',
                        '-moz-border-radius': '5px 5px'
                    },
                    handler: function () {
                        if (store.getData().length > 0) {
                            var quick_replies = modulo.onQuick_replies(store.getData().items);
                            if (quick_replies) {
                                var respuesta = {
                                    text: window.down('[name=text]').getValue(),
                                    quick_replies: quick_replies
                                };
                                MODULO_INTENCION.down('[name=respuesta]').setValue(JSON.stringify(respuesta));
                                window.close();
                            } else
                                notificaciones("Elementos mal formados, Revise", 2);
                        } else {
                            var respuesta = {
                                text: window.down('[name=text]').getValue()
                            };
                            MODULO_INTENCION.down('[name=respuesta]').setValue(JSON.stringify(respuesta));
                            window.close();
                        }
                    }
                }
            ]
        }).show();
        if (datos && datos.length > 2) {
            if (respuesta.text)
                window.down('[name=text]').setValue(respuesta.text);
            if (respuesta.quick_replies)
                for (var i = 0; i < respuesta.quick_replies.length; i++) {
                    var recordSelected = new Ext.data.Record(respuesta.quick_replies[i]);
                    store.add(recordSelected);
                }
        }
    },

    //--------------------------------------------------------------------------
    //ATTACHMEN
    onMensajeAtachmen: function (datos) {
        var modulo = this;
        var store = Ext.create('botWeb.store.stores.s_ElementoAtachmen');
        var respuesta;
        try {
            respuesta = JSON.parse(datos);
        } catch (e) {
            respuesta = [];
        }
        var window = new Ext.create('Ext.window.Window', {
            //title: 'Elementos de Attachment',
            closable: true,
            layout: 'fit',
            modal: true,
            height: 480,
            width: 920,
            constrain: true,
            items: [
                {
                    layout: {
                        type: 'hbox',
                        pack: 'start',
                        align: 'stretch'
                    },
                    items: [
                        {flex: 2,
                            items: [
                                {
                                    region: 'center',
                                    xtype: 'form',
                                    name: 'panel_puntos_ClientesCenter',
                                    layout: 'anchor',
                                    header: false,
                                    padding: 5,
                                    headerAsText: false,
                                    items: [
                                        {
                                            xtype: "combobox",
                                            fieldLabel: "Tipo de Elemento",
                                            value: 0,
                                            forceSelection: true,
                                            width: "40%",
                                            tooltip: "Seleccionar un elemento",
                                            name: "idElemento",
                                            emptyText: "Seleccione",
                                            reference: "text",
                                            displayField: "text",
                                            valueField: "id",
                                            filterPickList: true,
                                            anyMatch: true,
                                            queryMode: "local",
                                            growMax: 20,
//                                            afterLabelTextTpl: INFOMESSAGEREQUERID,
                                            allowOnlyWhitespace: false,
                                            blankText: "Este campo no puede ser vacío",
                                            store: Ext.create('botWeb.store.stores.s_TipoIntencionElemento'),
                                            listeners: {
                                                select: function (grid, selected, eOpts) {
                                                    document.getElementById("recursoAttachment").innerHTML = "<img src=" + selected.data.url + ">";
                                                },
                                                change: function (thisObj, newValue, oldValue, eOpts) {
                                                    var valor = true;
                                                    window.down('[name=urlImg]').setVisible(valor);
                                                    if (newValue) {
                                                        if (newValue === 'generic')
                                                            valor = false;
                                                        else if (newValue === 'list') {
                                                            valor = false;
                                                            window.down('[name=urlImg]').setVisible(valor);
                                                            window.down('[name=urlImg]').setValue("");
                                                        } else {
                                                            window.down('[name=botones]').value = "[]";
                                                            window.down('[name=subTitulo]').setValue("");
                                                        }
                                                        window.down('[name=titulo]').allowBlank = valor;
                                                        window.down('[name=subTitulo]').setVisible(!valor);
                                                        window.down('[name=List_botones]').setVisible(!valor);
                                                        window.down('[name=titulo]').setVisible(!valor);
                                                    } else {
                                                        window.down('[name=titulo]').allowBlank = true;
                                                        window.down('[name=subTitulo]').allowBlank = true;
                                                        window.down('[name=urlImg]').allowBlank = true;
                                                        window.down('[name=titulo]').setValue("");
                                                        window.down('[name=subTitulo]').setValue("");
                                                        window.down('[name=urlImg]').setValue("");
                                                        window.down('[name=botones]').setValue("[]");
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'form',
//                                            title: 'Mensaje',
                                            name: 'formMensaje',
                                            ui: 'light',
                                            padding: 5,
                                            defaults: {
                                                width: '100%',
                                                margin: '0 0 5 0',
                                                minLengthText: "Este campo requiere un mínimo de {0} caracteres",
                                                maxLengthText: "Este campo acepta un máximo de {0} caracteres",
                                                blankText: "Este campo no puede ser vacío",
                                                labelWidth: 90,
                                                allowOnlyWhitespace: true,
                                                allowBlank: false
                                            },
                                            items: [
                                                {
                                                    flex: 1,
                                                    xtype: 'container',
                                                    layout: 'hbox',
                                                    margin: '0 0 5 0',
                                                    items: [
                                                        {
                                                            flex: 1,
                                                            xtype: 'container',
                                                            layout: 'vbox',
                                                            margin: '0 0 5 0',
                                                            items: [
                                                                {
                                                                    xtype: 'textfield',
                                                                    fieldLabel: 'Titulo',
                                                                    name: 'titulo',
                                                                    emptyText: 'Titulo',
                                                                    anyMatch: true,
                                                                    allowOnlyWhitespace: false,
                                                                    maxLength: '80',
                                                                    minLengthText: "Este campo requiere un mínimo de {0} caracteres",
                                                                    maxLengthText: "Este campo acepta un máximo de {0} caracteres"
                                                                },
                                                                {
                                                                    xtype: 'textfield',
                                                                    fieldLabel: 'Subtitulo',
                                                                    name: 'subTitulo',
                                                                    emptyText: 'Subtitulo',
                                                                    maxLength: '80',
                                                                    minLengthText: "Este campo requiere un mínimo de {0} caracteres",
                                                                    maxLengthText: "Este campo acepta un máximo de {0} caracteres"
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            flex: 1,
                                                            xtype: 'container',
                                                            layout: 'vbox',
                                                            margin: '0 0 5 0',
                                                            items: [
                                                                {
                                                                    xtype: 'textfield',
                                                                    fieldLabel: 'Subtitulo',
                                                                    emptyText: 'Subtitulo',
                                                                    minLengthText: "Este campo requiere un mínimo de {0} caracteres",
                                                                    maxLengthText: "Este campo acepta un máximo de {0} caracteres",
                                                                    name: 'botones',
                                                                    hidden: true,
                                                                    value: '[]'
                                                                },
                                                                {
                                                                    xtype: 'textfield',
                                                                    fieldLabel: 'Imagen/URL',
                                                                    name: 'urlImg',
                                                                    emptyText: 'Imagen/URL',
                                                                    minLengthText: "Este campo requiere un mínimo de {0} caracteres",
                                                                    maxLengthText: "Este campo acepta un máximo de {0} caracteres"
                                                                },
                                                                Ext.create('Ext.Button', {
                                                                    height: 30,
                                                                    fieldLabel: 'Botones',
                                                                    name: 'List_botones',
                                                                    text: 'Agregar o Modificar Botones',
                                                                    handler: function () {
                                                                        modulo.onAddBotones(window);
                                                                    }
                                                                })
                                                            ]
                                                        }
                                                    ]
                                                }
                                            ],
                                            dockedItems: [
                                                {
                                                    ui: 'footer',
                                                    xtype: 'toolbar',
                                                    dock: 'bottom',
                                                    defaults: {
                                                        width: '25%',
                                                        height: 30
                                                    },
                                                    items: [
                                                        {
                                                            text: 'Limpiar',
                                                            tooltip: 'Limpiar',
                                                            disabled: false,
                                                            limpiar: true,
                                                            handler: function () {
                                                                window.down('[name=gridElementos]').getView().deselect(window.down('[name=gridElementos]').getSelection());
                                                                window.down('[name=formMensaje]').getForm().reset();
                                                            }
                                                        },
                                                        '->',
                                                        {
                                                            text: 'Crear',
                                                            tooltip: 'Crear',
                                                            name: 'btnCrear',
                                                            disabled: false,
                                                            handler: function () {
                                                                var val = window.down('[name=idElemento]').getValue();
                                                                if (val) {
                                                                    var form = window.down('[name=formMensaje]').getForm();
                                                                    if (form.isValid()) {
                                                                        var limite = 1;
                                                                        if (val === 'generic')
                                                                            limite = 10;
                                                                        else if (val === 'list')
                                                                            limite = 4;
                                                                        if (store.getData().items.length < limite) {
                                                                            var recordSelected = new Ext.data.Record(form.getValues());
                                                                            store.add(recordSelected);
                                                                            window.down('[name=formMensaje]').getForm().reset();
                                                                        } else
                                                                            notificaciones("El numero de maximo de elemetos para el tipo " + val + " es de " + limite, 2);
                                                                    } else
                                                                        notificaciones("Llene los campos obligatorios", 2);
                                                                } else
                                                                    notificaciones("Aun no se selecciono el tipo de elemento", 2);
                                                            }
                                                        },
                                                        {
                                                            text: 'Editar',
                                                            tooltip: 'Editar',
                                                            name: 'btnEditar',
                                                            disabled: true,
                                                            handler: function () {
                                                                var form = window.down('[name=formMensaje]').getForm();
                                                                if (form.isValid()) {
                                                                    store.add(new Ext.data.Record(form.getValues()));
                                                                    store.remove(window.down('[name=gridElementos]').getSelection());
                                                                } else
                                                                    notificaciones("Llene los campos obligatorios", 2);
                                                            }
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            xtype: "grid",
                                            name: "gridElementos",
                                            autoScroll: true,
                                            frame: true,
                                            bufferedRenderer: false,
                                            minHeight: 200,
                                            viewConfig: {
                                                plugins: {
                                                    ptype: 'gridviewdragdrop',
                                                    dragText: 'Arrastra y suelta para reorganizar'
                                                },
                                                enableTextSelection: true,
                                                emptyText: "<center>Sin Elementos..</center>"
                                            },
                                            resizable: true,
                                            store: store,
                                            columns: [
                                                {name: 'title', text: "Título", flex: 1, dataIndex: "titulo"},
                                                {name: 'subtitle', text: "Subtitulo", flex: 1, dataIndex: "subTitulo"},
                                                {text: "botones", flex: 1, dataIndex: "botones"},
                                                {text: "URL imagen", flex: 1, dataIndex: "urlImg", sortable: true},
                                                {flex: 1,
                                                    menuDisabled: true,
                                                    sortable: false,
                                                    xtype: 'actioncolumn',
                                                    minWidth: 20,
                                                    items: [
                                                        {
                                                            getClass: function (v, meta, rec) {
                                                                return 'gridAuxDelete x-fa fa-times';
                                                            },
                                                            handler: function (grid, rowIndex, colIndex) {
                                                                var rec = grid.getStore().getAt(rowIndex);
                                                                store.remove(rec);
                                                            }
                                                        }
                                                    ]
                                                }
                                            ],
                                            split: true,
                                            region: "north",
                                            listeners: {
                                                selectionchange: function (thisObj, selected, eOpts) {
                                                    modulo.onSetActiveRecordElemento(selected, window);
                                                }
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            buttons: [
                {
                    xtype: 'button',
                    iconAlign: 'right',
                    text: 'Cerrar',
                    tooltip: 'Cerrar',
                    style: {
//                        background: "#26A965",
//                        border: '1px solid #36beb3',
                        '-webkit-border-radius': '5px 5px',
                        '-moz-border-radius': '5px 5px'
                    },
                    handler: function () {
                        window.close();
                    }
                },
                '->',
                {
                    text: 'Guardar Cambios',
                    tooltip: 'Registrar Cambios',
                    anchor: '100%',
                    handler: function () {
                        if (store.getData().length > 0) {
                            var val = window.down('[name=idElemento]').getValue();
                            var elements = modulo.onAttachment(store.getData().items, val);
                            if (elements) {
                                var payload = {};
                                payload["template_type"] = val;
                                if (val === "generic") {
                                    payload["elements"] = elements;
                                    var respuesta = {attachment: {type: "template", payload: payload}};
                                    window.close();
                                } else if (val === "list") {
                                    payload["top_element_style"] = "compact";
                                    payload["elements"] = elements;
                                    var respuesta = {attachment: {type: "template", payload: payload}};
                                    window.close();
                                } else if (val === "image" || val === "audio" || val === "file" || val === "video") {
                                    var respuesta = {attachment: {type: val, payload: elements}};
                                    window.close();
                                } else
                                    notificaciones("Seleccione un tipo de elemento", 2);
                                MODULO_INTENCION.down('[name=respuesta]').setValue(JSON.stringify(respuesta));
                            } else
                                notificaciones("Elementos mal formados, Revise", 2);
                        } else
                            notificaciones("Falta crear un elemento", 2);
                    }
                }
            ]
        }).show();
        if (datos && datos.length > 2) {
            var tipo = respuesta.attachment.type;
            var elements = respuesta.attachment.payload.elements;
            if (tipo === 'template')
                tipo = respuesta.attachment.payload.template_type;
            window.down('[name=idElemento]').setValue(tipo);
            if (tipo === 'generic' || tipo === 'list') {
                for (var i = 0; elements && i < elements.length; i++) {
                    var datos = {};
                    elements[i].title ? datos["titulo"] = elements[i].title : 0;
                    elements[i].subtitle ? datos["subTitulo"] = elements[i].subtitle : 0;
                    elements[i].image_url ? datos["urlImg"] = elements[i].image_url : 0;
                    elements[i].buttons ? datos["botones"] = JSON.stringify(elements[i].buttons) : 0;
                    store.add(new Ext.data.Record(datos));
                }
            } else {
                if (respuesta.attachment.payload && respuesta.attachment.payload.url) {
                    var datos = {};
                    datos["urlImg"] = respuesta.attachment.payload.url;
                    store.add(new Ext.data.Record(datos));
                }
            }
        }
    },

    onQuick_replies: function (store) {
        var quickReplies = [];
        var respuesta = true;
        for (var i = 0; i < store.length; i++) {
            var dato = store[i].data;
            if (dato.content_type === "location" || dato.content_type === "user_phone_number" || dato.content_type === "user_email") {
                quickReplies.push({
                    content_type: dato.content_type
                });
            } else {
                if (dato.title !== "" && dato.content_type && dato.content_type !== "") {
                    var Replies = {content_type: dato.content_type, title: dato.title};
                    if (dato['content_type'] === 'text')
                        if (dato.payload && dato.payload.length > 0)
                            Replies['payload'] = dato.payload;
                        else
                            respuesta = false;
                    dato.image_url !== "" ? Replies['image_url'] = dato.image_url : 0;
                    quickReplies.push(Replies);
                } else {
                    respuesta = false;
                    break;
                }
            }
        }
        if (respuesta)
            return quickReplies;
        else
            return respuesta;
    },

    onAddBotones: function (vista) {
        var modulo = this;
        var botones = vista.down('[name=botones]').getValue(); //botones
        var respuesta;
        try {
            respuesta = JSON.parse(botones);
        } catch (e) {
            respuesta = [];
        }
        var storeBotones = Ext.create('Ext.data.Store', {
            extend: 'Ext.data.Model',
            fields: [
                {name: 'buttons_type'},
                {name: 'buttons_title'},
                {name: 'buttons_url'},
                {name: 'buttons_payload'}
            ]
        });
        for (var i = 0; i < respuesta.length; i++) {
            var datos = {};
            respuesta[i].type ? datos["buttons_type"] = respuesta[i].type : 0;
            respuesta[i].title ? datos["buttons_title"] = respuesta[i].title : 0;
            respuesta[i].url ? datos["buttons_url"] = respuesta[i].url : 0;
            respuesta[i].payload ? datos["buttons_payload"] = respuesta[i].payload : 0;
            storeBotones.add(new Ext.data.Record(datos));
        }
        var window = new Ext.create('Ext.window.Window', {
            title: 'Lista de Botones',
            closable: true,
            layout: 'fit',
            modal: true,
            height: 380,
            width: 820,
            bodyPadding: 10,
            constrain: true,
            items: [
                {
                    region: 'center',
                    xtype: 'form',
                    name: 'panel_puntos_ClientesCenter',
                    layout: 'anchor',
                    header: false,
                    padding: 5,
                    headerAsText: false,
                    items: [
                        {
                            xtype: 'form',
                            name: 'formMensaje',
                            ui: 'light',
                            padding: 5,
                            layout: 'hbox',
                            defaults: {
                                width: '100%',
                                margin: '0 0 5 0',
                                minLengthText: "Este campo requiere un mínimo de {0} caracteres",
                                maxLengthText: "Este campo acepta un máximo de {0} caracteres",
//                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                blankText: "Este campo no puede ser vacío",
                                labelWidth: 90,
                                allowOnlyWhitespace: true,
                                allowBlank: false
                            },
                            items: [
                                {
                                    flex: 1,
                                    fieldLabel: 'Configuracion de botones',
                                    xtype: 'container',
                                    layout: 'vbox',
                                    items: [
                                        {
                                            xtype: 'combobox',
                                            fieldLabel: 'Tipo de Boton',
                                            name: 'buttons_type',
                                            width: "90%",
                                            store: Ext.create('botWeb.store.stores.s_TipoIntencionBotonAttachment'),
                                            displayField: 'text',
                                            valueField: 'id',
                                            listeners: {
                                                change: function (thisObj, newValue, oldValue, eOpts) {
                                                    var valor = true;
                                                    if (newValue) {
                                                        if (newValue === "web_url")
                                                        {
                                                            valor = false;
                                                            window.down('[name=buttons_payload]').setValue("");
                                                        } else
                                                            window.down('[name=buttons_url]').setValue("");
                                                        window.down('[name=buttons_url]').allowBlank = valor;
                                                        window.down('[name=buttons_payload]').allowBlank = !valor;
                                                        window.down('[name=buttons_title]').allowBlank = false;
                                                        window.down('[name=buttons_payload]').setVisible(valor);
                                                        window.down('[name=buttons_url]').setVisible(!valor);
                                                    } else {
                                                        window.down('[name=buttons_payload]').allowBlank = true;
                                                        window.down('[name=buttons_url]').allowBlank = true;
                                                        window.down('[name=buttons_title]').allowBlank = true;
                                                        window.down('[name=buttons_payload]').setValue("");
                                                        window.down('[name=buttons_url]').setValue("");
                                                        window.down('[name=buttons_title]').setValue("");
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'textfield',
                                            width: "90%",
                                            fieldLabel: 'Titulo',
                                            name: 'buttons_title',
                                            emptyText: 'Titulo',
                                            allowBlank: true,
                                            minLengthText: "Este campo requiere un mínimo de {0} caracteres",
                                            maxLengthText: "Este campo acepta un máximo de {0} caracteres"
                                        }
                                    ]
                                },
                                {
                                    flex: 1,
                                    fieldLabel: 'Configuracion de botones',
                                    xtype: 'container',
                                    layout: 'vbox',
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            fieldLabel: 'URL',
                                            name: 'buttons_url',
                                            emptyText: 'URL',
                                            width: "90%",
                                            allowBlank: true,
                                            minLengthText: "Este campo requiere un mínimo de {0} caracteres",
                                            maxLengthText: "Este campo acepta un máximo de {0} caracteres"
                                        }, {
                                            xtype: 'textfield',
                                            width: "90%",
                                            fieldLabel: 'Payload',
                                            name: 'buttons_payload',
                                            emptyText: 'Payload',
                                            allowBlank: true,
                                            minLengthText: "Este campo requiere un mínimo de {0} caracteres",
                                            maxLengthText: "Este campo acepta un máximo de {0} caracteres"
                                        }
                                    ]
                                }
                            ], dockedItems: [{
                                    ui: 'footer',
                                    xtype: 'toolbar',
                                    dock: 'bottom',
                                    defaults: {
                                        width: '25%',
                                        height: 30
                                    },
                                    items: [
                                        {
                                            text: 'Limpiar',
                                            tooltip: 'Limpiar',
                                            disabled: false,
                                            limpiar: true,
                                            handler: function () {
                                                window.down('[name=gridElementos]').getView().deselect(window.down('[name=gridElementos]').getSelection());
                                                window.down('[name=formMensaje]').getForm().reset();
                                            }
                                        },
                                        '->',
                                        {
                                            text: 'Crear',
                                            tooltip: 'Crear',
                                            name: 'btnCrear',
                                            disabled: false,
                                            handler: function () {
                                                var form = window.down('[name=formMensaje]').getForm();
                                                if (storeBotones.getData().items.length < 3) {
                                                    if (form.isValid()) {
                                                        var recordSelected = new Ext.data.Record(form.getValues());
                                                        window.down('[name=gridElementos]').getStore().add(recordSelected);
                                                        window.down('[name=formMensaje]').getForm().reset();
                                                    } else {
                                                        notificaciones("Llene los campos obligatorios", 2);
                                                    }
                                                } else {
                                                    notificaciones("Se alcanzo el numero maximo de elementos", 2);
                                                }
                                            }
                                        },
                                        {
                                            text: 'Editar',
                                            tooltip: 'Editar',
                                            name: 'btnEditar',
                                            disabled: true,
                                            handler: function () {
                                                var form = window.down('[name=formMensaje]').getForm();
                                                if (form.isValid()) {
                                                    window.down('[name=gridElementos]').getStore().add(new Ext.data.Record(form.getValues()));
                                                    storeBotones.remove(window.down('[name=gridElementos]').getSelection());
                                                } else {
                                                    notificaciones("Llene los campos obligatorios", 2);
                                                }
                                            }
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: "grid",
                            name: "gridElementos",
                            width: '100%',
                            collapsible: false,
                            viewConfig: {
                                plugins: {
                                    ptype: 'gridviewdragdrop',
                                    dragText: 'Arrastra y suelta para reorganizar'
                                },
                                enableTextSelection: true,
                                emptyText: "<center>Sin Elementos..</center>"
                            },
                            resizable: true,
                            store: storeBotones,
                            columns: [
                                {text: "Tipo de boton", flex: 1, dataIndex: "buttons_type"},
                                {text: "URL del boton", flex: 1, dataIndex: "buttons_url", sortable: true},
                                {text: "Titulo del boton", flex: 1, dataIndex: "buttons_title", sortable: true},
                                {text: "Payload del boton", flex: 1, dataIndex: "buttons_payload", sortable: true},
                                {
                                    flex: 1,
                                    menuDisabled: true,
                                    sortable: false,
                                    xtype: 'actioncolumn',
                                    minWidth: 20,
                                    items: [{
                                            getClass: function (v, meta, rec) {
                                                return 'gridAuxDelete x-fa fa-times';
                                            },
                                            handler: function (grid, rowIndex, colIndex) {
                                                var rec = grid.getStore().getAt(rowIndex);
                                                storeBotones.remove(rec);
                                            }
                                        }]
                                }
                            ],
                            listeners: {
                                selectionchange: function (thisObj, selected, eOpts) {
                                    modulo.onSetActiveRecordElemento(selected, window);
                                }
                            }
                        }

                    ]
                }
            ],
            buttons: [{
                    xtype: 'button',
                    iconAlign: 'right',
                    text: 'Cerrar',
                    tooltip: 'Cerrar',
                    style: {
//                        background: "#26A965",
//                        border: '1px solid #36beb3',
                        '-webkit-border-radius': '5px 5px',
                        '-moz-border-radius': '5px 5px'
                    },
                    handler: function () {
                        window.close();
                    }
                }, '->', {
                    text: 'Guardar Cambios',
                    tooltip: 'Registrar Cambios',
                    anchor: '100%',
                    style: {
//                        background: "#26A965",
//                        border: '1px solid #36beb3',
                        '-webkit-border-radius': '5px 5px',
                        '-moz-border-radius': '5px 5px'
                    },
                    handler: function () {
                        if (storeBotones.getData().length > 0) {
                            var elements = modulo.onAttachmentButton(storeBotones.getData().items);
                            if (elements) {
                                vista.down('[name=botones]').setValue(JSON.stringify(elements));
                                window.close();
                            } else {
                                notificaciones("Elementos mal formados, Revise", 2);
                            }

                        } else {
                            notificaciones("Falta crear un elemento", 2);
                        }
                    }
                }]
        }).show();
    },

    onAttachment: function (store, tipo) {
        var attachment = [];
        var respuesta = true;
        for (var i = 0; i < store.length; i++) {
            var dato = store[i].data;
            if (dato.title !== "") {
                var array = {};
                if (tipo === "image" || tipo === "audio" || tipo === "file" || tipo === "video") {
                    dato.urlImg !== "" ? array["url"] = dato.urlImg : respuesta = false;
                    array["is_reusable"] = true;
                    return array;
                } else {
                    dato.titulo !== "" ? array["title"] = dato.titulo : respuesta = false;
                    dato.subTitulo !== "" ? array["subtitle"] = dato.subTitulo : 0;
                    dato.urlImg !== "" ? array["image_url"] = dato.urlImg : 0;
                }
                if (dato.botones && dato.botones.length > 2) {
                    try {
                        respuesta = JSON.parse(dato.botones);
                        array["buttons"] = respuesta;
                    } catch (e) {
                        true;
                    }
                }
                attachment.push(array);
            } else {
                respuesta = false;
                break;
            }
        }
        if (respuesta)
            return attachment;
        else
            return respuesta;
    },

    onAttachmentButton: function (store) {
        var attachment = [];
        var respuesta = true;
        for (var i = 0; i < store.length; i++) {
            var lista = {};
            var dato = store[i].data;
            lista["type"] = dato.buttons_type;
            dato.buttons_title !== "" ? lista["title"] = dato.buttons_title : respuesta = false;
            dato.buttons_url !== "" ? lista["url"] = dato.buttons_url : 0;
            dato.buttons_payload !== "" ? lista["payload"] = dato.buttons_payload : 0;
            attachment.push(lista);
        }
        if (respuesta)
            return attachment;
        else
            return respuesta;
    },
    onSetActiveRecordElemento(selected, window) {
        window.down('[name=formMensaje]').getForm().reset();
        window.down('[name=btnCrear]').setDisabled(selected.length);
        window.down('[name=btnEditar]').setDisabled(!selected.length);
        if (selected.length > 0) {
            window.down('[name=formMensaje]').getForm().loadRecord(selected[0]);
        }
    }
});