/* global Ext, opcionesCheck, INFOMESSAGEBLANKTEXT, formatEstadoRegistro */

/**
 * This view is an example list of people.
 */
STORE_LOGS = Ext.create('botWeb.store.logs.s_Logs');
Ext.define('botWeb.view.logs.v_Logs', {
    extend: 'Ext.panel.Panel',
    xtype: 'logsUsuario',
    controller: 'c_Logs',
    requires: [
        'Ext.layout.container.Border',
        'Ext.selection.CellModel',
        'botWeb.view.logs.c_Logs'
    ],
    layout: 'border',
    bodyBorder: false,
    defaults: {
        collapsible: true,
        collapsed: false,
        split: true,
        bodyPadding: 0
    },
    listeners: {
        afterrender: 'onView'
    },
    items: [
        {
            xtype: 'panel',
            region: 'center',
            width: '70%',
            collapseMode: 'mini',
             margin: 1,
            header: false,
//            collapsible: true,
            collapsed: false,
            collapsible: false,
//            layout: 'fit',
            items: [
                {
                    xtype: 'grid',
                    name: 'grid',
                    plugins: [{ptype: 'gridfilters'}],
                    bufferedRenderer: false,
                    store: STORE_LOGS,
                    viewConfig: {
                        deferEmptyText: false,
                        enableTextSelection: true,
                        preserveScrollOnRefresh: true,
                        listeners: {
                            loadingText: 'Cargando...'
                        }, loadMask: true,
                        emptyText: '<center><h1 style="margin:20px">No existen resultados</h1></center>'
                    },
                    tbar: [
                        {
                            xtype: "combobox",
                            fieldLabel: "Plataforma",
                            value: 0,
                            flex: 1,
                            tooltip: "Seleccionar una plataforma",
                            name: "idPlataforma",
                            emptyText: "Seleccione",
                            reference: "text",
                            displayField: "text",
                            valueField: "id",
                            filterPickList: true,
                            queryParam: "param",
                            queryMode: "remote",
                            growMax: 20,
//                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                            allowOnlyWhitespace: false,
//                                blankText: INFOMESSAGEBLANKTEXT,
                            store: Ext.create('botWeb.store.stores.s_Plataforma')

                        },
                        {
                            xtype: "combobox",
                            fieldLabel: "Intencion",
                            value: 0,
                            flex: 1,
                            tooltip: "Seleccionar una intención",
                            name: "idIntencion",
                            emptyText: "Seleccione",
                            reference: "text",
                            displayField: "text",
                            valueField: "id",
                            filterPickList: true,
                            queryParam: "param",
                            queryMode: "remote",
                            growMax: 20,
//                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                            allowOnlyWhitespace: false,
//                                blankText: INFOMESSAGEBLANKTEXT,
                            store: Ext.create('botWeb.store.stores.s_Intencion')

                        },
                        {
                            xtype: 'textfield',
                            name: 'txtParam',
                            flex: 2,
                            emptyText: 'USUARIO',
                            tip: 'Buscar por el nombre de la intención o descripción',
                            listeners: {
                                specialkey: 'onBuscar',
                                render: 'cargarToolTip'
                            }
                        },
                        {
                            xtype: 'datefield',
                            fieldLabel: 'Fecha de Consulta',
                            name: 'fecha',
                            allowBlank: false,
                            value: new Date(),
                            emptyText: 'Ej: 2018-01-01',
                            ariaHelp: 'Ej: 2018-01-01',
                            format: 'Y-m-d',
                            tip: 'Seleccionar fecha',
                            maxValue: new Date()
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-search',
                            tooltip: 'Buscar',
                            handler: 'onBuscar'
                        }, {
                            xtype: 'button',
                            iconCls: 'x-fa fa-eraser',
                            tooltip: 'Limpiar buscador',
                            handler: 'onLimpiar'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fas fa-sync',
                            tooltip: 'Recargar',
                            handler: 'onRecargar'
                        }
                    ],
                    features: [
                        {
                            ftype: 'grouping',
                            groupHeaderTpl: '{name}',
                            hideGroupedHeader: true,
                            enableGroupingMenu: true
                        }
                    ],
                    columns: [
                        Ext.create('Ext.grid.RowNumberer', {header: '#', width: 40, align: 'center'}),
                        {text: 'id Plataforma', dataIndex: 'idPlataforma', tooltip: "id Plataforma", filter: true, sortable: true, flex: 1},
                        {text: 'Lugar', dataIndex: 'lugar', tooltip: "Lugar", filter: true, sortable: true, flex: 1},
                        {text: 'Cliente', dataIndex: 'idCliente', tooltip: "Cliente", filter: true, sortable: true, flex: 1},
                        {text: 'Intencion', dataIndex: 'intencion', tooltip: "Intencion", filter: true, sortable: true, flex: 1},
                        {text: '% Aproximación', dataIndex: 'porcentajeProximacion', tooltip: "% Aproximación", filter: true, sortable: true, flex: 1},
                        {text: 'Mensaje', dataIndex: 'mensajeEntrada', tooltip: "Mensaje", filter: true, sortable: true, flex: 1},
//                        {text: 'Articulo', dataIndex: 'articulo', tooltip: "Articulo", filter: true, sortable: true, flex: 0.5},
                        {align: 'center', dataIndex: 'respuesta', text: 'Respuesta', tooltip: "Respuesta", filter: true, flex: 2, sortable: true, cellWrap: true, tdCls: 'x-change-cell', renderer: function (value) {
                                try {
                                    return JSON.stringify(JSON.parse(value)[0]);
                                } catch (e) {
                                    return value;
                                }

                            }},
                        {text: 'Fecha Registro', dataIndex: 'fechaRegistro', filter: true, xtype: 'datecolumn', format: 'Y-m-d H:i:s', sortable: true, flex: 1}
                    ],
                    listeners: {
//                        select: 'onSelect'
                    },
                    bbar: Ext.create('Ext.PagingToolbar', {
                        store: STORE_LOGS,
                        displayInfo: true,
                        name: 'paginacionGrid',
                        emptyMsg: "Sin datos que mostrar.",
                        displayMsg: 'Visualizando {0} - {1} de {2} registros',
                        beforePageText: 'Página',
                        afterPageText: 'de {0}',
                        firstText: 'Primera página',
                        prevText: 'Página anterior',
                        nextText: 'Siguiente página',
                        lastText: 'Última página',
                        refreshText: 'Actualizar',
                        inputItemWidth: 35,
                        items: [
                            {
//                                    xtype: 'button',
//                                    text: 'Exportar',
//                                    iconCls: 'x-fa fa-download',
//                                    handler: function (btn) {
//                                        onExportar(btn, "Sanciones", this.up('grid'));
//                                    }
                            }
                        ],
                        listeners: {
                            afterrender: function () {
                                this.child('#refresh').hide();
                            }
                        }
                    })
                }
            ]
        }
    ]
});
