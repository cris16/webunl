/* global Ext */

Ext.define('botWeb.store.stores.s_TipoIntencionBotonQuick', {
    extend: 'Ext.data.ArrayStore',
    data: [
        ["text", 'Texto','img/recurso/txt.gif'],
        //["location", 'Localización','img/recurso/img.gif'],        
        ["user_phone_number", 'Numero de Telefono','img/recurso/tlf.gif'],
        ["user_email", 'Correo electronico','img/recurso/eml.gif']
    ],
    fields: [
        {name: 'id'},
        {name: 'text'},
        {name: 'url'}
    ]
});