<?php

include '../../dll/config.php';
if (!$mysqli = getConectionDb()) {
    return $mysqli;
}
extract($_GET);

$sql = "SELECT count(*) as total FROM botUnl.relacion i WHERE TRUE";
if (isset($param)) {
    $sql .= " AND (i.ayuda like '%$param%')";
}

$result = $mysqli->query($sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
    return $mysqli->close();
}
$myrow = $result->fetch_assoc();
$total = $myrow['total'];
if ($total > 0) {
    $sql = "SELECT r.orden, r.entidadUno as idEntidadUno, e.nombre as entidadUno, r.entidadDos as idEntidadDos,ee.nombre as entidadDos, r.entidadTres as idEntidadTres,"
            . " eee.nombre as entidadTres, r.orden, r.ayuda, r.text, r.articulo, r.version, IF(r.habilitado = 1, 1, 0) as habilitado ,DATE_FORMAT(CONVERT_TZ(r.fechaRegistro, @@session.time_zone, '+00:00'), '%Y-%m-%dT%H:%i:%s.000Z') AS fechaRegistro"
            . " FROM botUnl.relacion r"
            . " INNER JOIN botUnl.entidad e on e.idEntidad=r.entidadUno"
            . " INNER JOIN botUnl.entidad ee on ee.idEntidad=r.entidadDos"
            . " INNER JOIN botUnl.entidad eee on eee.idEntidad=r.entidadTres"
            . " WHERE TRUE ";
    if (isset($param)) {
        $sql .= " AND (r.ayuda like '%$param%') ";
    }
    $sql .= " order by e.nombre";
    if (isset($limit)) {
        $inicio = intval($limit) * (intval($page) - 1);
        $sql .= " LIMIT $inicio, $limit ";
    } else {
        $sql .= " LIMIT $LIMITE_REGISTROS";
    }
    $result = $mysqli->query($sql);
    if (!isset($result->num_rows)) {
        echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
        return $mysqli->close();
    }
    $arreglo = [];
    while ($myrow_read = $result->fetch_assoc()) {
        $arreglo[] = array(
            'id' => ($myrow_read["idEntidadUno"] . "," . $myrow_read["idEntidadDos"] . "," . $myrow_read["idEntidadTres"] . "," . $myrow_read["orden"]),
            "entidadUno" => $myrow_read["entidadUno"],
            "idEntidadUno" => $myrow_read["idEntidadUno"],
            "idEntidadDos" => $myrow_read["idEntidadDos"],
            "entidadDos" => $myrow_read["entidadDos"],
            "idEntidadTres" => $myrow_read["idEntidadTres"],
            "entidadTres" => $myrow_read["entidadTres"],
            "orden" => $myrow_read["orden"],
            "ayuda" => $myrow_read["ayuda"],
            "text" => $myrow_read["text"],
            "articulo" => $myrow_read["articulo"],
            "version" => $myrow_read["version"],
            "habilitado" => $myrow_read["habilitado"],
            "fechaRegistro" => $myrow_read["fechaRegistro"]
        );
    }
    echo json_encode(array('success' => TRUE, 'data' => $arreglo, 'total' => $total));
} else
    echo json_encode(array('success' => true, 'data' => [], 'total' => $total));
$mysqli->close();
