/* global Ext, MODULO_ENTIDADES */

Ext.define('botWeb.store.entidades.s_Entidades', {
    extend: 'Ext.data.Store',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'nivel', type: 'int'},
        {name: 'text'},
        {name: 'nombre'},
        {name: 'habilitado'},
        {name: 'fechaRegistro'}
    ],
    pageSize: 250,
    remoteSort: false,
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/entidades/read.php',
            create: 'php/entidades/create.php',
            update: 'php/entidades/update.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'data',
            totalProperty: 'total',
            successProperty: 'success'
        },
        simpleSortMode: true
    },
    onCreateRecords: function (records, operation, success) {
        if (!success) {
            var res = JSON.parse(operation._response.responseText);
            Ext.MessageBox.show({
                title: 'Atención!',
                msg: res['message'],
                buttons: Ext.MessageBox.OK
            });
        }
    },
    listeners: {
        beforeload: function (store, operation, eOpts) {
            if (MODULO_ENTIDADES) {
                var params = {
                    param: MODULO_ENTIDADES.down('[name=txtParam]').getValue()
                };
                store.proxy.extraParams = params;
            }
        },
        load: function (thisObj, records, successful, operation, eOpts) {
            if (!successful) {
                var obj = {};
                if (isJsonString(operation._response.responseText)) {
                    obj = JSON.parse(operation._response.responseText);
                    if (!obj.success)
                        if (obj.error)
                            Ext.toast(obj.error, 'ERROR');
                        else if (obj.message)
                            Ext.toast(obj.message, 'Mensaje');
                        else
                            Ext.toast('Problema en la comunicación con el servidor local', 'ERROR', );
                } else {
                    obj.resAjax = -2;
                    obj['error'] = "Exite un error al crear el JSON";
                    obj['message'] = "Existe un error con la solicitud realizada, inténtelo mas tarde por favor.";
                    Ext.toast(obj.message, 'Mensaje');
                }
            }
        }
    }


});
