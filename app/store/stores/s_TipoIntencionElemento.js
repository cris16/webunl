/* global Ext */

Ext.define('botWeb.store.stores.s_TipoIntencionElemento', {
    extend: 'Ext.data.ArrayStore',
    data: [
        ["generic", 'Lista horizontal','img/recurso/lh.gif'],
        ["image",'Imagen','img/recurso/img.gif'],
        ["audio",'Audio','img/recurso/aud.gif'],
        ["file",'Archivo','img/recurso/arch.gif'],
        ["video",'Video','img/recurso/vid.gif']
        
    ],
    fields: [
        {name: 'id'},
        {name: 'text'},
        {name: 'url'}
    ]
});