<?php

include '../../dll/config.php';
if (!$mysqli = getConectionDb()) {
    return $mysqli;
}
extract($_GET);

$sql = "SELECT count(*) as total FROM botUnl.intenciones i WHERE TRUE";
if (isset($param)) {
    $sql .= " AND (i.nombre like '%$param%' OR i.text like '%$param%')";
}

$result = $mysqli->query($sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
    return $mysqli->close();
}
$myrow = $result->fetch_assoc();
$total = $myrow['total'];
if ($total > 0) {
    $sql = "SELECT idIntenciones, idIntencionTipo, nombre, parametro, text, IF(i.habilitado = 1, 1, 0) as habilitado, DATE_FORMAT(CONVERT_TZ(fechaRegistro, @@session.time_zone, '+00:00'), '%Y-%m-%dT%H:%i:%s.000Z') AS fechaRegistro, ifnull(respuesta,'{}')as respuesta"
            . " FROM botUnl.intenciones i WHERE TRUE ";
    if (isset($param)) {
        $sql .= " AND (i.nombre like '%$param%' OR i.text like '%$param%') ";
    }
    if (isset($limit)) {
        $inicio = intval($limit) * (intval($page) - 1);
        $sql .= " LIMIT $inicio, $limit ";
    } else {
        $sql .= " LIMIT $LIMITE_REGISTROS";
    }
    $result = $mysqli->query($sql);
    if (!isset($result->num_rows)) {
        echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
        return $mysqli->close();
    }
    $arreglo = [];
    while ($myrow_read = $result->fetch_assoc()) {
        $arreglo[] = array(
            'id' => intval($myrow_read["idIntenciones"]),
            'idIntencionTipo' => intval($myrow_read["idIntencionTipo"]),
            'nombre' => ($myrow_read["nombre"]),
            'parametro' => ($myrow_read["parametro"]),
            'text' => ($myrow_read["text"]),
            'habilitado' => ($myrow_read["habilitado"]),
            'respuesta' => ($myrow_read["respuesta"]),
            'fechaRegistro' => ($myrow_read["fechaRegistro"]),
        );
    }
    echo json_encode(array('success' => TRUE, 'data' => $arreglo, 'total' => $total, 'message' => "Información encontrada con éxito.", 'hora' => date_default_timezone_get()));
} else {
    echo json_encode(array('success' => false, 'data' => [], 'total' => $total, 'message' => "No se encontraron resultados."));
}
$mysqli->close();
