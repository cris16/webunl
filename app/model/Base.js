Ext.define('botWeb.model.Base', {
    extend: 'Ext.data.Model',

    schema: {
        namespace: 'botWeb.model'
    }
});
