<?php

include '../../dll/config.php';
//include '../../dll/funciones.php';
extract($_GET);
extract($_POST);
if (!$mysqli = getConectionDb())
    return $mysqli;
$sql = "SELECT idPlataforma, nombre  "
        . "FROM $DB_NAME.plataforma WHERE true ";
if (isset($param) && ($param !== '')) {
    $sql .= " AND (LOWER(nombre) LIKE LOWER('%$param%') OR  LOWER(idPlataforma) LIKE LOWER('%$param%'))";
}
if (isset($idPlataforma) && ($idPlataforma !== '')) {
    $sql .= " AND idPlataforma=$idPlataforma";
}

if (isset($limite)) {
    $sql .= " LIMIT $limite";
} else {
    $sql .= " LIMIT $LIMITE_REGISTROS";
}
$result = $mysqli->query($sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS", 'sql' => $sql));
    return $mysqli->close();
}
$arreglo = [];
while ($myrow_read_users = $result->fetch_assoc()) {
    $arreglo[] = array(
        'id' => intval($myrow_read_users["idPlataforma"]),
        'text' => $myrow_read_users["nombre"]
    );
}
$mysqli->close();
echo json_encode(array('success' => true, 'data' => $arreglo));

