<?php

include '../../dll/config.php';
//include '../../dll/funciones.php';
extract($_GET);
extract($_POST);
if (!$mysqli = getConectionDb())
    return $mysqli;
$sql = "SELECT idEntidadSinonimo,nombre,IF(s.habilitado = 1, 1, 0) as habilitado,fechaRegistro "
        . "FROM $DB_NAME.entidadSinonimo s WHERE TRUE";

if (isset($idEntidad) && $idEntidad != '') {
    $sql .= " and s.idEntidad = $idEntidad ";
}

//if (isset($limite)) {
//    $sql .= " LIMIT $limite";
//} else {
//    $sql .= " LIMIT $LIMITE_REGISTROS";
//}
$result = $mysqli->query($sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS", 'sql' => $sql));
    return $mysqli->close();
}
$arreglo = [];
while ($myrow_read_users = $result->fetch_assoc()) {
    $arreglo[] = array(
        'id' => intval($myrow_read_users["idEntidadSinonimo"]),
        'text' => ($myrow_read_users["nombre"]),
        'habilitado' => $myrow_read_users["habilitado"],
        'fechaRegistro' => $myrow_read_users["fechaRegistro"],
    );
}
$mysqli->close();
echo json_encode(array('success' => true, 'data' => $arreglo));

