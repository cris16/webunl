/* global Ext */

Ext.define('botWeb.store.stores.s_TipoIntencionMensaje', {
    extend: 'Ext.data.ArrayStore',
    data: [
        ['quick_replies', 'Quick Replies'],
        ['attachment', 'Attachment'],
//        ["receipt",'Orden de recibo'],
        ["button",'Lista de botones'],
        ["text",'Texto']
    ],
    fields: [
        {name: 'id'},
        {name: 'text'}
    ]
});