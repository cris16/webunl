<?php

include '../../dll/config.php';
if (!$mysqli = getConectionDb()) {
    return $mysqli;
}
extract($_GET);

$sql = "SELECT count(*) as total FROM botUnl.usuario u WHERE TRUE ";

if (isset($param)) {
    $sql .= " AND (u.nombre like '%$param%' OR u.apellido like '%$param%' OR u.usuario like '%$param%')";
}

$result = $mysqli->query($sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
    return $mysqli->close();
}
$myrow = $result->fetch_assoc();
$total = $myrow['total'];

if ($total > 0) {
    $sql = "Select idUsuario, usuario, '********' as pass, rol, nombre, apellido, if(habilitado=0,0,1) as habilitado,  DATE_FORMAT(CONVERT_TZ(fechaRegistro, @@session.time_zone, '+00:00'), '%Y-%m-%dT%H:%i:%s.000Z') AS  fechaRegistro "
            . " FROM botUnl.usuario u WHERE TRUE";
    if (isset($param)) {
        $sql .= " AND (u.nombre like '%$param%' OR u.apellido like '%$param%' OR u.usuario like '%$param%')";
    }

    if (isset($limit)) {
        $inicio = intval($limit) * (intval($page) - 1);
        $sql .= " LIMIT $inicio, $limit ";
    } else {
        $sql .= " LIMIT $LIMITE_REGISTROS";
    }
    $result = $mysqli->query($sql);
    if (!isset($result->num_rows)) {
        echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
        return $mysqli->close();
    }
    $arreglo = [];
    while ($myrow_read = $result->fetch_assoc()) {
        $arreglo[] = array(
            'id' => intval($myrow_read["idUsuario"]),
            'usuario' => ($myrow_read["usuario"]),
            'pass' => ($myrow_read["pass"]),
            'rol' => ($myrow_read["rol"]),
            'nombre' => ($myrow_read["nombre"]),
            'apellido' => ($myrow_read["apellido"]),
            'habilitado' => ($myrow_read["habilitado"]),
            'fechaRegistro' => ($myrow_read["fechaRegistro"]),
        );
    }
    echo json_encode(array('success' => TRUE, 'data' => $arreglo, 'total' => $total));
} else {
    echo json_encode(array('success' => true, 'data' => [], 'total' => $total));
}
$mysqli->close();
