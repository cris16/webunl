/* global Ext, MODULO_INTENCION, MODULO_USUARIOS */

Ext.define('botWeb.store.usuarios.s_Usuarios', {
    extend: 'Ext.data.Store',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'usuario'},
        {name: 'nombre'},
        {name: 'apellido'},
        {name: 'pass'},
        {name: 'habilitado'},
        {name: 'rol'},
        {name: 'fechaRegistro'}
    ],
    pageSize: 250,
    remoteSort: false,
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/usuarios/read.php',
            create: 'php/usuarios/create.php',
            update: 'php/usuarios/update.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'data',
            totalProperty: 'total',
            successProperty: 'success'
        },
        simpleSortMode: true,
        metachange: function (dsfds, meta, eOpts) {
            console.log("", meta, eOpts);
        }
    },
    listeners: {
        beforeload: function (store, operation, eOpts) {
            if (MODULO_USUARIOS) {
                var params = {
                    param: MODULO_USUARIOS.down('[name=txtParam]').getValue()
                };
                store.proxy.extraParams = params;
            }
        },
        load: function (thisObj, records, successful, operation, eOpts) {
            if (!successful) {
                var obj = {};
                if (isJsonString(operation._response.responseText)) {
                    obj = JSON.parse(operation._response.responseText);
                    if (!obj.success)
                        if (obj.error)
                            Ext.toast(obj.error, 'ERROR');
                        else if (obj.message)
                            Ext.toast(obj.message, 'Mensaje');
                        else
                            Ext.toast('Problema en la comunicación con el servidor local', 'ERROR', );
                } else {
                    obj.resAjax = -2;
                    obj['error'] = "Exite un error al crear el JSON";
                    obj['message'] = "Existe un error con la solicitud realizada, inténtelo mas tarde por favor.";
                    Ext.toast(obj.message, 'Mensaje');
                }
            }
        }
    }


});
