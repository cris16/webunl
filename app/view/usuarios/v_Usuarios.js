/* global Ext, opcionesCheck, INFOMESSAGEBLANKTEXT, formatEstadoRegistro */

/**
 * This view is an example list of people.
 */
STORE_USUARIOS = Ext.create('botWeb.store.usuarios.s_Usuarios');
Ext.define('botWeb.view.usuarios.v_Usuarios', {
    extend: 'Ext.panel.Panel',
    xtype: 'usuarios',
    controller: 'c_Usuarios',
//    title: 'intencion',
    requires: [
        'Ext.layout.container.Border',
        'Ext.selection.CellModel',
        'botWeb.view.usuarios.c_Usuarios'
    ],
    layout: 'border',
    bodyBorder: false,
    defaults: {
        collapsible: true,
        collapsed: false,
        split: true,
        bodyPadding: 0
    },
    listeners: {
        afterrender: 'onView'
    },
    items: [
        {
            xtype: 'panel',
            region: 'center',
            width: '70%',
            collapseMode: 'mini',
            margin: 1,
            header: false,
//            collapsible: true,
            collapsed: false,
            collapsible: false,
//            layout: 'fit',
            items: [
                {
                    xtype: 'grid',
                    name: 'grid',
                    plugins: [{ptype: 'gridfilters'}],
                    bufferedRenderer: false,
                    store: STORE_USUARIOS,
                    viewConfig: {
                        deferEmptyText: false,
                        enableTextSelection: true,
                        preserveScrollOnRefresh: true,
                        listeners: {
                            loadingText: 'Cargando...'
                        }, loadMask: true,
                        emptyText: '<center><h1 style="margin:20px">No existen resultados</h1></center>'
                    },
                    tbar: [
                        {
                            xtype: 'textfield',
                            name: 'txtParam',
                            flex: 2,
                            emptyText: 'Nombre, Apellido, usuario..',
                            tip: 'Buscar por el nombre de la intención o descripción',
                            listeners: {
                                specialkey: 'onBuscar',
                                render: 'cargarToolTip'
                            }
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-search',
                            tooltip: 'Buscar',
                            handler: 'onBuscar'
                        }, {
                            xtype: 'button',
                            iconCls: 'x-fa fa-eraser',
                            tooltip: 'Limpiar buscador',
                            handler: 'onLimpiar'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fas fa-sync',
                            tooltip: 'Recargar',
                            handler: 'onRecargar'
                        }
                    ],
                    features: [
                        {
                            ftype: 'grouping',
                            groupHeaderTpl: '{name}',
                            hideGroupedHeader: true,
                            enableGroupingMenu: true
                        }
                    ],
                    columns: [
                        Ext.create('Ext.grid.RowNumberer', {header: '#', width: 30, align: 'center'}),
                        {text: 'Nombre', dataIndex: 'nombre', tooltip: "nombre", filter: true, sortable: true, flex: 1},
                        {text: 'Apellido', dataIndex: 'apellido', tooltip: "apellido", filter: true, sortable: true, flex: 1},
                        {text: 'Usuario', dataIndex: 'usuario', filter: true, sortable: true, flex: 1},
                        {text: 'rol', dataIndex: 'rol', filter: true, sortable: true, flex: 1},
                        {text: 'Fecha Registro', dataIndex: 'fechaRegistro', filter: true, xtype: 'datecolumn', format: 'Y-m-d H:i:s', sortable: true, flex: 1},
                        {filter: true, tooltip: "Habilitado", text: "Habilitado", width: 100, dataIndex: 'habilitado', sortable: true, renderer: formatEstadoRegistro}
                    ],
                    listeners: {
                        select: 'onSelect'
                    },
                    bbar: Ext.create('Ext.PagingToolbar', {
                        store: STORE_USUARIOS,
                        displayInfo: true,
                        name: 'paginacionGrid',
                        emptyMsg: "Sin datos que mostrar.",
                        displayMsg: 'Visualizando {0} - {1} de {2} registros',
                        beforePageText: 'Página',
                        afterPageText: 'de {0}',
                        firstText: 'Primera página',
                        prevText: 'Página anterior',
                        nextText: 'Siguiente página',
                        lastText: 'Última página',
                        refreshText: 'Actualizar',
                        inputItemWidth: 35,
                        items: [
                            {
//                                    xtype: 'button',
//                                    text: 'Exportar',
//                                    iconCls: 'x-fa fa-download',
//                                    handler: function (btn) {
//                                        onExportar(btn, "Sanciones", this.up('grid'));
//                                    }
                            }
                        ],
                        listeners: {
                            afterrender: function () {
                                this.child('#refresh').hide();
                            }
                        }
                    })
                }
            ]
        },
        {
            xtype: 'panel',
            region: 'east',
            padding: 1,
            width: '30%',
            collapseMode: 'mini',
            anchor: '100% 100%',
            layout: 'anchor',
            cls: 'panelCrearEditar',
            autoScroll: true,
            collapsible: true,
            collapsed: false,
            header: false,
            headerAsText: false,
            items: [
                {
                    xtype: 'form',
//                    title: 'Tipo de Usuarioses',
                    name: 'form',
                    cls: 'quick-graph-panel shadow panelComplete',
                    ui: 'light',
                    padding: 1,
                    defaults: {
                        width: '100%',
                        margin: '0 0 5 0',
                        defaultType: 'textfield',
//                        minLengthText: MINIMUMMESSAGUEREQUERID,
//                        maxLengthText: MAXIMUMMESSAGUEREQURID,
//                        afterLabelTextTpl: INFOMESSAGEREQUERID,
                        blankText: INFOMESSAGEBLANKTEXT,
                        labelWidth: 90,
                        allowOnlyWhitespace: false,
                        allowBlank: false
                    },
                    items: [{
                            xtype: "combobox",
                            fieldLabel: "Rol",
                            value: 0,
                            width: "100%",
                            tooltip: "Seleccionar el tipo de rol",
                            name: "rol",
                            emptyText: "Seleccione",
                            reference: "text",
                            displayField: "text",
                            valueField: "id",
                            filterPickList: true,
                            queryParam: "param",
                            queryMode: "remote",
                            growMax: 20,
//                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                            allowOnlyWhitespace: false,
//                                blankText: INFOMESSAGEBLANKTEXT,
                            store: Ext.create('botWeb.store.stores.s_TipoRol')

                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Nombre',
                            name: 'nombre',
                            minLength: 3,
                            maxLength: 45
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Apellido',
                            name: 'apellido',
                            minLength: 3,
                            maxLength: 45
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Usuario',
                            name: 'usuario',
                            minLength: 3,
                            maxLength: 45
                        },
                        {
                            xtype: 'textfield',
                            inputType: 'password',
                            allowBlank: false,
                            required: true,
//                            msgTarget: 'qtip',
                            fieldLabel: 'Password',
                            name: 'pass',
                            placeholder: 'password',
//                            margin: "45 0 0 0",
//                            responsiveConfig: {
//                                'desktop': {
//                                    msgTarget: 'side'
//                                }
//                            }
                        },
                        {
                            xtype: 'checkboxgroup',
                            allowBlank: true,
                            allowOnlyWhitespace: true,
                            items: [
                                {xtype: 'checkboxfield', uncheckedValue: 0, name: 'habilitado', boxLabel: 'Habilitado', inputValue: 1}
                            ]
                        }
                    ], dockedItems: [{
                            ui: 'footer',
                            xtype: 'toolbar',
                            dock: 'bottom',
                            defaults: {
                                width: '25%',
                                height: 30
                            },
                            items: [
                                {
                                    text: 'Limpiar',
                                    tooltip: 'Limpiar',
                                    disabled: false,
                                    limpiar: true,
                                    handler: 'onLimpiarFormulario'
                                },
                                '->',
                                {
                                    text: 'Crear',
                                    tooltip: 'Crear',
                                    name: 'btnCrear',
                                    handler: 'onCrear',
                                    disabled: false
                                },
                                {
                                    text: 'Editar',
                                    tooltip: 'Editar',
                                    name: 'btnEditar',
                                    handler: 'onEditar',
                                    disabled: false
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ]
});
