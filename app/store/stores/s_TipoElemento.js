/* global Ext */

Ext.define('botWeb.store.stores.s_TipoElemento', {
    extend: 'Ext.data.ArrayStore',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'title', type: 'string'},
        {name: 'text', type: 'string'},
        {name: 'subtitle', type: 'string'},
        {name: 'content_type', type: 'string'},
        {name: 'buttons_title', type: 'string'},
        {name: 'buttons_payload', type: 'string'},
        {name: 'buttons_type', type: 'string'},
        {name: 'buttons_url', type: 'string'},
        {name: 'payload', type: 'string'},
        {name: 'image_url', type: 'string'}
    ],
    data: [
       
    ]
});