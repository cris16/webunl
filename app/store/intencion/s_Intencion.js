/* global Ext, MODULO_INTENCION */

Ext.define('botWeb.store.intencion.s_Intencion', {
    extend: 'Ext.data.Store',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'idIntencionTipo', type: 'int'},
        {name: 'nombre'},
        {name: 'parametro'},
        {name: 'text'},
        {name: 'respuesta'},
        {name: 'habilitado'},
        {name: 'fechaRegistro'}
    ],
    pageSize: 250,
    remoteSort: false,
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/intencion/read.php',
            create: 'php/intencion/create.php',
            update: 'php/intencion/update.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'data',
            totalProperty: 'total',
            successProperty: 'success',
            messageProperty: 'error'
        }
    },
    onCreateRecords: function (records, operation, success) {
        if (!success) {
            var res = JSON.parse(operation._response.responseText);
            Ext.MessageBox.show({
                title: 'Atención!',
                msg: res['message'],
                buttons: Ext.MessageBox.OK
            });
        }
    },
    listeners: {
        beforeload: function (store, operation, eOpts) {
            if (MODULO_INTENCION) {
                var params = {
                    param: MODULO_INTENCION.down('[name=txtParam]').getValue()
                };
                store.proxy.extraParams = params;
            }
        },
        load: function (thisObj, records, successful, operation, eOpts) {
//            console.log("thisObj, records, successful, operation", records, successful, operation);
            if (successful) {
                var obj = {};
                if (operation && operation._response && operation._response.responseJson) {
                    obj = operation._response.responseJson;
                    if (!obj.success) {
                        if (obj.error)
                            Ext.toast(obj.error, 'ERROR');
                    } else if (obj.message)
                        Ext.toast(obj.message, 'Mensaje');
                    else
                        Ext.toast('Problema en la comunicación con el servidor local', 'ERROR', 2);
                } else {
                    obj.resAjax = -2;
                    obj['error'] = "Exite un error al crear el JSON";
                    obj['message'] = "Existe un error con la solicitud realizada, inténtelo mas tarde por favor.";
                    Ext.toast(obj.message, 'Mensaje');
                }
            }
        },
        beforesync: function (options) {
            console.log("beforesync", options);

        }
    }


});
