/*
 * This file launches the application by asking Ext JS to create
 * and launch() the Application class.
 */
/* global Ext */

Ext.application({
    extend: 'botWeb.Application',

    name: 'botWeb',

    requires: [
        // This will automatically load all classes in the botWeb namespace
        // so that application classes do not need to require each other.
        'botWeb.*'
    ],

    // The name of the initial view to create.
    mainView: 'botWeb.view.main.Main'
});
