/* global Ext, URL_SERVICIOS */
var MODULO_ENTIDADES;
Ext.define('botWeb.view.entidades.c_Entidades', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.c_Entidades',
    onView: function (panelLoad) {
        MODULO_ENTIDADES = panelLoad;
        MODULO_ENTIDADES.down('[name=grid]').getStore().load();
        MODULO_ENTIDADES.down('[name=btnEditar]').disable();
        if (document.body) {
            HEIGT_VIEWS = (document.body.clientHeight);
        } else {
            HEIGT_VIEWS = (window.innerHeight);

        }
        HEIGT_VIEWS = HEIGT_VIEWS - 65;
        MODULO_ENTIDADES.setHeight(HEIGT_VIEWS);
        MODULO_ENTIDADES.down('[name=grid]').setHeight(HEIGT_VIEWS);
        MODULO_ENTIDADES.down('[name=form]').setHeight(HEIGT_VIEWS);
    },
    onRecargar: function () {
        MODULO_ENTIDADES.down('[name=grid]').getStore().reload();
    },
    onLimpiarFormulario: function (btn, e) {
        if (btn.limpiar) {
            MODULO_ENTIDADES.down('[name=form]').getForm().reset();
            var grid = MODULO_ENTIDADES.down('[name=grid]');
            grid.getView().deselect(grid.getSelection());
            MODULO_ENTIDADES.down('[name=form]').down('[name=gridSinonimos]').getStore().removeAll();
        }
        MODULO_ENTIDADES.down('[name=grid]').getStore().load();
        MODULO_ENTIDADES.down('[name=btnEditar]').disable();
        MODULO_ENTIDADES.down('[name=btnCrear]').enable();
    },
    onLimpiar: function (btn, e) {
        MODULO_ENTIDADES.down('[name=txtParam]').reset();
        if (btn.limpiar) {
            MODULO_ENTIDADES.down('[name=form]').getForm().reset();
            var grid = MODULO_ENTIDADES.down('[name=grid]');
            grid.getView().deselect(grid.getSelection());
            MODULO_ENTIDADES.down('[name=form]').down('[name=gridSinonimos]').getStore().removeAll();
        }
        MODULO_ENTIDADES.down('[name=grid]').getStore().load();
        MODULO_ENTIDADES.down('[name=btnEditar]').disable();
        MODULO_ENTIDADES.down('[name=btnCrear]').enable();
    },
    onSelect: function (grid, selected, eOpts) {
        if (!MODULO_ENTIDADES.down('[name=form]').down('[name=nivel]').getStore().getById(selected['data']['nivel'])) {
            MODULO_ENTIDADES.down('[name=form]').down('[name=nivel]').getStore().proxy.extraParams = {param: ""};
            MODULO_ENTIDADES.down('[name=form]').down('[name=nivel]').getStore().reload({
                params: {idIntencionTipo: selected['data']['nivel']},
                callback: function (records) {
                    if (records.length > 0) {
                        MODULO_ENTIDADES.down('[name=form]').down('[name=nivel]').setValue(selected['data']['nivel']);
                    }
                }
            });
        }
        MODULO_ENTIDADES.down('[name=form]').down('[name=gridSinonimos]').getStore().removeAll();
        if (selected && selected['data']) {
            var gridProyecto = MODULO_ENTIDADES.down('[name=form]').down('[name=gridSinonimos]').getStore();
            if (gridProyecto.getData().length <= 0) {
                gridProyecto.proxy.extraParams = {idEntidad: selected.get('id')};
//                mostrarBarraProgreso("Carganto la Información");
                gridProyecto.reload({callback: function (records, operation, success) {
//                        ocultarBarraProgreso();
                    }
                });
            }
        }

        MODULO_ENTIDADES.down('[name=form]').getForm().loadRecord(selected);
        MODULO_ENTIDADES.down('[name=btnEditar]').enable();
        MODULO_ENTIDADES.down('[name=btnCrear]').disable();
    },
    onCrear: function () {
        var me = this, form = MODULO_ENTIDADES.down('[name=form]').getForm();
        var dataEntrenamiento = me.onDatosExtras(MODULO_ENTIDADES.down('[name=form]'));
        var gridLeer = MODULO_ENTIDADES.down('[name=grid]');
        if (form.isValid()) {
            gridLeer.getStore().proxy.extraParams = dataEntrenamiento;

            MODULO_ENTIDADES.down('[name=grid]').getStore().insert(0, form.getValues());
            MODULO_ENTIDADES.down('[name=grid]').getStore().sync({
//                callback: function (response) {
//                    onProcesarPeticion(response, me.onLimpiarFormulario({limpiar: true}));
//                },
                success: function (response, options) {
                    notificaciones("Registro creado", 'Mensaje', 1);
                    me.onLimpiarFormulario({limpiar: true});
                },
                failure: function (response, options) {
                    notificaciones("Algo ocurrio, intene más tarde", 'Mensaje', 2);
                    me.onLimpiarFormulario({limpiar: true});
                }
            });
        } else
            mensajesValidacionForms(form.getFields());
    },
    onEditar: function () {
        var me = this, form = MODULO_ENTIDADES.down('[name=form]').getForm();
        var dataEntrenamiento = me.onDatosExtras(MODULO_ENTIDADES.down('[name=form]'));
        var gridLeer = MODULO_ENTIDADES.down('[name=grid]');
        var gridSelect = gridLeer.getSelection();
        if (dataEntrenamiento['total'] > 0)
            gridSelect[0].set('dataEntrenamiento', true);
        if (form.isValid()) {
            form.updateRecord(form.activeRecord);
            gridLeer.getStore().proxy.extraParams = dataEntrenamiento;
            MODULO_ENTIDADES.down('[name=grid]').getStore().sync({
//                callback: function (response) {
//                    onProcesarPeticion(response, me.onLimpiarFormulario({limpiar: true}));
//                },
                success: function (response, options) {
                    notificaciones("Cambios realizados", 'Mensaje', 1);
                    me.onLimpiarFormulario({limpiar: true});
                },
                failure: function (response, options) {
                    notificaciones("Algo ocurrio, intene más tarde", 'Mensaje', 2);
                    me.onLimpiarFormulario({limpiar: true});
                }
            });
        } else
            mensajesValidacionForms(form.getFields());
    },
    onBuscar: function (btn, e) {
        var txtParam = MODULO_ENTIDADES.down('[name=txtParam]').getValue(), params = {}, me = this;
        params['param'] = txtParam;
        if (btn.xtype === 'button' || e.event.keyCode === 13) {
            me.onLimpiarFormulario({limpiar: true});
            MODULO_ENTIDADES.down('[name=paginacionGrid]').moveFirst();
            MODULO_ENTIDADES.down('[name=grid]').getStore().load({
                params: params,
                callback: function (records, operation, success) {
                    if (!success)
                        setMensajeGridEmptyText(MODULO_ENTIDADES.down('[name=grid]'), '<h3>Problema en la comunicación con el servidor local, por favor inténtelo mas tarde.</h3>');
                    else if (records.length === 0)
                        setMensajeGridEmptyText(MODULO_ENTIDADES.down('[name=grid]'), '<h3>No existen resultados.</h3>');
                }
            });
        }
    },
    cargarToolTip: function (c) {
        Ext.create('Ext.tip.ToolTip', {
            target: c.getEl(),
            html: c.tip
        });
    },
    onAgregarElementos: function () {
        MODULO_ENTIDADES.down('[name=form]').down('[name=gridSinonimos]').getStore().add(new Ext.data.Record({text: "", habilitado: 1, nuevo: true}));
    },
    onDeleteEntrenamiento: function (grid, rowIndex, colIndex) {
        var ventana = MODULO_ENTIDADES.down('[name=form]');
        var gridPerfil = ventana.down('[name=gridEntrenamiento]');
        var rec = grid.getStore().getAt(rowIndex);
        if (rec.data.nuevo)
            MODULO_ENTIDADES.down('[name=form]').down('[name=gridSinonimos]').getStore().remove(rec);
        else {
            rec.set('actualizar', true);
            if (rec.data.habilitado)
                rec.set('habilitado', 0);
            else
                rec.set('habilitado', 1);
        }
    },
    onDatosExtras: function (ventanaPerfil, gridProyecto) {
        var listPerfiles = [];
        var dataAuxPerfil = ventanaPerfil.down('[name=gridSinonimos]').getStore().data.items;
        for (var i in dataAuxPerfil) {
            if (dataAuxPerfil[i].data.nuevo)
                listPerfiles.push({id: dataAuxPerfil[i].data.id, text: dataAuxPerfil[i].data.text, habilitado: dataAuxPerfil[i].data.habilitado, nuevo: dataAuxPerfil[i].data.nuevo});
        }
        var dataAuxPermisosAdmin = ventanaPerfil.down('[name=gridSinonimos]').getStore().getUpdatedRecords();
        for (var i in dataAuxPermisosAdmin) {
            listPerfiles.push({id: dataAuxPermisosAdmin[i].data.id, text: dataAuxPerfil[i].data.text, habilitado: dataAuxPermisosAdmin[i].data.habilitado, nuevo: false});
        }
        return {dataEntrenamiento: JSON.stringify(listPerfiles), total: listPerfiles.length};
    },
    onSinonimo: function (btn, e) {
        MODULO_ENTIDADES.down('[name=ventanaSinonimo]').show();
        MODULO_ENTIDADES.down('[name=ventanaSinonimo]').down('[name=gridSinonimo]').getStore().removeAll();
        MODULO_ENTIDADES.down('[name=txtSinonimo]').setValue("");
    },
    onBuscarSinonimo: function (btn, e) {
        var txtParam = MODULO_ENTIDADES.down('[name=txtSinonimo]').getValue(), params = {}, me = this;
        if (!txtParam, txtParam.length < 2)
            notificaciones("No tiene el numero minimo de caracteres", 'Mensaje', 2);
        else if (txtParam.split(" ").length > 1)
            notificaciones("La busqueda es de una palabra", 'Mensaje', 2);
        else {
            txtParam = txtParam.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();
            Ext.Ajax.request({
                url: URL_SERVICIOS + "buscarSinonimo",
                method: 'POST',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                params: {sinonimo: txtParam},
                success: function (response) {//gridSinonimo
                    var res = JSON.parse(response.responseText);
                    if (res['en'] == -1)
                        notificaciones(res['m'], 'Mensaje', 2);
                    else
                        notificaciones(res['m'], 'Mensaje', 1);
                    MODULO_ENTIDADES.down('[name=ventanaSinonimo]').down('[name=gridSinonimo]').getStore().setData(res['data']);
                }, failure: function () {
                    alert('failure');
                }
            });
        }
    },
    onGuardar: function () {
        var data = MODULO_ENTIDADES.down('[name=ventanaSinonimo]').down('[name=gridSinonimo]').getSelection();        
        if (data.length > 0) {//gridSinonimos
            for (var i = 0; i < data.length; i++) {
                var sinonimo = data[i]['data'];
                if (!isInStore(MODULO_ENTIDADES.down('[name=form]').down('[name=gridSinonimos]').getStore(), sinonimo['text'], 'text', 'exact'))
                    MODULO_ENTIDADES.down('[name=form]').down('[name=gridSinonimos]').getStore().add(new Ext.data.Record({text: sinonimo['text'], habilitado: 1, nuevo: true}));
            }
        } else
            notificaciones("No selecciono o busco los sinónimos de la entidad.", 'Mensaje', 2);
        MODULO_ENTIDADES.down('[name=ventanaSinonimo]').hide();
    }
});