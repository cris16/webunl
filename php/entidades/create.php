<?php

include '../../dll/config.php';
$data = json_decode(file_get_contents('php://input'));
if (isset($data)) {
    if (!$mysqli = getConectionDb()) {
        return;
    }

    $habilitado = 0;
    $idParametro = 0;
    if (isset($data->idParametro) && is_int($data->idParametro)) {
        $idParametro = $data->idParametro;
    }
    if (isset($data->habilitado)) {
        if ((bool) $data->habilitado) {
            $habilitado = 1;
        }
    }

    $sql_create_Intencion = "INSERT INTO botUnl.entidad (nombre, nivel, habilitado, idUsuarioRegistro) "
            . " VALUES "
            . "( '" . $data->nombre . "', '" . $data->nivel . "', " . $habilitado . " ,'" . $_SESSION["idUsuario"] . "')";
    $res = EJECUTAR_SQL($mysqli, $sql_create_Intencion);
    if (isset($res['id']) && intval($res['id']) > 0) {

        $dataEntrenamiento = json_decode($dataEntrenamiento);
        if (isset($dataEntrenamiento)) {
            foreach ($dataEntrenamiento as $p) {
                $resAdicional[] = $p;
                if ((bool) $p->nuevo) {
                    $sql_ProyectoNuevo = "INSERT INTO botUnl.entidadSinonimo (idEntidad, nombre, version, habilitado, idUsuarioRegistro) VALUES "
                            . " (" . $res['id'] . ", '" . $p->text . "', 1, b'1', " . $_SESSION["idUsuario"] . ");";
                    $resAdicional[] = EJECUTAR_SQL($mysqli, $sql_ProyectoNuevo);
                } else {
                    $sql_ProyectoActualizar = "UPDATE botUnl.entidadSinonimo SET ";
                    if ((bool) $p->habilitado) {
                        $sql_ProyectoActualizar .= "habilitado=b'1', ";
                    } else {
                        $sql_ProyectoActualizar .= "habilitado=b'0',";
                    }
                    $sql_ProyectoActualizar .= (isset($p->text)) ? "nombre = '$p->text', " : "";
                    $sql_ProyectoActualizar .= " idUsuarioActualizacion= " . $_SESSION["idUsuario"] . ",fechaActualizacion= NOW() "
                            . "where  idEntidadSinonimo= " . $p->id;
                    $resAdicional[] = EJECUTAR_SQL($mysqli, $sql_ProyectoActualizar);
                }
            }
        }
    }
    echo json_encode($res);

//    echo json_encode(EJECUTAR_SQL($mysqli, $sql_create_Intencion));
} else {
    echo json_encode(array('success' => false, 'message' => "FALTAN PARÁMETROS"));
}
$mysqli->close();
