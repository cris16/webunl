<?php

include '../../dll/config.php';

$data = json_decode(file_get_contents('php://input'));
if (isset($data->id)) {
    if (!$mysqli = getConectionDb())
        return;
    $sql_update = "UPDATE botUnl.plataforma SET ";
    $sql_update .= (isset($data->plataforma)) ? "nombre = '$data->plataforma', " : "";
    $sql_update .= (isset($data->idTipoPlataforma)) ? "idTipoPlataforma = '$data->idTipoPlataforma', " : "";
    $sql_update .= (isset($data->versionPlataforma)) ? "versionPlataforma = '" . number_format($data->versionPlataforma, 1) . "', " : "";
    $sql_update .= (isset($data->credenciales)) ? "detalle = '$data->credenciales', " : "";
    if (isset($data->habilitado)) {
        $sql_update .= (isset($data->habilitado)) ? " habilitado = b'" . (int) ($data->habilitado) . "', " : "";
    }
    $sql_update .= 'idUsuarioActualizacion =' . $_SESSION["idUsuario"] . ', ';
    $sql_update .= 'fechaActualizacion = NOW() WHERE ';
    $sql_update .= ' idPlataforma = ' . $data->id;
//    echo $sql_update;
    echo json_encode(EJECUTAR_SQL($mysqli, $sql_update));
} else {
    echo json_encode(array('success' => false, 'message' => "FALTAN PARÁMETROS"));
}
$mysqli->close();
