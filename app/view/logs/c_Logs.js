/* global Ext */
var MODULO_LOGS;
Ext.define('botWeb.view.logs.c_Logs', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.c_Logs',
    onView: function (panelLoad) {
        MODULO_LOGS = panelLoad;
        if (document.body) {
            HEIGT_VIEWS = (document.body.clientHeight);
        } else {
            HEIGT_VIEWS = (window.innerHeight);

        }
        HEIGT_VIEWS = HEIGT_VIEWS - 65;
        MODULO_LOGS.setHeight(HEIGT_VIEWS);
        MODULO_LOGS.down('[name=grid]').setHeight(HEIGT_VIEWS);
//        MODULO_LOGS.down('[name=grid]').getStore().load();
    },
    onRecargar: function () {
        MODULO_LOGS.down('[name=grid]').getStore().reload();
    },
    onLimpiarFormulario: function (btn, e) {
        if (btn.limpiar) {
            MODULO_LOGS.down('[name=form]').getForm().reset();
            var grid = MODULO_LOGS.down('[name=grid]');
            grid.getView().deselect(grid.getSelection());
            MODULO_LOGS.down('[name=form]').down('[name=idPlataforma]').enable();
        }
        MODULO_LOGS.down('[name=grid]').getStore().load();

    },
    onLimpiar: function (btn, e) {
        MODULO_LOGS.down('[name=txtParam]').reset();
        MODULO_LOGS.down('[name=idPlataforma]').reset();
        MODULO_LOGS.down('[name=idIntencion]').reset();
        MODULO_LOGS.down('[name=fecha]').reset();
        MODULO_LOGS.down('[name=grid]').getStore().load();
    },
    onBuscar: function (btn, e) {
        var params = {}, me = this;
        if (MODULO_LOGS.down('[name=fecha]').getRawValue()) {
            MODULO_LOGS.down('[name=paginacionGrid]').moveFirst();
            var fecha = MODULO_LOGS.down('[name=fecha]').getRawValue();
            params['fecha'] = fecha;
            if (MODULO_LOGS.down('[name=idPlataforma]').getSelection())
                params['idPlataforma'] = MODULO_LOGS.down('[name=idPlataforma]').getSelection()['id'];
            if (MODULO_LOGS.down('[name=idIntencion]').getSelection())
                params['idIntencion'] = MODULO_LOGS.down('[name=idIntencion]').getSelection()['id'];
            if (MODULO_LOGS.down('[name=txtParam]').getValue())
                params['param'] = MODULO_LOGS.down('[name=txtParam]').getValue();

//            if (MODULO_LOGS.down('[name=comboClientes]').getSelection()) {
//                var dataCliente = MODULO_NOTIFICACION.down('[name=comboClientes]').getSelection();
//                params['iU'] = dataCliente['data']['idContacto'];
//            }
//            console.log("params",params);
            if (btn.xtype === 'button' || e.event.keyCode === 13) {
                MODULO_LOGS.down('[name=grid]').getStore().load({
                    params: params,
                    callback: function (records, operation, success) {
                        if (!success)
                            setMensajeGridEmptyText(MODULO_LOGS.down('[name=grid]'), '<h3>Problema en la comunicación con el servidor local, por favor inténtelo mas tarde.</h3>');
                        else if (records.length === 0)
                            setMensajeGridEmptyText(MODULO_LOGS.down('[name=grid]'), '<h3>No existen resultados.</h3>');
                    }
                });
            }

        } else
            notificaciones("Los campos de fecha y plataforma son requeridos", 2);

    },
    cargarToolTip: function (c) {
        Ext.create('Ext.tip.ToolTip', {
            target: c.getEl(),
            html: c.tip
        });
    }
});